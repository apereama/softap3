import sys
import cairo
from TurtleCanvas import TurtleCanvas


#=====================
# Leaflet
#=====================
class Leaflet:
  """
  a leaflet made of turtle canvases, where I can draw
  """

  #-------------------------------
  def __init__(self, filename, width, height, title = ""):
    """
    """
    self.title = title
    self.pages = []
    self.width, self.height = width, height
    self.filename = filename
    self.surface = cairo.PDFSurface (self.filename, self.width, self.height)
    self.context = cairo.Context(self.surface)
    self.lastPage = None
    self.filename = filename
    
  #--------------------------------
  def newPage(self):
    """
    """
    if self.lastPage != None:
      self.context.show_page() # this creates a new page
      
    canvas = TurtleCanvas((self.width, self.height), self.surface, self.context)
    self.lastPage = canvas    
    return canvas    
  
  #--------------------------------
  def toPdf(self, filename):
    """
    returns a pdf with all the pages
    """
    if self.lastPage != None:
      self.context.show_page() # close the page
    

    