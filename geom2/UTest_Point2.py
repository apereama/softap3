#!/usr/bin/python
# -*- coding: utf-8 -*-
# ====================
#  HURA 0.9
# ====================
#
#
# ====================
#  License
# ====================
#
#    Hura v0.9
#    Copyright (C)2013 Angel Perea Martinez ( angelpeream @ yahoo.com )
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
#    MA  02110-1301  USA
#
# (see gpl.txt for the complete licence text).
#

import unittest
from softap3.geom2.Segment import Segment
from softap3.geom2.Point2D import Point2 as Point
from softap3.geom2.Transformations import *

INDEG = 360.0 / 6.28
#===========================
# THTestCase
#===========================
class Point2TestCase(unittest.TestCase):
  """
  test of the pushkin facilitiess
  """

  #----------------------------------------
  def setUp(self):
    pass

  #-----------------------------------------
  def tearDown(self):
    pass

  #----------------------------------------
  def testAngle(self):
    """
    """
    p1, p2 = Point(0,0), Point(0,1)
    q1, q2 = Point(4,4), Point(4,5)
    #print "argument", p1.argument()
    #print "argument", p2.argument()
    #print "testAngle", p1.angle(p2) * INDEG
    print
    print "args", p1.argument() * INDEG, p2.argument() * INDEG
    print (p2 - p1)
    print (p2 - p1).argument() * INDEG
    #print p2.angle(p1)Point(2,0).angle(Point(0,20)) * INDEG, 90)

  #--------------------------------------
  def testSub(self):
    self.assertEqual(Point(11,10) - Point(10,10), Point(1,0))
    self.assertEqual(Point(0,1) - Point(0,0), Point(0,1))
    

#------------------------------
#        getTestSuite
#------------------------------
def getTestSuite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(Point2TestCase))
    return suite



if __name__ == '__main__':
   unittest.TextTestRunner(verbosity=2).run(getTestSuite())