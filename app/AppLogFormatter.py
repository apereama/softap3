import logging

from softap3.stringTools import colorize

#=====================
# AppLogFormatter
#=====================

class AppLogFormatter(logging.Formatter):

    """
    an special formatter for applications, with colored output
    """
    lastSource = None

    #-------------------------------
    def __init__(self):
        """
        """
        logging.Formatter.__init__(self)
        self.lastFleeting = False
        self.alwaysTitle = True
        self.index = 0

    #-------------------------------
    def format(self, record):
        """
        a formatter that colorizes
        Level 15 is used to display lines that are going to be refreshed
        """
        INDENT = " " * 28
        color = {10: "grey", 15: "green", 20: "green",
                 30: "yellow", 40: "red", 50: "red"}.get(record.levelno, "gray")

        offset = {
            10: 0,
            15: 0,
            20: 0,
            30: 2,
            40: 0,
            50: 0}.get(
                record.levelno,
                0)
                                            # to compensate wrong colorize and
                                            # rjust

        record.tag = colorize(record.levelname, color)

        f, l, t = record.pathname.split("/")[-1], record.lineno, record.threadName
        name = "%s:%s@%s" % (f, l, t)

        record.name = colorize(name, color)

        # 2) title changes also if same logger but different level because of
        # color!

        if ((record.name != AppLogFormatter.lastSource) or
           self.lastFleeting or self.alwaysTitle):
            n = record.__dict__["name"]
            name = (" " * offset) + ('%s: ' % (n,)).rjust(40)
        else:
            name = INDENT

        # print dir(record)

        # 3) rest of lines
        msg = str(record.msg)
        self.index += 1
        lines = []
        for l in msg.split("\n"):
            while len(l) > 78:
                chunk, l = l[:78], l[78:]
                lines.append(chunk)

            lines.append(l)

        AppLogFormatter.lastSource = record.name
        self.lastFleeting = "\\fleeting" in msg         # to put names after a fleeting command

        return "\r" + name + lines[0] + \
               "\n".join([INDENT + l for l in ([""] + lines[1:])])
                # first r ensures begin of line
                # first line without indent, and the others indented and \n
                # separated



#=======================
# AppStreamHandler
#=======================
class AppStreamHandler(logging.StreamHandler):

    #-----------------------------------------------------------

    def emit(self, record):
        """
        Emit a record.

        If a formatter is specified, it is used to format the record.
        The record is then written to the stream with a trailing newline.  If
        exception information is present, it is formatted using
        traceback.print_exception and appended to the stream.  If the stream
        has an 'encoding' attribute, it is used to determine how to do the
        output to the stream.
        """
        try:
            stream = self.stream

            # changes to the standard, to allow \r and \n
            if str(record.msg).strip().endswith("\\fleeting"):   # allow for refreshing lines
                record.msg = str(record.msg).rstrip()[
                    :-9]     # strip trailing flag "\fleeting"
                fs = "%s\r"
            else:
                fs = "%s\n"
            #
            msg = self.format(record)
            if not logging._unicode:  # if no unicode support...
                stream.write(fs % msg)

            else:
                try:
                    if (isinstance(msg, str) and
                            getattr(stream, 'encoding', None)):
                        ufs = fs.decode(stream.encoding)
                        try:
                            stream.write(ufs % msg)
                        except UnicodeEncodeError:
                            # Printing to terminals sometimes fails. For example,
                            # with an encoding of 'cp1251', the above write will
                            # work if written to a stream opened or wrapped by
                            # the codecs module, but fail when writing to a
                            # terminal even when the codepage is set to cp1251.
                            # An extra encoding step seems to be needed.
                            stream.write((ufs % msg).encode(stream.encoding))
                    else:
                        stream.write(fs % msg)
                except UnicodeError:
                    stream.write(fs % msg.encode("UTF-8"))
            self.flush()
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)
            
            
            
#-------------------------------------------
def setupLogging():
    """
    """
    handler = AppStreamHandler()
    handler.setLevel(logging.INFO)
    handler.setFormatter(AppLogFormatter())
    logging.getLogger().addHandler(handler)
    logging.getLogger().setLevel(logging.INFO)

#-----------------------
# getLogger
#------------------------
def getLogger(name):
    return logging.getLogger(name)


#--------------------------
#
#--------------------------
def setLogLevel(level):
    logging.getLogger().setLevel(level)            
