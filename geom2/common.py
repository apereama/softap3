from softap3.geom2.Point2D import Point2D as Point
from softap3.geom2.Segment import Segment
from softap3.geom2.Shape import Shape
from softap3.geom2.BBox import BBox

degrees = 6.2830 / 360.0
