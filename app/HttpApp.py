#!/usr/bin/python
#-*- coding: utf-8 -*-

#######################################################################
#
# Application: 
# Module:
# Version:
# Author: Angel Perea Martinez (angelpeream@yahoo.com)
#
#
########################################################################


"""
====================
  License
====================

    Copyright (C)2007 Angel Perea Martinez ( angelpeream @ yahoo.com )

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
    MA  02110-1301  USA

 (see gpl.txt for the complete licence text).

"""

import sys
from softap.app.SimpleApp import *
from .HtaObject import HtaObject
from .HttpRequestHandler import HttpRequestHandler
from softap.common import *
import threading
import http.server
     
logger = logging.getLogger("asdf")

#=============================
# MyServer
#=============================
class MyServer(http.server.HTTPServer):
    """
    just to adapt error handling of the HTTPServer
    """

    def __init__(self, address, handler):
      """
      """
      http.server.HTTPServer.__init__(self, address, handler)
      self.timeout = 5         # timeout for waiting request. After a timeout, some statistics are print and the loop starts again
      self.timedOut = False    # gets to True if time elapsed without any hit
      
    #------------------------
    def handle_error(self, request, client_address):
      """ 
      handles errors during requests
      """            
      logger.warning('Error during request "%s" from %s' % (str(request), 
                                                            client_address,))
      logger.warning(getTraceback())

      
    #-------------------------------
    def handle_timeout(self):
      """
      called when timeout
      """
      self.timedOut = True
      
#======================
#      HttpApp
#======================
class HttpApp(SimpleApp):
  """
  """
  
  #----------------------------
  def __init__(self, root):
    """
    """
    SimpleApp.__init__(self, "edelnuss")
    self.root = root
    self.debugMode = True
    self.portNumber = 8040


  #--------------------------------------------
  def stop(self):
    """ 
    stops the serving and ends
    """
    self.SHUTDOWN = True
    self.logger.info("shutting down...")
    
    
  #--------------------------------------------
  def serveDaemon(self):
    """ starts a thread to serve """
    thread = threading.Thread(target=self.serve)
    thread.setDaemon(True) # don't hang on exit
    thread.start()
    
    
  #--------------------------------------------
  def serve(self):
    """
    the serving method. uses a global engine object to keep it
    between requests (creations of a new RequestHandler)
    """
        
    #recorder = self.configuration.recorder; # get black box    
    uptimeWatch = StopWatch()
    #recorder.setValue("browserServer.totalHits", 1)
   
    ### 2) create conection
    server_address = ('', self.portNumber)
    HttpRequestHandler.browser = self.root
    print("-----", self.root)
    try:      
      httpd = MyServer(server_address, HttpRequestHandler)
    except Exception as e:
      raise
      self.fatalError("could'nt start server. port %i is being used  " % self.portNumber, 
                 exitCode = -1)
    else:
      self.logger.info("serving '%s' on port %s" % (colorize("asdf", "cyan"), 
                                               colorize(str(self.portNumber), "cyan")))


    self.SHUTDOWN = False   
    hits = 0
    uptime = 1
    while not self.SHUTDOWN:

      reqWatch = StopWatch()      
      ######################
      httpd.timedOut = False # to check for time outs
      httpd.handle_request() # class SocketServer implements serve_forever in this way      
      ######################
      reqWatch.stop()
      
      uptime = uptimeWatch.getTime() + 1
      
      #hits = recorder.getValue("browserServer.totalHits")      
      self.logger.info(colorize("requests: %s   uptime: %s    reqs/min: %i" %\
                            (hits, secs2human(uptime), hits * 60.0 /  uptime ),
                           "grey") +\
                  "\\fleeting")
      
      ## TODO it swallows exceptions raised here. Why???
      
      ## log data about the server
      #recorder.logValue("browserServer.logHits", 1)
      #if not httpd.timedOut: recorder.incValue("browserServer.totalHits")        
      #recorder.setValue("browserServer.uptime", uptime) # updates the uptime
      #recorder.logValue("browserServer.requestTime", reqWatch.read())
      if self.SHUTDOWN: break  # because the request could make it go down
      
    self.logger.info("server finished")


  #-----------------------------
  def run(self):
    """
    method to execute that starts serving the browser
    This method can be called as is or as a daemon
    """
    
    ### 2) CREATE BROWSER 
      
    try:
      self.logger.info("initializing browser...")          
      
      server = self.serveDaemon()    # starts the browser in a daemon thread
                                        
      # 2) opens a browser with the initial page (no command)
      
      #web.openUrl("127.0.0.1:%i" % self.portNumber)
      
      while 1: # here I can put a loop to issue commands from this console 
               # to control levels etc!!!
               
        command = input(">")    # is a command app!!. It launches interactive() 
                                 # after starting the two services

    except KeyboardInterrupt as e:
      self.stop()
      if self.debugMode : raise
      fatalError(colorize(" keyboard interrupt: shutting down...", "yellow"), 
                 exitCode = EXITCODE_BREAK)
      
    except BaseException as e:
      self.logger.error("Uncatched exception in server '%s'. Exiting..." % e)
      print(getTraceback())
      self.fatalError(colorize("Untrapped exception while serving: shutting down...", "yellow"), 
                 exitCode = EXITCODE_SOFTWARE)

    finally:
     
      if self.root != None:        
        try:
          self.root.finalize()
        except BaseException as e:
          self.logger.error("exception '%s' while shutting down tree" % e)
          #self.logError(e)
        
      self.logger.info("end.")


    
#============================================
if __name__ == "__main__":
  app = RongoDaemon()
  app.main(sys.argv[1:])

