"""
@TODO
"""
############################################################################
#    Copyright (C) 2009 by Angel Perea Martinez                            #
#    angelpeream@yahoo.com                                                 #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation; either version 2 of the License, or     #
#    (at your option) any later version.                                   #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU General Public License     #
#    along with this program; if not, write to the                         #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################

from Transformations import *
# from Point3 import *
from .Point3 import Point3


class Camera:
    """
    a generic camera
    """

    # -----------------------------------------
    def __init__(self, position, pointing, up=Point3(0, 0, 1), aperture=1.0, focal=1.0):
        """
    @param position a handle with position
    """

        if isinstance(position, tuple): position = Point3(*position)
        if isinstance(pointing, tuple): pointing = Point3(*pointing)

        self.position = position
        self.pointing = pointing  # point where the camera points to
        self.up = up

        self.aperture = aperture
        self.focal = focal

    # ------------------------------------------
    def getTransform(self):
        """
    returns the transformation that 
    """

        a = 7
        scale = Scale(a, a, a)
        s = Translation(-self.position.x, -self.position.y, -self.position.z)
        uz = (self.pointing - self.position).normalize()
        ux = (uz ** self.up).normalize()
        uy = uz ** ux
        t = OrtogonalTransformation(ux, uy, uz)
        return t * s * scale

    # ------------------------------------------
    def transform(self, triangles):
        """
        @TODO
        """

        # 1) transforms the triangles
        ts = []
        trans = self.getTransform()
        for t in triangles:
            ts.append(trans * t)

        # 2) clip
        pass

        # 3) return remaining triangles
        return ts

    def getBeam(self, pixelX, pixelY, gridSize):
        """
        returns a beam for the given pixel
        """
        raise NotImplementedError()
        # shift = 0
        # p = self.vp.position + self.vp.direction.normalize() * self.focal + shift
        # return Handle(self.vp.position, p - self.vp.position,
        #              self.up.normalize(), self.right)
