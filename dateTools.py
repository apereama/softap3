import datetime
from .safeCheck import checkIntRange
import time

#-------------
def getToday():
        """
        """
        y, m, d = time.strftime("%Y:%m:%d").split(":")
        d = Date(y, m, d)
        return d

#================
# Date
#================
class Date:
    """
    """
    def __init__(self, yy, mm, dd, ss= 0):
        """
        """
        self.year = int(yy)
        self.month = int(mm)
        self.day = int(dd)
        self.secs = int(ss)
        
        if not (self.day >=1 and self.day <= 31): raise ValueError("wrong value (%i) for day" % self.day)
        if not (self.month >=1 and self.month <= 31): raise ValueError("wrong value (%i) for month" % self.month)
    
    
    #-----------------------
    def isLeap(self):
        """
        returns true if the year is a leap year
        """
        return self.year % 4 == 0
    
    
    #----------------------
    def daysInMonth(self, m):
        """
        returns the days in the given month
        """
        checkIntRange(m, 1, 12, ValueError, "month must be between 1 and 12")
        days = [None, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        return days[m]
    
    #--------------------
    def adjust(self):
        """
        """
        if self.day < 1:
            self.month -= 1
            self.day = self.daysInMonth(self.month)
            
        if self.month < 1:
            self.month = 12
            self.year -= 1
            
        if self.day > self.daysInMonth(self.month):
            self.month += 1
            self.day = 1
        
        if self.month > 12:
            self.year += 1
            self.month = 1
 
    #-------------------
    def shift(self, ss):
        """
        """
        if ss > 0:
            while ss > 0:
                self.day += 1
                self.adjust()
                ss -= 1
        else:
            while ss < 0:
                self.day -= 1
                self.adjust()
                ss += 1
                
    #-----------------------------------        
    def _getTimeObject(self):
            """
            """
            s = "%s/%s/%s" % (str(self.day).rjust(2, "0"), str(self.month).rjust(2, "0"), str(self.year).rjust(4,"0"))
            return datetime.datetime.strptime(s, r"%d/%m/%Y")
    
    
    #------------------------------------
    def julian(self):
        """
        returns the julian day
        """
    
        #if self.log.hasErrors(): raise ValueError( "format error:" + self.displayErrors())

        if self.year >= 0:
            r = self._getTimeObject().toordinal()
        else:
            r =    365 * self.year
        
        return r

        
