
#######################################
#
# Time
#
#
#
#
########################################
import re


#-----------------------------------------
def makeTime(year, month, day, hour, minute, seconds):
    """
    """
import time
     
def now():
    return time.time()

days = 86400
day = 86400
mins = 60
hour = 3600
hours = 3600

#--------------------------
# human2secs
#--------------------------
def human2secs(lexeme):
    s = "[0-9]+"
    dval = 86400; wval = dval * 7; mval = dval * 30; yval = dval * 365
    t = "mins|min|m|sec|s|hours|hour|day|days|week|weeks|month|months|year|years"
    u =    [60, 60 ,60, 1,1,3600, 3600,dval,dval, wval, wval, mval, mval, yval, yval]
    v = t.split("|")
    lexeme = lexeme.replace(" ", "").lower()
    value = 0;

    while lexeme != "":
        m = re.match(s, lexeme)
        if m == None:
            raise ValueError("must be a number: " + lexeme)
        multiplier = float(m.group(0))
        lexeme = lexeme[len(m.group(0)):]
        m = re.match(t, lexeme)
        if m == None:
            raise ValueError("unit not known")
        unit = u[v.index(m.group(0))]
        value += multiplier * unit
        lexeme = lexeme[len(m.group(0)):]

    return value

#--------------------------
#
#--------------------------
def human2secsShort(string, default = None):
    """
    """
    try:
        m = re.match("(.+d)?(.+h)?(.+m)?(.+s)?", string)
        #a = m.groups()
        s = 0
        if m.group(1) != None:    s += int(m.group(1)[:-1]) * 86400
        if m.group(2) != None:    s += int(m.group(2)[:-1]) * 3600
        if m.group(3) != None:    s += int(m.group(3)[:-1]) * 60
        if m.group(4) != None:    s += int(m.group(4)[:-1]) * 1
    except:
        if default == None: raise
        return default
        
    return s

#--------------------------
# secs2human
#--------------------------
def secs2human(seconds):
    """
    """
    if abs(seconds)     < 1.0:
        return "%2.2f msecs" % (seconds * 1000.0)
    else:
        sign = "-" if seconds < 0 else ""
        seconds = abs(seconds)
        string = ""
        seconds = float(seconds)
        minutes = int(seconds / 60)
        hours = int(minutes / 60)
        days = int(hours / 24)
        months = int(days / 30)
        years = int(months / 12)

        if years != 0: string += "%i years, " % (years)
        if months > 0: string += "%i months, " % (months % 12)

        if years == 0 and months == 0:
            if days > 0: string += "%i days, " % (days % 30)
            if hours != 0: string += "%i hours, " % (hours % 24)
            if minutes != 0: string += "%i mins, " % (minutes % 60)
            if seconds != 0:
                    string = string[:-2] + " and %i secs    " % (seconds % 60)
                    if string.startswith(" and"): string = string[5:]
        else:
            string = string[:-2] + " and %i days, " % (days % 30)

        if string == "":
            string = "%2.2f seconds" % seconds
        else:
            string = string[:-2]

        return sign + string

#------------------------------------
def secs2humanShort(seconds):
    """
    return a human-readable string for a number of secs ellapsed
    """
    string = ""
    seconds = float(seconds)
    minutes = int(seconds / 60)
    hours = int(minutes / 60)
    days = int(hours / 24)
    months = int(days / 30)
    years = int(months / 12)


    if years == 0 and months == 0:
            if days > 0: string += "%id" % (days % 30)
            if (hours%24) != 0: string += "%ih" % (hours % 24)
            if (minutes % 60 ) != 0: string += "%im" % (minutes % 60)
            if (seconds % 60 ) != 0:
                string += "%is" % (seconds % 60)
    else:
            if years != 0: string += "%iy" % (years)
            if months > 0: string += "%im" % (months % 12)
            string += "%id" % (days % 30)

    if string == "":
        string = "%2.2fs" % seconds
    else:
        if seconds < 0: string = "-" + string
        
    return string

#------------------------------------
def secs2humanCompact(seconds):
    """
    return a human-readable string for a number of secs ellapsed
    only two figures are returned
    """
    string = ""
    
    if seconds < 0:
        seconds = - seconds
        negative = True
    else:
        negative = False

    seconds = float(seconds)
    minutes = int(seconds / 60)
    hours = int(minutes / 60)
    days = int(hours / 24)
    months = int(days / 30)
    years = int(months / 12)
    chunks = 0
    
    if years == 0 and months == 0:
            if chunks < 2 and days > 0: 
                string += "%id" % (days % 30)
                chunks += 1
                
            if chunks < 2 and (hours%24) != 0: 
                string += "%ih" % (hours % 24)
                chunks +=1
            
            if chunks < 2 and (minutes % 60 ) != 0: 
                string += "%im" % (minutes % 60)
                chunks += 1
                
            if chunks < 2 and (seconds % 60 ) != 0:
                string += "%is" % (seconds % 60)
                chunks += 1
    else:
            if years != 0: 
                string += "%iY%iM" % (years, months % 12)
            else:
                string += "%iM%id" % (months % 12, days % 30)
            
            
    if string == "":
        string = "%2.2fs" % seconds
    
    if negative: string = "-" + string
    
    return string
#--------------------------------
# mins2human
#--------------------------------
def mins2human(mins, short = False):
    """
    """
    if short:
        return secs2humanShort(mins * 60)
    else:
        return secs2human(mins * 60)
    
             
def time2human(t):
    return time.strftime("%d/%m/%y", time.localtime(t))    

#============================
#     Time
#=============================
class Time:

    def __init__(self):
            """
            """

    #-----------------------------------------
    def advance(self,timeTuple, seconds):
        """
        """


    #---------------------------
    def strToTimeLapse(self):
        """
        """

    #---------------------------
    def getField(self, field):
        """
            field is one of "month", "day", "weekday", "year", "seconds", "hours", "minutes"
        """

    #----------------------------
    def distance(self,other):
            """
            returns the distance as a timelapse between two
            """
            return TimeLapse()


#=====================================
#            TimeLapse
#=====================================
#--------------------------------------
class TimeLapse:

    def __init__(self, seconds):
        self.seconds = seconds
        self.minutes = 0
        self.hours = 0
        self.days = 0
        self.months = 0

    #--------------------------------
    def toHuman(self):
            string = ""
            minutes = int(self.seconds / 60)
            hours = int(self.minutes / 60)
            days = int(self.hours / 24)
            months = int(self.days / 30)
            years = int(self.months / 12)

            if self.years != 0: string += "%i years, " % (self.years)
            if self.months > 0: string += "%i months, " % (self.months % 12)

            if years == 0 and months == 0:
                if days > 0: string += "%i days, " % (days % 30)
                if hours != 0: string += "%i hours, " % (hours % 24)
                if minutes != 0: string += "%i mins, " % (minutes % 60)
                if self.seconds != 0:
                        string = string[:-2] + " and %i secs    " % (self.seconds % 60)
                        if string.startswith(" and"): string = string[5:]
            else:
                string = string[:-2] + " and %i days, " % (days % 30)

            if string == "":
                string = "%2.2f seconds" % self.seconds
            else:
                string = string[:-2]

            return string

            
            
    #----------------------------------
    def convertTo(self, units):
            """
            """
            # convierte en las unidades pedidas y lo devuelve


