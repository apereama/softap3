"""
@TODO
"""

from Transformations import TrivTrans
from Body import Body


class ComplexBody(Body):
    """
    a body that contains other bodies
    """

    def __init__(self):
        """
        """
        Body.__init__(self)
        self.trans = TrivTrans()
        self.subBodies = []

    def getTriangles(self):
        """
        @TODO
        """
        primitives = []

        for b in self.subBodies:
            primitives += b.getTrans() * b.getTriangles()

        return primitives
