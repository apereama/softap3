from rongo.Common import guard
from rongo.core.MimeData import  MimeData



#====================
# DocbookDocument
#====================
class DocbookDocument:
    """
    a docbook document.
    """
    
    #---------------
    def __init__(self):
        """
        """
        self.text = []
        self.language = "html"
        self.name = "document"
        
    #-------------------
    def writeLine(self, line):
        """
        """
        self.text.append(line)
        
    #-------------------
    def addSection(self, title, level):
        """
        """
        tags = {1: ("<h1>", "</h1>"),
                        2: ("<h2>", "</h2>"),
                        3: ("<h3>", "</h3>"),
                        4: ("<h4>", "</h4>")
                        }
        
        tag = tags[guard(level, 1, 4)]
        self.writeLine(tag[0] + title + tag[1])
        
    #--------------------
    def write(self, text):
        """
        """
        self.text.append(text)
     
    #--------------------
    def getFile(self):
        """
        """
        return MimeData("%s.txt" % self.name, "\n".join(self.text))
