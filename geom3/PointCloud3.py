#=============================
#
#=============================
class PointCloud3:
  """
  """
  def __init__(self, points):
    """
    """
    self.points = points

  #--------------------------------------
  def getMassCenter(self):
    """
    """
    nPoints = float(len(self.points))
    x = sum(p.x for p in self.points)
    y = sum(p.y for p in self.points)
    z = sum(p.z for p in self.points)
    return Point(x / nPoints, y / nPoints, z / nPoints)

  #-------------------------------------
  def getHull(self):
    """
    """
    raise "Not implemented"

  #--------------------------------------
