"""
@TODO
"""


class Handle:
    """
    represents a coordinate system located in a point
    """

    def __init__(self, point, direction, up):
        """
        @param point of the base
        @param point up
        @para point direction
        """
        self.p = point

        self.dir = direction.normalize()
        self.right = (self.dir ** up).normalize()
        self.up = (self.right ** self.dir).normalize()

    def getTransformationInto(self, other):
        """
       returns the transformation that transforms other handle in this
       """

        t1 = Offset3D(-other.p.x, -other.p.y, -other.p.z)
        t2 = Rotation3D(other.up.x, other.up.y, other.up.z,
                        other.right.x, other.right.y, other.right.z,
                        other.dir.x, other.dir.y, other.dir.z)

        # now, other points as the  xyz axes

        # we get the transformation of self into xyz axes

        t = t2 * t1  # this is the transformation that gets other into the xyz axes

        # this is where goes the handle under this transformation

        p2 = t.apply(self.p)
        up, right, direction = t.apply(self.up), t.apply(self.right), t.apply(self.dir)

        # this transforms this handle in the xyz

        t3 = Offset3D(-p2.x, -p2.y, -p2.z)
        t4 = Rotation3D(up.x, up.y, up.z,
                        right.x, right.y, right.z,
                        direction.x, direction.y, direction.z)

        # and undoing the initial transformation we get xyz into the handle "other"

        t5 = t2.inverse()
        t6 = t1.inverse()

        return t6 * t5 * t4 * t3 * t2 * t1

    def __str__(self):
        """@TODO"""
        return str(self.p) + ":" + str(self.dir) + ":" + str(self.right) + ":" + str(self.up)
