"""
@TODO
"""

from Body import *
from Point3 import *
import math
from .Triangle import Triangle

PI = math.pi


class Cone(Body):
    """
  @TODO
  """

    def __init__(self, height, radius, apexShift=(0, 0)):
        """@TODO"""
        Body.__init__(self)
        self.height = height
        self.radius = radius
        self.apex = Point3(apexShift[0], apexShift[1], height)

    def getTriangles(self):
        """
        @TODO
        """
        triangles = []
        detail = 20

        for i in range(detail):
            theta = (i * 2 * PI) / detail
            thetaold = ((i - 1) * 2 * PI) / detail

            # bottom
            triangles.append(Triangle(Point3(0, 0, 0),
                                      Point3(self.radius * math.cos(theta),
                                             self.radius * math.sin(theta),
                                             0),
                                      Point3(self.radius * math.cos(thetaold),
                                             self.radius * math.sin(thetaold),
                                             0)
                                      )
                             )

            # side
            triangles.append(Triangle(self.apex,
                                      Point3(self.radius * math.cos(theta),
                                             self.radius * math.sin(theta),
                                             0),
                                      Point3(self.radius * math.cos(thetaold),
                                             self.radius * math.sin(thetaold),
                                             0)
                                      )
                             )

        return triangles
