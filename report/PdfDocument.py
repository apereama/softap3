
from softap3.safeCheck import guard 

from softap3.shellTools import bashCall
from softap3.MimeData import MimeData
from softap3.fsTools import readFile, readBinFile, writeBinFile, writeFile

from .Document import Document
import os
#import tempfile
from softap3.report.TextWriter import TextWriter
import random

def old_tempnam():
    """
    returns a temporary name
    """
    return "/tmp/file" + "".join([random.choice("abcdefghijklmnopqrstuvwxyz0123456789") for _ in range(5)])
    # @TODO fix this before EDN20!!


TEMPLATE = r"""

%%packages%%
\documentclass[%%columns%% hidelinks,a4paper,8pt]{report}
\usepackage[usenames,dvipsnames, svgnames]{xcolor}
\usepackage{graphicx}    % to include figures
\usepackage{listings}
\usepackage{ulem}        % tachar text
\normalem
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[greek,german,russian]{babel}
\usepackage{tabularx}
\usepackage{pdfpages}
\usepackage{multicol}
\usepackage{subfloat}
\usepackage{geometry}
\usepackage{soulutf8}     % highlight and things. needs soulutf8 for accents
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{amssymb}
\usepackage{subcaption}
\usepackage{mdframed}
\usepackage[version=4]{mhchem}
\usepackage{float}
\usepackage{hyperref}
\usepackage{enumitem}
\usepackage{wrapfig}
\usepackage{sectsty}
\usepackage{tikz}
\usepackage{tikz-qtree}

% helvetica as default
\usepackage[scaled]{helvet}
\renewcommand\familydefault{\sfdefault} 
\usepackage[T1]{fontenc}

\usetikzlibrary{trees}    % for trees 
\geometry{a4paper, portrait, margin=20mm}
\pagestyle{empty}
\setlistdepth{9}
\renewlist{itemize}{itemize}{9}
\setlist[itemize]{label=$\bullet$}

%%..............................
\begin{document}
%%text%%
\end{document}
"""[1:]

import unicodedata

#------------------------
# latexProtect
#------------------------
def latexProtect(text):
        """
        this is a pretty slow function, most likely candidate to be optimized!!!!
        
        @param string string being formatted to latex
        @returns the string so i doesn't flip error messages
        """
        string = str(text)
        unique = "".join([random.choice("123456789") for _ in range(100)]) # an unlikely string
        string = string.replace(r"$", unique)    # change to avoid problems later...
        
        string = string.replace("\\", r"$\backslash$")
                        # this will generate $, that's why they were removed
                        # and changed
                        
        string = string.replace(unique, r"\$") # to avoid replacing later this backslash.
        string = string.replace( r"{", r"\{")    # to avoid replacing later this backslash.
        string = string.replace(r"}", r"\}") # to avoid replacing later this backslash.
        string = string.replace(r"&", r"\&") # to avoid replacing later this backslash.
        string = string.replace(r"#", r"\#") # to avoid replacing later this backslash.
        string = string.replace(r"%", r"\%") # to avoid replacing later this backslash.
        string = string.replace(r"^", r"\^{}")    # to avoid replacing later this backslash.
        string = string.replace(r"_",r"-")    # to avoid replacing later this backslash.
        string = string.replace(r"~",r"\~")    # to avoid replacing later this backslash.

        return "".join(ch for ch in string if unicodedata.category(ch)[0] != "C")

        return string
    
        while 1: 
            try:
                _ = str(string)
                return _
        
            except:                
                s = ""
                for c in string:
                    try:
                        _ = str(c)
                        s += _
                    except:
                        s += "\$"

                string = s

#====================
# PdfDocument
#====================
class PdfDocument(Document):
    """
    a pdf document, that allows for putting
    Pdf documents can be formed with html
    """
    
    #-------------------
    def __init__(self, template = TEMPLATE, params = None):
        """
        """
        Document.__init__(self, params = params)
        self.writer = TextWriter()
        self.language = "pdf"
        self.template = template
        self.params = {}
        self.parts = {} # dictionary to store sections
        self.columns = 2
        self.lastError = None
        if params != None:
            self.params = params


    #-------------------------
    def addSection(self, title, level):
        """
        adds a section to th document
        @title the title for the section
        @level an integer, from 1 to 5. The meanings are:
            * 1: part
            * 2: chapter
            * 3: section
            * 4: subsection
            * 5: subsubsection
        """

        tags = {1: ("\part{}", ""),
                2: ("\chapter{}", ""),
                3: ("\section{}", ""),
                4: ("\subsection{}", ""),
                5: ("\subsubsection{}", ""),
                }

        tag = tags[guard(level, 1, 4)]
        self.writer.writeLine(tag[0] + title + tag[1])


    #-----------------------
    def getSource(self):
        """
        @return the latex code
        """
        self.flush()
        
        # 1) main part
        latex = self.writer.getText()
            # use getText() to avoid applying templates in the part
                
        # 2) Compose document, merging the parts
        for p in self.partList:
            self.addSection(p, 1)
            partWriter = self.getPart(p).getWriter()
            partWriter.flush()
            latex += partWriter.getText()
                 
            # 2.2) merge images
            pass
        
        # 3) apply the template        
        latex = self.template.replace("%%text%%", latex)
        c = "twocolumn" if self.columns == 2 else ""
        latex = latex.replace("%%columns%%", c)
        latex = latex.replace("%%packages%%", "")
        return latex

            
    #--------------------------------
    def writeRaw(self, text):
        """
        writes text without protecting it, so any latex commands inside
        the text will be interpreted
        @param text     
        """
        self.write(text, raw = True)
        
    #-------------------------------
    def rawWrite(self, text):
        """
        writes text without escaping control chars
        """
        self.writeRaw(text)
        
        
    #----------------------
    def addFigure(self, caption = None, content = None):
        """
        @param caption caption for the figure
        @param content what goes inside the figure borders
        """

        self.writeRaw(r"""
\begin{figure}[H]
\centering 
""")    

        try:
            if not isinstance(content, str):
                content.display_text_latex(2, self)
            else:
                self.rawWrite(content)

            if not isinstance(caption, str):
                caption = caption.display_text_latex(2, self)
            
            self.writeCommand("caption", caption)

        finally:
                self.writeCommand("end", "figure")


    #-------------------------------------------
    def addImageAbsolute(self, x, y, filename, data, scale = 0.3):
        """
        adds an image in an absolute position respect of the page
        """
        if not isinstance(data, MimeData): 
            raise ValueError("mime data expected")
        
        fname = self.addResource(filename, data)
        self.writeLn(r"\begin{picture}(%i,%i)" % (x,y), raw = True)
        self.writeLn(r"\put(0,0){\hbox{\includegraphics[scale=%f]{%s}}}" %
                        (fname, scale), raw = True)
        
        self.writeLn(r"\end{picture}", raw = True)

        
    #----------------------------------
    def addDoubleFigure(self, caption1, content1, caption2, content2):
        """
        """
        self.writeLn("[double figure]")
    
    
    #----------------------------------
    def addFlowFigure(self, caption, content):
        """
        """
        self.write("[flow figure]")

            
    #----------------------------------
    def addTable(self, cells):
        """
        """
        self.writeLn("[table]")

    #-----------------------------
    def flush(self):
        self.writer.flush()

    #----------------------------------
    def writeItem(self, text, name = "", raw = False):
        """
        writes a latex \command 
        @param text
        @param name
        @param raw 
        """
        if name != "":
            name = "{%s}" % self._protect(name)

        self.writeLn(r"\item%s %s" % (name, self._protect(text)), raw = True)


    #------------------------------------
    def writeError(self, text):
        """
        writes text in the form of an error
        @param text explanation of the error 
        
        """
        self.writeRaw(r"\textcolor{Red}{%s}" % self._protect(text))
   
    #-----------------------
    def getWriter(self):
        """
        returns the textWritter of this document
        """
        return self.writer
            
            
    #---------------------------------
    def writeCommand(self, command, text = ""):
        """
        writes a latex \command
        @param command name of the command
        @param text text inside {} 
        """
        self.writeLn(r"\%s{%s}" % (command, self._protect(text)), raw = True)
        
              
    #----------------------
    # private functions
    #--------------------
    def _getFile(self, name = "document", tempDir = None):
        """
        This is a long function to pack everything into a single file.
        in this case, just parses and returns the pdf file
        @param name
        @param tempDir temporary directory where to do the calculations
        """
        
        if tempDir == None:
            directory = oldTempnam(); 
        else:
            directory = tempDir
             
        if not os.path.isdir(directory): os.makedirs(directory)
        
        self.saveResources(directory)
        latexFilename = "file.latex"
        pdfFilename = "file.pdf"
        
        # 1) execute latex command
        
        current = os.getcwd();    os.chdir(directory)
        writeFile(latexFilename, self.getSource())
        cmd = "xelatex    -interaction=nonstopmode -halt-on-error -output-directory='%s' '%s'" % (directory, latexFilename)
        code, std, err    = bashCall(cmd)
        
        if code != 0:
            self.lastError = str(std) + "\n" + "-------" + "\n" + str(err) 
            print(self.lastError)
            print("source code in %s" % directory)
            raise ValueError("there was an error parsing the source code")
        
        
        # 2) return the generated file        
        
        data = readBinFile(pdfFilename)
        data = MimeData("%s.pdf" % name, data)
        os.chdir(current) # return to the previous directory
        return data
    
    
    #---------------------------------
    def _protect(self, text):
        """
        """
        return latexProtect(text)
    
