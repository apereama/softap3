
from Body import Body
from Primitives import *
from Point3 import *


#=======================
# Brick
#=======================
class Brick(Body):
  """
  """
  
  #------------------------------------------------------
  def __init__(self, width, height, depth):
    """
    """
    Body.__init__(self)
    self.width, self.height, self.depth = width, height, depth
  
  #-----------------------------------------------------
  def getVolume(self):
    """
    """
    return self.width * self.height * self.depth
  
  #------------------------------------------------------
  def getTriangles(self):
    """
        p3-----------p4
       / |          / |
     p1-----------p2  |  
     |   |        |   |
     |   |        |   |
     |  p7--------|--p8
     | /          | /
     p5 ----------p6
     
    """
    
    w2, h2, d2 = self.width / 2.0, self.height / 2.0, self.depth / 2.0
    p1, p2 = Point3(-w2,  h2, -d2), Point3(w2,  h2, -d2)
    p3, p4 = Point3(-w2,  h2,  d2), Point3(w2,  h2,  d2)
    p5, p6 = Point3(-w2, -h2, -d2), Point3(w2, -h2, -d2)
    p7, p8 = Point3(-w2, -h2,  d2), Point3(w2, -h2,  d2)
    
    
    return [
      Triangle(p1, p5, p2, vis="* *"), Triangle(p5, p6, p2, vis="** "),  # front
      Triangle(p2, p6, p4, vis="* *"), Triangle(p6, p8, p4, vis="** "),  # east side
      Triangle(p7, p4, p8, vis=" **"), Triangle(p4, p7, p3, vis=" **"),  # back
      Triangle(p1, p3, p5, vis="* *"), Triangle(p5, p3, p7, vis=" **"),  # west side
      Triangle(p3, p1, p2, vis="** "), Triangle(p3, p2, p4, vis=" **"),  # upper lid
      Triangle(p5, p7, p6, vis="* *"), Triangle(p7, p8, p6, vis="** ")   # bottom lid
    ]
   
   #---------------------------------------------------
   