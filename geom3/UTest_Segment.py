
import unittest

#############################
#  TESTS
############################

#------------------------------------------
from softap3.geom3d.Vector import *
from softap3.geom3d.Plane import Plane
from softap3.geom3d.Segment import Segment

#===============
#
#================
class SegmentTestCase(unittest.TestCase):
    """
    tests sobre otras funciones de la base
    """
    
    def setUp(self):
        pass
    
    #--------------------------
    def tearDown(self):
        pass        
   
    #---------------------------
    def test1(self):
      """
      """
      s1 = Segment(Point(-2, 0,0), Point(2,0,0))
      s2 = Segment(Point( 0, -3, 3), Point(0, 3, 3))
      self.assertAlmostEqual(s1.distance(s2), 3.0)
      
      
        
        

if __name__ == '__main__':
     suite = unittest.TestSuite()
     suite.addTest(unittest.makeSuite(SegmentTestCase))
     unittest.TextTestRunner(verbosity=2).run(suite)


