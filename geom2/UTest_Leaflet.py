from softap3.geom2.Leaflet import Leaflet
from softap3.geom2.Point2D import *
import random

l = Leaflet("output.pdf", 400, 400, "test")

for i in range(10):
    canvas = l.newPage()
    canvas.setColor(0, 0, 0)
    for j in range(100):
        canvas.setTransform(Point2D(0, 0), Point2D(400, 400), Point2D(0, 0), Point2D(400, 400))

        canvas.line(random.uniform(0, 400), random.uniform(0, 400),
                    random.uniform(0, 400), random.uniform(0, 400))

    canvas.rectangle(Point2D(0, 0), Point2D(400, 400))
    canvas.line(0, 0, 4000, 4000)

    canvas.savePng("asdf.png")
