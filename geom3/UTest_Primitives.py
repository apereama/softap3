

#############################
#  TESTS
############################

#------------------------------------------


#===============
#
#================
class VectorTestCase(unittest.TestCase):
    """
    tests sobre otras funciones de la base
    """
    
    def setUp(self):
        pass
        
    def tearDown(self):
        pass        
   

    def testPow(self):
      x = Vector(1,0,0)
      y = Vector(0,1,0)
      z = Vector(0,0,1)
      print x ** y
      print y ** z
      print z ** x
      
    
#=================
#
#=================
class HandleTestCase(unittest.TestCase):
  
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testGetTransformation(self):
        h1 = Handle(Point(0,0,0), Vector(0,1,0), Vector(0,0,1))
        h2 = Handle(Point(10,10,10), Vector(0,0,1), Vector(1,0,0))
        
        t = h1.getTransformationInto(h2)

        print 
        print t
        p1 = Point(10,10,10);  print p1, t.apply(p1)
        p2 = Point(11, 10, 10); print p2, t.apply(p2)
        p3 = Point(10, 11, 10); print p3, t.apply(p3)
        p3 = Point(10, 10, 11); print p3, t.apply(p3)
        

        
        
        

if __name__ == '__main__':
     suite = unittest.TestSuite()
     suite.addTest(unittest.makeSuite(VectorTestCase))
     suite.addTest(unittest.makeSuite(HandleTestCase))     
     unittest.TextTestRunner(verbosity=2).run(suite)


