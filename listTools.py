# coding=utf-8

"""
@TODO <short description of the module>

@TODO <description of the module>

:copyright: 2016 by Angel Perea Martinez (angelpeream@yahoo.com), see AUTHORS
            for more details

:license: lGPLv3, see LICENSE for more details
"""

   
#=======================
#  DictCounter
#=======================
class DictCounter:
    """
    """
    def __init__(self):
        self.d = {}
      
    def fill(self, key):
        if key in self.d:
            self.d[key] += 1
        else:
            self.d[key] = 0


#========================
# setCompare
#========================
def setCompare(setA, setB):
    """
    Compares two sets, returning
    """
    onlyInA  = [e for e in setA if e not in setB]
    inBoth = [e for e in setA if e     in setB]
    onlyInB = [e for e in setB if e not in setA]
    return onlyInA, inBoth, onlyInB

#=========== List functions  =================0

#------------------
# sort
#------------------
def sort(ls):
    """ sorts a list and returns it, making possible compact
    expressions like a = sort(myList)[0]
    """
    a = sorted(ls[:])
    return a


#---------------------------
# remDups
#---------------------------
def remDups(list_):
    """
    remove duplicates in a list, IN PLACE
    """
    s = {}
    for i, e in reversed(list(enumerate(list_))):
        if e in s:
            list_.pop(i)
        else:
            s[e] = True
            
    return list_
#----------------------------------
# getPairs
#---------------------------------
def getPairs(pList, cycle=False):
    """
    returns the pairs
    """
    result = []
    l = len(pList)
    n = l - 1
    if cycle:
        n += 1
    for i in range(n):
        j1 = i % l
        j2 = (i + 1) % l
        result.append((pList[j1], pList[j2]))

    return result


#---------------------------------------
# shuffle
#------------------------------------
def shuffle(listA, listB):
    """
    """
    if len(listA) != len(listB):
        raise ValueError("diff lengths")

    result = []
    for i in range(len(listA)):
        result.append(listA[i])
        result.append(listB[i])

    return result


#---------------------------------------
# compose
#------------------------------------
def compose(listA, listB):
    """
    """
    if len(listA) != len(listB):
        raise ValueError("diff lengths")

    result = []
    for i in range(len(listA)):
        result.append((listA[i], listB[i]))

    return result
#------------------------------------
# compareSets
#-----------------------------------
def compareAsSets(list1, list2):
    """
    compare two lists a sets
    @return a tuple (members in l1, members in both, members in l2)
    """
    list1 = list1[:]  # create copies
    list2 = list2[:]

    # 0) initialize

    both, r1, r2 = [], [], []

    # 1)  match equal elements

    for l1 in list1:
        if l1 in list2:
            list2.remove(l1)
            both.append(l1)
        else:  # is in l1 but no in l2
            r1.append(l1)

    # 2) the rest are in l2 but not in l1

    r2.extend([a for a in list2])
    return r1, both, r2

#---------------------
# compare
#-----------------------
def compare(list1, list2):
    return compareAsSets(list1, list2)

#-----------------------
# intersection
#-----------------------
def intersection(list1, list2):
    """
    returns a list with the elements present in both lists
    """
    return compare(list1, list2)[1]

#------------------------
# union
#------------------------
def union(list1, list2):
    """
    returns a list with the members in one or both lists
    """
    a, b, c = compare(list1, list2)
    return a + b + c

#--------- 
#------------------------
# distribute
#------------------------
def distribute(list1, nSets):
    """
    distribute the elements of the list into N sets
    """
    result = []
    posts = len(list1)
    for i in range(nSets):  # 0,1,2,3 for a 3-lenght list
        result.append(
            list1[int(round(posts * 1.0 * (i) / nSets)): int(round(posts * 1.0 * (i + 1) / nSets))])

    return result


def pairs2dict(pairs):
    d = {}
    for a, b in pairs:
        d[a] = b
    return d
        
        
        
