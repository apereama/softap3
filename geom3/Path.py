
from Body import Body
from Primitives import *
from Point3 import *
from softap3.common import getPairs

#=======================
# Path
#=======================
class Path(Body):
  """
  """
  
  #------------------------------------------------------
  def __init__(self, points):
    """
    """
    Body.__init__(self)
    self.points = points
  
  
  #------------------------------------------------------
  def getTriangles(self):
    """
    """
    primitives = []
    for p1, p2 in getPairs(self.points):
      primitives.append(Segment(p1,p2))
    
    return primitives
      
    