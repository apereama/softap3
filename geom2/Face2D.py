#======================
#  Face2D
#======================
class Face2D:
  """
  """
  def __init__(self, points = None):
    if points == None: points = []
    self.point = points
    
    
  #----------------------------------
  def join(self, other, myEdge, itsEdge):
    """
    moves this face to 
    """
    trans = self.getTrans(myEdge, itsEdge)
    self.trans = trans * self.trans
    
  #----------------------------------
  def getBBox(self):
    """
    returns the bounding box
    """
    raise NotImplementedError("not implemented")