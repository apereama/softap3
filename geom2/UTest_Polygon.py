"""
@TODO
"""
#!/usr/bin/python
# ====================
#  HURA 0.9
# ====================
#
#
# ====================
#  License
# ====================
#
#    Hura v0.9
#    Copyright (C)2013 Angel Perea Martinez ( angelpeream @ yahoo.com )
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
#    MA  02110-1301  USA
#
# (see gpl.txt for the complete licence text).
#

import unittest, random
from softap3.geom2.Segment import Segment
from softap3.geom2.Point2D import Point2
from softap3.geom2.Polygon2 import *
from softap3.geom2.TurtleCanvas import *

from anableps.tenebrio.Triangle2 import Triangle2

#===========================
# THTestCase
#===========================
class PolygonTestCase(unittest.TestCase):
  """
  test of the pushkin facilitiess
  """

  def setUp(self):
    """@TODO"""
    pass

  def tearDown(self):
    """@TODO"""
    pass

  def noest1(self):
    """
    @TODO
    """
    p = Polygon2( (Point2(0,0),Point2(2,0),Point2(2,2),Point2(1,1), Point2(0,2)) )

    pol = """

      9+ . . . . . . . . . . . . . .
      8+ . - . . . . . . . . . . . .
      7+ . . a . . . . . b . . . . .
      6+ . . . . . . . . . . . . . .
      5+ . . . . . x - . . - . . . .
      4+ . . c . . . . . d . . . . .
      3+ . . . . . . . . . . . . . .
      2+ . . . . . . . . . . . . . .
      1+ . . . . . . . . . . . . . .
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         1 2 3 4 5 6 7 8 9 10
    """
    
    a = Point2(3,7, "a"); b = Point2(9,7,"b")
    c = Point2(3,4, "c"); d = Point2(9,4, "d")
    x = Point2(6,5, "x");

    #p = Polygon2( [a,c,d,b])    
    #print p.earclip();

    p = Polygon2( [a,c,x,d,b])
    #print pol
    #print p.earclip();
    self.assertEqual(p.earclip(), [(a, c, x), (x, d, b), (x, b, a)])

    
  #-----------------------------------------
  def est3(self):
    """
    """
    p = Polygon2( (Point2(0,0),Point2(2,0),Point2(2,2),Point2(1,1), Point2(0,2)) )    
    
    """

      9+ . . . k . . . i . . . . . .
      8+ . m . . . . . . . . . . . .
      7+ . . . l . j h . . . . . . .
      6+ . . . . . . . . . . . . . .
      5+ . . n . . . f . . g . . . .
      4+ . . . . b d . . . . . . . .
      3+ o . p . . . . . . . . . . .
      2+ . . . . c . . e . . . . . .
      1+ . a . . . . . . . . . . . .
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         1 2 3 4 5 6 7 8 9 10      
    """
    p = Polygon2( (Point2(2, 1, "a"),Point2(5, 4, "b"),Point2(5, 2, "c"),Point2(6, 4, "d"), Point2(8, 2, "e"),
                   Point2(7, 5, "f"),Point2(10 ,5, "g"),Point2(7, 7, "h"),Point2(8, 9, "i"), Point2(6, 7, "j"),
                   Point2(4, 9, "k"),Point2(4, 7, "l"),Point2(2, 8, "m"),Point2(3, 5, "n"), Point2(1, 3, "o"),
                   Point2(3, 4, "p")
                ))
    print p.earclip()

  
#-----------------------------------------
  def test4(self):
    """
    """

    s = """

      9+ . . . . . x . . . . . . . .
      8+ . . . . . . . . . . . . . .
      7+ . . . . . . . . . . . . . .
      6+ . . . . a . b . . . . . . .
      5+ . t . . . . . . . . y . . .
      4+ . . . . d . c . . . . . . .
      3+ . . . . . . . . . . . . . .
      2+ . . . . . . . . . . . . . .
      1+ . . . . . z . . . . . . . .
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         1 2 3 4 5 6 7 8 9 10
    """
    print s
    a = Point2(5, 6, "a"); b = Point2(7,6, "b"); c = Point2(7,4, "c"); d = Point2(5,4, "d")
    x = Point2(6, 9, "x"); y = Point2(10,5, "y"); z = Point2(6,1, "z"); t = Point2(2,5, "t")
    p = Polygon2([a,t,d,z,c,y,b,x])
    p.reverse()
    self.show(p)

    
  #-------------------------
  def show(self, polygon):
    """
    """    
    canvas = TurtleCanvas((600,600))
    canvas.setTransform(Point2(-20, -20), Point2(20, 20), Point2(0,0), Point2(600,600))    
    polygon.paint(canvas)
    for t in polygon.scale(1.0).triangulate():
      #print t
      a,b,c = t
      Triangle2(a,b,c, color=randomColor() ).scale(0.7).paint(canvas);
        
    canvas.savePng("testPrim2.png")


def randomColor():
    return (random.uniform(0,1), random.uniform(0,1), random.uniform(0,1))

#------------------------------
#        getTestSuite
#------------------------------
def getTestSuite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(PolygonTestCase))
    return suite



if __name__ == '__main__':
   unittest.TextTestRunner(verbosity=2).run(getTestSuite())