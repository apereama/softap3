"""
########################################################

  EDN TEST TOOLS.


many functions used in ed testing


getTemporalCopy(directory/file, prefix = ""): returns a path to a temporal copy of the directory/file
compareHTML(code, masterPage, setMaster = False, show = False):  compares html code


something to put a biscuit in a controlled environment




########################################################
"""

import sys, shutil
import re
import time
import os
import traceback
import random
import inspect
from inspect import currentframe, getframeinfo
from io import StringIO

MIN_LEVEL = 10
MAX_LEVEL = 50
LOG_LEVEL = 30
lastTestPoint = None
   
#-------------------------    
def warning(*strings):
    log(40, *strings)
        
#---------------------        
def debug(*strings):
    """
    """
    log(10, *strings)
    
#--------------------    
def log(level, *strings):        
    if LOG_LEVEL <= level:
        print((" ".join([str(s) for s in strings])))

#---------------------
# currentLine
#---------------------
def currentLine():
    """
    returns the current line in a program
    """
    return inspect.currentframe().f_back.f_lineno
  
  
#-------------------------
# getTraceback
#-------------------------
def getTraceback():
    """
    returns the traceback
    """
    tb = sys.exc_info()[2] # third value is the traceback
    return "\n".join(traceback.format_tb(tb))


#-------------------------------------------
# DEBUG traps
#-------------------------------------------
__exceptionTriggers = {}    # here go the exception triggers set for debugging
                            # the application 
    
#---------------------------
def printHelpMod(module):
    """
    """
    #print dir(module)
    for function in dir(module):
        print()
        print(("----------- %s.%s --------------" % (module.__name__, function)))
        try:
            print((getattr(module, function).__doc__[:240]))
        except:
            print("(error)")

#-------------------------------
#
#-------------------------------
def testDebugTrap(triggerName):
    """
    tests the trap. If the name was introduced, raises
    the exception
    
    this code is put in the application
    """
    if triggerName in __exceptionTriggers:
        exception, params = __exceptionTriggers[triggerName]
        raise exception(*params)
     
#---------------------------------
#
#---------------------------------
def addDebugTrap(triggerName, exception, *params):
    """
    Sets the trap so the aplication crashes at the
    designated points
    
    this function is called by the unittest, before
    calling the application, 
    """
    __exceptionTriggers[triggerName]  = (exception, params)
    
#--------------------------
# breakpoint
#--------------------------
def breakpoint_(*args):
    print((" ".join([str(s) for s in args])))
    eval(input("?"))
    


#------------------
# xprint
#------------------
def xprint(*args):
    """
    prints along with the filenumber
    """
    frameinfo = getframeinfo(currentframe())
    sys.stdout.write("%s:%s:%s\n" % (frameinfo.filename, frameinfo.lineno,
             
                                     " ".join(args)))

#----------------------------
# dprint
#----------------------------
def dprint(string):
    """
    @param string string to be printed between horizontal markers
    """
    print("------------")
    print(string)
    print("------------")


#----------------------------
# iprint
#----------------------------
def iprint(string, i, j=None):
    """
    prints a string, marking the ith character
    @param string string
    @param i position of the the char wanted to highlight
    """
    print("------------")
    print(string)
    print((" " * (i) + "^"))
    print("------------")

#-----------------------
# testPoint
#-----------------------
def testPoint(string):
    """
    """
    global lastTestPoint
    t = "(%s)" % (time.time() - lastTestPoint) if lastTestPoint != None else ""
    lastTestPoint = time.time()
    print(("%s %s") % (string, t)); 
    sys.stdout.flush()
   
#---------------------
#
#---------------------
def display(string):
    print(string)

#-----------------------
# info
#-----------------------
def info(string):
    print(("INFO: " + string))

 
  
#----------------------
# error
#-----------------------  
def error(string):
    print("ERROR", string)
  
 


#======================
# TestPin
#======================
class TestPin:

    def __init__(self):
        self.reset()

    #------------------------------
    def log(self, message):
        """
        """
        self.plog.append(message)

    #-------------------------------
    def reset(self):
        self.plog = []
        self.data = {}

    #------------------------------
    def __setitem__(self, name, value):
        self.data[name] = value
        #self.log.append("~%s=%s" % (name, value))

    #------------------------------
    def __getitem__(self, name):
        """
        returns the value under a name
        """
        return self.data[name]

    #-----------------------------------
    def __str__(self):
        """
        """
        lines = []
        lines.append("------ data ------")
        for k in self.data:
            lines.append("%s : %s" % (k, self.data[k]))
        lines.append("------ log ------")

        for l in self.plog:
            lines.append(l)

        return "\n".join(lines)

    #------------------------------
    def contains(self, string):
        """
        """
        for l in self.plog:
            if string in l: return True

        return False





#======================
#            Report
#======================
class Report:
    """
    a class to do reports. accepts parameters and logs also things
    """

    #-----------------------------------------------
    def __init__(self, template):
        """
        """
        self.params = {}
        self.template = template
        self.logs = []
        self.subreports = []    # subreports

    #-----------------------------------------------
    def setParam(self, name, value):
        """
        """
        self.params[name] = value

    #-----------------------------------------------
    def getParam(self, name, default = None):
        """
        """
        return self.params.get(name, default)

    #---------------------------------------------
    def get(self, name, default = None):
        return self.getParam(name, default)

    #--------------------------------------------
    def set(self, name, value):
        self.setParam(name, value)

    #-----------------------------------------------
    def addSubreport(self, name , report):
        """
        """
        self.subReports.append([name, report])

    #-------------------------------------------------
    def log(self, note):
        """
        """
        self.logs.append(note)

    #-----------------------------------------------
    def __str__(self):
        """
        """

        ### 1) el template con los parametros

        lines = []

        template = self.template#-----------------------

        while 1:
            match = re.search(r"@[a-zA-Z_]+", template)
            if match == None: break
            name = match.group(0)[1:]
            value = self.params.get(name, "(name '%s'    not found) " % name)
            template = template.replace("@" + name, value)

        lines.extend(template.split("\n"))

        ### 2) los subreports:

        for name, report in self.subreports:
            lines.append("== %s ==" % name)
            lines.extend(str(report).split("\n"))

        ### 3) return

        return "\n".join(lines)


#-----------------------------------
# secondsToHuman
#-----------------------------------
def secondsToHuman(seconds):
    """
    12 months, 23 days, 3h and 5 seconds
    """

    if seconds < 1.0:
        if seconds > 1.0e-3:
            return "%4.4f ms" % (seconds * 1000.0)
        else:
            return "%4.4f us" % (seconds * 1e6)


    string = ""
    minutes = int(seconds / 60)
    hours = int(minutes / 60)
    days = int(hours / 24)
    months = int(days / 30)
    years = int(months / 12)

    if years != 0: string += "%i years, " % (years)
    if months > 0: string += "%i months, " % (months % 12)


    if years == 0 and months == 0:
        if days > 0: string += "%i days, " % (days % 30)
        if hours != 0: string += "%i hours, " % (hours % 24)
        if minutes != 0: string += "%i min, " % (minutes % 60)
        if seconds != 0: string = string[:-1] + "%i secs" % (seconds % 60)
    else:
        string = string[:-2] + " and %i days, " % (days % 30)


    return string


#====================
#     TestClock
#====================
class TestClock:
    """
    """

    def __init__(self):
        """
        """
        self.start()

    #--------------------------------------------
    def start(self):
        """
        """
        self._start = time.time()

    #---------------------------------------------
    def time(self, human = False):
        """
        """
        return self.stop(human)

    #--------------------------------------------
    def stop(self, human = False):

        if not human:
            return time.time() - self._start
        else:
            return self.__str__()

    #--------------------------------------------
    def __str__(self):
        """
        """
        return secondsToHuman(self.time())


#----------------------------
# silentStdStreams
#----------------------------
def silentStdStreams():
    """
    redirects stdout and stderr so msgs are no longer displayed
    """
    sys.stdoutCopy = sys.stdout
    sys.stderrCopy = sys.stderr
    sys.stdout = StringIO()
    sys.stderr = StringIO()


#---------------------------
# resetStdStreams
#---------------------------
def resetStdStreams():
    """
    resets stdout and stdin to its default values
    """
    sys.stdout = getattr(sys, "stdoutCopy")
    sys.stderr = getattr(sys, "stderrCopy")

#----------------------------
# triggerLog
#---------------------------
def triggerLog(level = MIN_LEVEL, msg = ""):
    """
    makes the logging branch active to display things
    """
    pass

#------------------------------
# armLogTrig
#------------------------------
def armLogTrig(level = MAX_LEVEL):
    """
    supress the logging output (setting the level to
    its highest value, to be ready for
    a trigger
    """
    pass

#------------------------------------
#    writeFile and readFile
#------------------------------------
def writeFile(filename , data):
    g = open(filename, "wb")
    g.write(data)
    g.close()

#-------------------------------
# readFile
#-------------------------------
def readFile(filename):
    f = open(filename, "rb")
    data = f.read(); f.close()
    return data


#------------------------------------
# returns a temporal name for whatever
#------------------------------------
def getTempName(prefix = ""):
    return "/tmp/file" + "".join([random.choice("abcdefghijklmnopqrstuvwxyz0123456789") for _ in range(5)])


#-----------------------------------
# makeTempDir
#-----------------------------------
def makeTempDir(prefix =""):
    path = getTempName()
    os.mkdir(path)
    return path


#----------------------------------------
#    getTemporalCopy
#----------------------------------------
def createWorkDir(templateDir, targetDir = None):
        """
        prepares the base by copying source dir in target dir. If not, just returns a name for the base
        """

        if targetDir == None:
            targetDir = getTempName()
        shutil.copytree(templateDir, targetDir)
        return targetDir



#---------------------------------
#    triggered debugging
#---------------------------------
triggers = {}

#--------------------------------
# trigger
#---------------------------------
def trigger(triggername, times = 1):
    global triggers
    triggers[triggername] = times

#------------------------------
# untrigger
#-------------------------------
def untrigger(triggername):
    global triggers
    del triggers[triggername]
    print(">>>><", triggers)

#-------------------------------
#
#---------------------------------
def trigDebug(message, trigger = None):
    global triggers

    if trigger == None or trigger in triggers:
        print()
        print("*************************")
        print(message)
        print("*************************")
        print()
        triggers[trigger] -= 1
        if triggers[trigger] == 0:
            del triggers[trigger]


#-----------------------------------------
# cut(s)
#-----------------------------------------
def cut(s):
    """
    cuts an string between the first and last -----
    very useful for string literals
    """
    s = s.split("\n")
    a = []
    while not s[0].startswith("------"):    s.pop(0)
    s.pop(0)
    while (not (s == [])) and (not s[0].startswith("------")):    a.append(s.pop(0))
    return "\n".join(a)


#-----------------------------------------
# lineCompare
#------------------------------------------
def lineCompare(str1, str2):
    """
    """
    if (not isinstance(str2, str)) or (not isinstance(str1, str)): 
        raise ValueError("not strs")

    while len(str1) < len(str2): str1 += "\n"
    while len(str2) < len(str1): str2 += "\n"

    str1 = str1.split("\n", -1)
    str2 = str2.split("\n", -1)

    print(len(str1), len(str2))

    for i in range(len(str1) ):
        print(str1[i].replace(" ", "·", -1))
        print(str2[i].replace(" ", "·", -1))
        print(". . . . . . . . . . . . . . . . ")




#=====================
#    TestToolsTestBase
#=====================
import unittest

class TestToolsTestCase(unittest.TestCase):

        #------------------------------------
        def setUp(self):
            """
            """
            pass

        #--------------------------------------
        def testMakeTestBench(self):
            """
            makes a list of children that are not mentioned in
            this children
            """
             
            

        #------------------------------------
        def test1(self):
            """
            """

#################################################

#-------------------------------------------
# getTestSuite
#-------------------------------------------
def getTestSuite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestToolsTestCase))
    return suite


#-------------------------------------
#-------------------------------------
if __name__ == '__main__':
        unittest.TextTestRunner(verbosity=2).run(getTestSuite())
