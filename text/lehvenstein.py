
INSERT, DELETE, REPLACE, EQUAL = "I", "D", "R", "="


#--------------------------------
# path
#--------------------------------
def path(m):
  """
  given a lehvenstein matrix for (s1,s2)
  returns the transformation instructions
  to get s2 from s1
  """

  path = []
  x = len(m[0])-1; y =len(m)-1

  while x > 0 or y > 0:
      direction = m[y][x][1]
      path.append(direction)

      if direction == INSERT: x -= 1
      elif direction == DELETE: y -=1
      elif direction == EQUAL or direction == REPLACE:
          x -=1; y-= 1
      else: raise "path symbol error"

  assert x == 0 and y == 0 # we arrive at nw (0,0) corner

  path.append(m[0][0][1]) # append the corner 0,0
  path.reverse()
  return path


#-----------------------
# lehvenstein
#-----------------------
def lehvenstein(txt1, txt2, iCost = 1, dCost = 1, rCost = 2, getPath = False):
  """
  lehvenstein
  """

  #if not isinstance(txt1, unicode):
  #    txt1 = unicode(txt1, "utf8"); 
   
  #if not isinstance(txt2, unicode):
  #    txt2 = unicode(txt2, "utf8")

  len1 = len(txt1); len2 = len(txt2)
  
  # 1 three trivial cases
  if len1 == 0 and len2 == 0:
      if getPath: return (0, [])
      else: return 0

  if len1 == 0 and len2 > 0:
      if getPath: return len2, INSERT * len2
      else: return len2

  if len2 == 0 and len1 > 0:
      if getPath: return len1, DELETE * len1
      else: return len1

  if getPath: m = []  # prepare a matrix to store the Rows

  # 2 first row
  
  Row =  [(0,INSERT)]*len2
  if  txt1[0] == txt2[0]: Row[0] = (0, EQUAL)  # first column
  else: Row[0] = (2,REPLACE)
  if getPath: m.append(Row) # save only if caller want path
  for j in range(1,len2): Row[j] = (iCost + Row[j-1][0], INSERT)
  oldRow = Row

  # 3 other rows
  
  for i in range(1,len1): # row loop
      Row = [0] * len2
      if getPath: m.append(Row) # save only if caller want path

      Row[0] = (oldRow[0][0] + dCost, DELETE)      # first column

      for j in range(1,len2): # col loop
          if txt1[i] == txt2[j]:
              Row[j] = (oldRow[j-1][0], EQUAL)
          else:
              up = (oldRow[j][0] + dCost, DELETE)
              nw = (oldRow[j-1][0] + rCost, REPLACE)
              left = (Row[j-1][0] + iCost, INSERT)
              Row[j] = min(up,left,nw)

      oldRow = Row

  # 4 get return type
  
  dist = Row[len2-1][0]
  
  if getPath:
      return dist, "".join(path(m))
  else:
      return dist
