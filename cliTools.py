import sys
import time
import re
import subprocess
from .stringTools import TextProgressMonitor
from .shellTools import colorize

############ INPUT FAMILY ################
  
dInputQueue = []
indentLength = 0

#------------------
# TextConsole
#------------------
class TextConsole:

    """
    implements a identable console
    """

    def __init__(self):
        self.depth = 0
        self.writeLn()

    def write(self, txt, depth=0):
        sys.stdout.write(txt)

    def input(self, prompt="", default=None, helpStr=None):

        if help is not None:
            prompt = prompt + "(" + helpStr + ")"

        s = xInput(prompt + " ")
        print(" " * self.depth, end=' ')
        if s == "" and default is not None:
            s = default

        return s

    def writeLn(self, str_="", indent=0):
        print(str_)
        self.depth = max(0, self.depth + indent)
        sys.stdout.write(" " * self.depth)

    def writeLine(self, str_="", indent=0):
        self.writeLn(str_, indent)

    def getProgressMonitor(self):
        return TextProgressMonitor(50)

    def indent(self):
        self.writeLine("", 4)

    def dedent(self):
        self.writeLine("", -4)




#--------------------
def getTermSize():
    """
    returns the size of the terminal    
    """
    try:
        rows, columns = subprocess.check_output(['stty', 'size']).split()
    except:
        rows, columns = 20,80
    return int(rows), int(columns)
    
#------------------
# xInput
#------------------
def xInput(prompt, inject = None):
    """
    keyboard input allowing for debug
    @param inject answer, used to debug
    """
    if len(dInputQueue) != 0:
        return dInputQueue.pop(0)
    
    elif inject != None:
        return inject
    
    else:
        return input(prompt)


def display(s):
    print(s)


#--------------------
#  readString
#--------------------
def readString(prompt, default=None, condition=lambda x: True):
    """
    reads an int from the console
    """
    while True:
        s = prompt

        if default:
            s += "[" + default + "]"

        s = xInput(s)

        if (s == "") and default is not None:
            s = default
            print(s)

        if condition(s):
            break  # break only if pass condition

    return s

 



#---------------------
#   readInt
#---------------------
def readInt(prompt, minValue=None, condition = lambda x: True, default = None):
    """
    reads an int from the console
    """
    while 1:
        s = xInput(prompt)
        try:
            if s == "": raise ValueError()  # eval fails on empty strings
            i = int(eval(s));
            if minValue and i < minValue:
                display(f"minimum value is {minValue}")
                continue
            
            if condition(i): break # break only if pass condition
        except ValueError:
            if s == "" and default != None:
                display(f"default value {default} taken")
                return int(default)
        except SyntaxError:
            if s == "" and default != None:
                return int(default)
            else:
                print("syntax error")
        
    return i


#------------------------
#   readFloat
#-----------------------
def readFloat(prompt, default = None, minValue=None, condition = lambda x: True):
    """
    reads an int from the console
    """
    if default != None: prompt += "[%s]" % default
    while 1:
        s = xInput(prompt)
        try:
            if s == "" and default != None:
                return default # whichever it is
            i = float(s);
            if minValue and i < minValue:
                display(f"minimum value is {minValue}")
                continue

            if condition(i): break # break only if pass condition
        except ValueError:
            pass
    return i


#-----------------------------
#  readYn
#-----------------------------
def readYN(prompt, default=None):
    """
    """
    while 1:
        x = input(prompt)
        if x == "":
            if default != None: return default

        x = x.strip().lower()
        if x == "yes" or x == "y":
            return True
        elif x == "no" or x == "n":
            return False

def readYn(prompt, default= None):
    readYN(prompt, default)

def readBool(prompt, default = None):
    """
    """
    return readYN(prompt, default)

def confirm(prompt, default = None):
    """
    """
    return readYn(prompt, default)

#------------------------------------
def parseTime(s):
        """        
        """
        def part2(text, separator):
            a, s, b = text.partition(separator)
            if s == "":
                return b, s, a
            else:
                return a, s, b
        
        days, separator, rest = part2(s, "d")        
        days = int(days) if separator != "" else 0
        hours, separator, rest = part2(rest, "h")
        hours = int(hours) if separator != "" else 0
        mins, separator, rest = part2(rest, "m")
        mins = int(mins) if separator != "" else 0
        secs, separator, rest = part2(rest, "s")
        secs = int(secs) if separator != "" else 0
        
        if rest.strip() != "":
            raise ValueError("wrong format for time (must be in dhms format")
        
        x = secs + mins*60 + hours * 3600 + days * 86400
        return x

#-----------------------------
def readTime(prompt, default = None, inject = None, filter_ = lambda x: True):
    """
    reads time in the form 33d23h12m21s
    """
        
    while 1:
        s = readString(prompt, default, inject = inject)
        try:
            x = parseTime(s)
            if not list(filter_(x)):
                print("error")
                continue 
            return x
        
        except ValueError:
            print("wrong format: expected .d.m.h")
    
#---------------------------------------    
def readDate(prompt, default = None, inject = None):
    while 1:
        s = readString(prompt, default, inject = inject)
        try:
            return time.mktime(time.strptime(s, "%d/%m/%Y"))            
        except ValueError:
            print ("wrong format. Must be dd/mm/yyyy")
        
#---------------------
#  readFormat
#---------------------
def readFormat(prompt, sFormat, default = None):
    """
    reads an int from the console
    """
    while 1:
        s = prompt

        if default:
            s += "[" + default + "]"

        s = xInput(s)

        if (s == "") and default != None:
            s = default
            print( s )

        if re.match(sFormat, s):
            break
        else:
            print("wrong format")

    return s
  
#------------------------------------
# inputChoice
#------------------------------------
def inputChoice(prompt, choices):
    """
    """
    while 1:
        s = input(prompt).lower().strip()
        if s in choices:
            return choices[s]

      
#------------------------------------
# inputInt
#------------------------------------
def inputInt(prompt, test = lambda x:True, default = None, shortcuts = None):
    """
    """
    if shortcuts == None: shortcuts = {}
    while 1:
        s = input(prompt).lower().strip()
        s = shortcuts.get(s, s)
        if s == "" and default != None:
            return int(default)
  
        try:
            if test(int(s)): return int(s)
            continue
  
        except:
            continue
        
#----------------------
#
#----------------------

def menu(opciones):
    """
    """
    l = len(opciones)
    for o in range(1, l + 1):
        print(str(o) + ") " +\
            opciones[o - 1])

    while True:
        try:
            i = int(xInput("?"))
            if i > 0 and i <= l:
                break
        except ValueError:
            pass

    return i - 1


def displayTitle(s):
    print(colorize(f"========[ {s.upper()} ]=========", "red"))
    
def display(s):
    print(" " * indentLength + s)
    
def indent():
    global indentLength
    indentLength += 2

def dedent():
    global indentLength
    indentLength -= 2
