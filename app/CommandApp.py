import inspect
import sys

from softap3.app.SimpleApp import SimpleApp, AppException
from softap3.common import getTraceback, colorize

def indent(string, i=4):
    return "\n".join([" " * i + s for s in string.split("\n")])

#======================
#    CommandApp
#======================
class CommandApp(SimpleApp):
    """
    An application that accepts commands via the command line and
    optionally through a menu or a remote XMLRPC connection
    
    app behaviour depends on the first parameter (an argument) that acts as 
    a command
    
    Commands handlers can be the functions themselvs or objects. Objects group 
    functions that act as commands 
    
    cmd are functions that receive the arguments and parameters. 
    Configuration is appart
    
    """

    #---------------------------------------------------
    def __init__(self, name, openMenu = True, daemon = False, rcPort = None):
        """
        @param openMenu setting it to True allows for user interactively 
                     issuing commands AFTER the param lines
                     
        @param daemon set to true, runs the command in the background
        @param rcPort if a value is given, opens a remote control in the given 
                     port to accept commands
        """
        
        SimpleApp.__init__(self, name, daemon)
        
        self.commandHandlers = {"interactive": self.interactive}  # stores the functions
        self.defaultCommand = None     # to have a default command
        self.defaultInteractive = True # will go to interactive if no command given
        self.prompt = "> "             # prompt for the interactive command
        
        self.helpTopics    = {}     # dictionary with pairs name:help for 
                                    # additional help topics, apart from commands
                                    # (that contain its own help)
 
    #-----------------------------------------------------
    def addCommandsByPrefix(self, prefix):
        """
        """
        for f in [f for f in dir(self) if f.startswith(prefix) and callable(getattr(self, f))]:
            self.addCommand(f[len(prefix):], getattr(self, f))
        
    #----------------------------------------------------
    def main(self, lineArgs, configFiles = None, configFileOption = None):
        """
        main function. looks for a command as the first argument and lets
        the parameter parsing to the command
        @param lineArgs
        """
        #---------------------------
        def splitCommands(lineArgs):
            commands = []
            current = []
            for i in lineArgs:
                if i == "::":
                    commands.append(current)
                    current = []
                else:
                    current.append(i)
                    
            commands.append(current)
            return commands
        
        
        # 1) call init
        
        self.init()     # calls user init before any command
        
        # 2) is interactive?
        
        if lineArgs == []:
            args = []; params = {} 
            if self.defaultCommand != None:
                command = self.defaultCommand
                    
            elif self.defaultInteractive == True: 
                command = "interactive"
                    
            else:
                self.fatalError("no command given")
                print((self.usage))                    
                sys.exit(-1)
                        
        else: # extract command argument and parameters
            args, params = self.parseCommandLine(lineArgs, 
                                                 allowRepeats = self.allowDupParams) 
            #print args, params
            if len(args) > 0: 
                command = args.pop(0)
            else: 
                command = ""
                                    
                                    
        # at this point we have a command , arguments and parameters            
        # 1) try if the application has some global parameters
        
        for name, value in list(params.items()):
            if hasattr(self, "paramset_" + name):
                    getattr(self, "paramset_" + name)(value)
                    params.pop(name)

        # 2) pass ctrl to the command
        
        try:            
            self.command(command, args, params)
                     
        except KeyboardInterrupt as e:
            raise
            self.error("user break")
                       
        except AppException as e:
                raise
                self.error(str(e))
                
        except BaseException as e:
                if self.debugMode: raise
                self.error("Uncatched exception (%s)" % e)
                print((getTraceback()))
                
        finally:
                self._finalize()    # this is the true chained finalize 

            
    #---------------------------
    def interactive(self):
        """
        an interactive menu
        """
        
        lastUserInput = None        
        
        try:            
            while 1:
            
                try:
                    userInput = input(self.prompt).strip()
                     
                    if userInput == "!":
                        if lastUserInput == None: 
                            self.error("no last command")
                            continue
                        else:
                            print((self.prompt + lastUserInput))
                            userInput = lastUserInput
                     
                    lastUserInput = userInput
                    args, params = self.parseCommandLine(userInput)
                    if len(args) == 0:
                            continue
                             
                             
                    command = args.pop(0)
                                        
                    if self.hasCommand(command):
                        self.command(command, args, params)
                    else:
                        self.unknownCommand(command, args, params)
                        #sys.exit(-1)
                 
                except KeyboardInterrupt as e:                    
                    self.info("user break...")
                    break
                
                except EOFError as e:
                    self.info("user break...")
                    break
             
                except TypeError as e: # wrong number of parameters
                    if self.debugMode:
                        print(( "reraising %s while in debugMode" % e))
                        raise
                    print(("error on command %s: %s" % (userInput, e)))
                    print((getTraceback()))
                         
                    print((self.cmd_help(command)))
             
                except AppException as e:
                    if self.debugMode: 
                        print(("reraising %s because debugMode" % e))
                        raise
                    print(("error on command %s: %s" % (userInput, e)))
                 
                 
             
                except BaseException as e :
                    if self.debugMode: 
                        raise
                        print(("reraising %s because debugMode" % e))
                        print((getTraceback()))
                        raise
                     
                    print(("uncatched error (%s)" % e))
                    print((getTraceback()))
                            
        finally:
            print("finalizing...")
            self._finalize() # this is the true chained finalizer
            
        
    
    #-------------------------
    # command-related functions
    #------------------------- 
    def addCommand(self, name, obj, alias = None):
        """
        adds a function or an object (a SimpleApp) as command
        when the command is invoked, the arguments and params will be processed 
        depending of the handler
        
        if the handler is a function, then 
        if the handler is a SimpleApp, then takes it as a command
        """
        if isinstance(name, str): name = [name]
        if alias == None: alias = []
        for n in name + alias:
            if n in self.commandHandlers: 
                self.warning("command '%s' already exists (%s)" % \
                                         (name, str(list(self.commandHandlers.keys()))) )
            self.commandHandlers[n] = obj
    
    #---------------------------------------------
    def addAllCommands(self, obj):
        """
        adds all the available commands in an object
        """
        for i in dir(obj):
            if i.startswith("cmd_") and callable(getattr(obj, i)):
                self.addCommand(i[4:], getattr(obj, i))

    #---------------------------------------------
    def getCommands(self):
        """
        returns a list with the available commands
        """
        s = [s[4:] for s in dir(self) if s.startswith("cmd_")]
        s.sort()
        return s
    
                
    #---------------------------------------------
    def command(self, command, args, params):
        """
        argparams is the command line split into chunks. No arg/param 
        processing done yet
        @command
        @command        
        """        
        if command in list(self.commandHandlers.keys()):
            
            handler = self.commandHandlers[command]
            
            if inspect.isclass(handler):
                raise ValueError("handlers must be objects, not classses")
            if isinstance(handler, SimpleApp):    # handler is a simple app
                argparams = args + [("--%s=%s" % (x, params[x])) for x in list(params.keys())]
                #print "passed" , argparams, "to", handler
                return handler.main(argparams)    
         
            elif callable(handler):    # handler is just a function
                return handler(*args, **params)            
                     
            else:
                raise ValueError("wrong type for command handler")
                           
        elif hasattr(self, "cmd_" + command) and\
            callable(getattr(self, "cmd_" + command)):
            getattr(self, "cmd_" + command)(*args, **params)
            
        else:
            self.unknownCommand(command, args, params)
            sys.exit(-1)    # exit the programm
            
            
    #---------------------------------------------
    def hasCommand(self, command):
        """
        returns True if <command> exists
        """
        
        if command in self.commandHandlers: 
            return True
        elif hasattr(self, "cmd_" + command) and\
                 callable(getattr(self, "cmd_" + command)):
            return True
        else:
            return False
     
    
    #---------------------------------------------
    def unknownCommand(self, command, args = None, params = None):
        """
        the fallback function for dealing with unknown commands
        Can be overridden 
        """
        print((colorize("unknown command '%s'" % (command,), "red")))
        print((colorize("     (availableCommands:%s)" % (", ".join(self.getCommands())), "white")))
        
        
    #-------------------------------
    # some default commands
    #--------------------------------
    def cmd_help(self, command = None):
        """
        """
        
        if command == None or command == "":
            lines = []
            lines.append("available commands: (use help <command> for info) " +
                                                                     ", ".join(self.getCommands()))
            if list(self.helpTopics.keys()) != []:
                lines.append("")
                lines.append("other topics:" + ", ".join(list(self.helpTopics.keys())))
            
            lines.append("")                
            print((colorize("\n".join(lines), "white")))
            return
            
        if command in list(self.commandHandlers.keys()) :
            handler = self.commandHandlers[command]
            
            if isinstance(handler, SimpleApp):
                print((indent(handler.help, 4)))
                
     
            elif callable(handler):         
                s = getattr(self, "cmd_" + command).__doc__
                    
                if s != None and "[help]" in s:
                        
                    t = s[s.find("[help]") + 7:].split("\n")
                    i = len(t[0]) - len(t[0].lstrip())
                    s = "\n".join([l[i:] for l in t])
                    print()
                    print((colorize(s, "white")))
                    
                else:
                    print(("no specific help for command '%s'" % command        ))
        
        elif hasattr(self, "help_" + command) and    \
                         callable(getattr(self, "help_"+ command)): 

                s = getattr(self, "help_" + command).__doc__
                    
                if s != None:
                    print(s)
                else:
                    print(("no specific help for command '%s'" % command        ))                
                
        elif command in self.helpTopics:
            print((indent(self.helpTopics[command], 4)))
        
        else:
            print(("command %s not found. Available commands: %s" %
                                                                             (command, ", ".join(self.getCommands()))))
            sys.exit(-1)
    
    
        
        
