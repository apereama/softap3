import unittest

#############################
#  TESTS
############################

# ------------------------------------------
from softap3.geom3d.Vector import *
from softap3.geom3d.Plane import Plane
from softap3.geom3d.Segment import Segment


# ===============
#
# ================
class PlaneTestCase(unittest.TestCase):
    """
    tests sobre otras funciones de la base
    """

    def setUp(self):
        pass

    # --------------------------
    def tearDown(self):
        pass

        # ---------------------------

    def test1(self):
        """
      """
        p = Plane(Point(2, 2, 2), Vector(2, 3, 2))
        l = Segment(Point(0, 0, 0), Point(4, 3, 2))
        a = p.intersection(l)
        print(a)
        print(p.distance(a))


if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(PlaneTestCase))
    unittest.TextTestRunner(verbosity=2).run(suite)
