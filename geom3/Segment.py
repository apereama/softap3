from Point3 import Point3

#====================
# Segment
#====================
class Segment:
  """
  """
  
  #---------------------------
  def __init__(self, p1, p2, isLine = False):
    """
    """
    self.p1 = p1
    self.p2 = p2
    self.u = (self.p2 - self.p1).normalize()  # vector direccion
    self.isLine = isLine  # set to true to represent an authentical line
    
  #-----------------------------
  def along(self, k):
    """
    """
    return k * p1 + (1-k) * p2
    
  #------------------------------
  def distance(self, other):
    """
    """
    if isinstance(other, Point):
      raise "notImplemented"
      
    elif isinstance(other, Segment):
      v = other.p1 - self.p1
      a = self.u * (self.u * v); b = other.u * (other.u * v)
      return (v - a - b).modulus()
    else:
      raise ValueError("unknown object type")

  #-------------------------------
  def midPoint(self, factor=0.5):
    """
    """
    return self.p1 * factor + self.p2 * (1.0 - factor)
  
  #-------------------------------
  def solveForZ(self, z):
    """
    """
    pass
  
  #-------------------------------
  def solveForX(self, x):
    """
    """
    pass
  
  #-------------------------------
  def soveForY(self, y):
    """
    """
    
    
  