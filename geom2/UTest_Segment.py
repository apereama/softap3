#!/usr/bin/python
# -*- coding: utf-8 -*-
# ====================
#  HURA 0.9
# ====================
#
#
# ====================
#  License
# ====================
#
#    Hura v0.9
#    Copyright (C)2013 Angel Perea Martinez ( angelpeream @ yahoo.com )
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
#    MA  02110-1301  USA
#
# (see gpl.txt for the complete licence text).
#

import unittest
from softap3.geom2.Segment import Segment
from softap3.geom2.Point2D import Point2


#===========================
# THTestCase
#===========================
class SegmentTestCase(unittest.TestCase):
  """
  test of the pushkin facilitiess
  """

  #----------------------------------------
  def setUp(self):
    pass

  #-----------------------------------------
  def tearDown(self):
    pass

  #-----------------------------------------
  def test1(self):
    """
    . o x
    . . .
    x . o
    """
    a = Segment(Point2(0.0,0.0), Point2(2.0,2.0))
    b = Segment(Point2(1.0,2.0), Point2(2.0,0.0))
    self.assertEqual(a.intersection(b)[0], Point2(1.33333333333, 1.33333333333))
    self.assertEqual(a.intersection(b)[1], 0.3333333333333333)
    self.assertEqual(a.intersection(b)[2], 0.6666666666666666)
    

    # case 2. Segment 2 points but not touch
    a = Segment(Point2(0.0,0.0), Point2(5.0,2.0))
    b = Segment(Point2(3.0,8.0), Point2(3.0,3.0))
    self.assertEqual(a.intersection(b)[0], None)

    # case 3. parallel
    a = Segment(Point2(0.0,0.0), Point2(2.0,2.0))
    b = Segment(Point2(1.0,0.0), Point2(3.0,2.0))
    self.assertEqual(a.intersection(b)[0], None)

#------------------------------
#        getTestSuite
#------------------------------
def getTestSuite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(SegmentTestCase))
    return suite



if __name__ == '__main__':
   unittest.TextTestRunner(verbosity=2).run(getTestSuite())