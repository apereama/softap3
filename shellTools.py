"""
Utilities for everything related to application and the shell

@TODO <description of the module>

:copyright: 2016 by Angel Perea Martinez (angelpeream@yahoo.com), see AUTHORS
            for more details

:license: lGPLv3, see LICENSE for more details
"""
import re
import unittest
import subprocess
import os
import sys

from .stringTools import colorize
from .devTools import getTraceback

#---------- those are values for sysexit.h -----------------

EXITCODE_OK = 0              # successful termination
EXITCODE_BASE = 64           # base value for error messages
EXITCODE_USAGE = 64          # command line usage error
EXITCODE_DATAERR = 65        # data format error
EXITCODE_NOINPUT = 66        # cannot open input
EXITCODE_NOUSER = 67         # addressee unknown
EXITCODE_NOHOST = 68         # host name unknown
EXITCODE_UNAVAILABLE = 69    # service unavailable
EXITCODE_SOFTWARE = 70       # internal software error
EXITCODE_OSERR = 71          # system error (e.g., can't fork)
EXITCODE_OSFILE = 72         # critical OS file missing
EXITCODE_CANTCREAT = 73      # can't create (user) output file
EXITCODE_IOERR = 74          # input/output error
EXITCODE_TEMPFAIL = 75       # temporary failure; user is invited to retry
EXITCODE_PROTOCOL = 76       # remote error in protocol
EXITCODE_NOPERM = 77         # permission denied
EXITCODE_CONFIG = 78         # configuration error
EXITCODE_BREAK = 79          # this one added for edn


def fatalError(string, exitCode=0, helpString=None,
               logger=None, writeLog=None):
    """
    Prints an error message and optionally aborts the application. It is not
    related with logger.error. This is meant to inform the user, while
     the logger deals with log-files

    @param abort: if different to 0, the program exits. Its value will be the exit code.

    """
    if logger is not None:
        logger.error(colorize("FATAL ERROR:", "lightred") + string)
    else:
        print(colorize("FATAL ERROR: " + string, "red"))

    if helpString is not None:
        print()
        print(helpString)

    if writeLog is not None:
        logError("")

    if exitCode != 0:
        sys.exit(exitCode)  # exit code is passed in the abort param


def logError(exception, filename="error.log"):
    s = "writing error details at '%s'" % (os.getcwd() + "/" + filename,)
    print(s)
    g = open(filename, "w")
    g.write(str(exception) + "\n")
    g.write("============\n")
    g.write(getTraceback())
    g.close()


def parseCommandLine(lineArgs, allowRepeats = False):
    """
    parses command line arguments transforming into arguments and parameters         
    """        
    
    # 1) args
    if isinstance(lineArgs, str): lineArgs = lineArgs.split(" ")
    lineArgs = [l for l in lineArgs if str(l) != ""]       
    args = [x for x in lineArgs if not x.startswith("-")]
    
    # 2) params    
    pList = [x for x in lineArgs if x.startswith("--") or x.startswith("-")]
    params = {}
    
    for p in pList:
        if p.startswith("--"): p = p[2:]
        if p.startswith("-"): p = p[1:]
            
        if "=" in p:
            a, b = p.split("=", 1)
            name, value = a,b
        else:
            name, value = p, True
            
        if name in params:
            if allowRepeats:
                if not isinstance(params[name], list):
                    params[name] = [params[name]]
                params[name].append(value) 
            
            else:
                raise Exception("repeated parameter %s" % name)
        else:
            params[name] = value
                                                    
    return args, params
   
   
def bashCommand(command):
    """
    issues a bash command
    """
    p = subprocess.Popen(command, shell=True)
    _pid, sts = os.waitpid(p.pid, 0)
    return sts


def bashCall(path):
    """
    Allows the caller to make a bash call
    and returns the exit conde and the standard outputs
    """
    p = subprocess.Popen(path, shell=True, stdin=subprocess.PIPE,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                         close_fds=True)

    p.stdin.close()
    stdout, stderr = p.stdout.read(), p.stderr.read()
    exitCode = p.wait()
    return exitCode, stdout, stderr


def splitBash(text):
    """
    split the command line in tokens, according with the bash rules
    for quotes and spaces ("param", \", \ )
    @param text the text as given in the console
    """
    tokens = []
    
    while text.strip() != "":
        a, text = __consumeToken(text)
        tokens.append(a)
    return tokens


def cmdlineToTuple(string):
    """
    @param the input from the console, as an string
    @return a tuple (command, args, params)
    """
    tokens = splitBash(string)
    if tokens == []:
        return ("", [], [])
    command, tokens = tokens[0], tokens[1:]
    args, params = parseBashArguments(tokens)
    return command, args, params


def parseBashArguments(tokens):
    """
    given a list, returns a dictionary with params and a list with the remaining arguments
    params are on the form -n, -n:value, -n=value -nvalue, --name:value, --name=value
    @param tokens
    @return a pair (list, dict). The dictionary contains the named params
    """
    
    args = []
    params = {}

    while tokens != []:
        p = tokens.pop(0).strip()

        if p.startswith("--"):
            e = "--([^:= ]+)([:=].+)?"
            m = re.match(e, p)
            if m is None:
                raise ValueError("bad format in long param '%s'" % p)
            name = m.group(1)
















































































































            value = m.group(2)[1:] if m.group(2) is not None else ""
            if value == "":
                value = True
            params[name] = value

        elif p.startswith("-"):
            e = "-(.)([:=].*)?"
            m = re.match(e, p)
            if m is None:
                raise ValueError("bad format in short param '%s'" % p)
            name = m.group(1)
            value = m.group(2)[1:] if m.group(
                2) is not None else ""  # strip = or :
            if value == "":
                value = True
            params[name] = value

        else:
            args.append(p)

    return args, params


def __consumeToken(text):
    """
    gets the first token, no matter if it's quoted (with, may be, scaped quotes inside)
    or is a word (with, may be, scaped space chars inside)
    @return the first bash token in a string
    """

    pattern = r"""('.*?(?<!\\)' *)|(".*?(?<!\\)" *)|(.*?(?<!\\)( +|$))"""
    #           \-------------/   \------------/   \--------------/
    #                 |                  |                +--------- (1)
    #                 |                  +-------------------------- (2)
    #                 +----------------------------------------------(3)
    #
    # (1) or a space separated param taking care of \<space>
    # (2) or a quotes delimited with care to \"
    # (3)

    m = re.match(pattern, text)

    param = m.group(0).strip()
    rest = text[len(m.group(0)):]

    if param.startswith('"') or param.startswith("'"):
        param = param[1:-1]
    param = param.replace(r'\"', '"')
    param = param.replace(r'\ ', ' ')

    return param, rest


class BashParseTestCase(unittest.TestCase):

    """
    Test the writting back to disk of the tree
    """

    #-------------------------

    def setUp(self):
        """
        """

    #-------------------------
    def tearDown(self):
        pass

    #--------------------------
    def test1(self):
        """
        """
        self.assertEqual(
            cmdlineToTuple("""cmd arg1 "arg 2" --param1=value1 --param2=value2""", ""))


def getTestSuite():

    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(BashParseTestCase))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=2).run(getTestSuite())

