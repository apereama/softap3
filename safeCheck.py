import sys

TOLERANCE = 1E-7



#----------------------------
# safeStr
#----------------------------
def safeStr(x, default=None):
    """
    """
    if x is None and default is not None:
        return default
    else:
        return x


#-------------------------------
# safeDiv
#-------------------------------
def safeDiv(x, y, default=0.0):
    """
    """
    if y == 0:
        return default
    else:
        return float(x) / y


#------------------------------
# safeFloat
#------------------------------
def safeFloat(number, default):
    """
    """
    try:
        return float(number)
    except:
        if default is not None:
            return float(default)
        else:
            raise


#------------------------------
# safeInt
#------------------------------
def safeInt(number, default):
    """
    """
    try:
        return int(number)
    except:
        if default is not None:
            return int(default)
        else:
            raise


#-----------------------------
# safeBool
#----------------------------
def safeBool(value, default):
    """
    """
     
    if value in (True, False):
        return value    
    if value == 0:
        return False
    s = str(value).strip().lower()
    if s in ["true", "si", "yes", "1"]:
        return True
    elif s in ["false", "no", "0"]:
        return False
    
    if not default in (True, False):
        raise ValueError("boolean default not provided and value '%s' is inconsistent" % value)
    else:
        return default


#----------------------
# guardNone
#----------------------
def guardNone(a, default):
    """
    """
    return default if a is None else a
 

#---------------------------
# Int
#---------------------------
def checkInt(number, exception, *args, **params):
    try:
        return int(number)
    except:
        if issubclass(exception, Exception):  # !!!
            raise exception(*args, **params)
        elif callable(exception):
            exception(*args, **params)
        else:
            raise ValueError("wrong parameter exception")


#-----------------------------
#  bashProtect
#-----------------------------
def bashProtect(string):
    """
    protects the string so it can be put inside a bash command
    @string command or name wanted to be protected against mismatched quotes, backslashes, ...
    """
    if " " in string:
        return "'%s'" % string
    else:
        return string


#--------------
#
#----------------
def parseInt(value, default=None, warning=None, error=None, fatalError=None):
    """
    """
    try:
        return int(value)
    except:
        if default != None:
            if warning != None: print(("warning: %s" % warning))
            return default
        else:
            if fatalError != None:
                sys.exit(fatalError)
            if error != None:
                raise ValueError("error")
   

#-----------------------
#  asBoolean
#-----------------------
def parseBoolean(value, onEmpty = None, onWrong = None):
    """
    """
   
    if value in [True, False]: return value
      
    if isinstance(value, str):
        value = str(value).lower().strip()
        allowed = {"true":True, "false":False, "yes": True, 
                 "no": False, "on": True, "off": False}
      
        if value in allowed: 
            return allowed[value]
        else:
            if value == "" and onEmpty != None: return onEmpty
            if onWrong != None: return onWrong # default value if wrong
            raise ValueError("wrong value '%s' for boolean" % value)
    




############### GUARDS ###############################

#----------------------------
# guard
#----------------------------
def guard(x, minValue, maxValue = None):
    """
    returns x if x between mixValue and maxValue
    if minValue is None, x won't be trimmed from below
    if maxValue is None, x won't be trimmed from above
    """
    if minValue == None: minValue = x
    if maxValue == None: maxValue = x
    #print(f"guard {x} from {minValue} to {maxValue}")
    return min(max(x, minValue), maxValue) # guard




#---------------------------------------------
def safeIndex(vector, index, default = None, warning = None):
    """
    """
    if index < 0: raise ValueError("index is smaller that zero")

    if len(vector) > index :
        return vector[index]
    else:
        if default != None:
            return default
        raise ValueError("index is higher than vector length")
  
      
 
 
 

safeString = safeStr
 
#---------------------------
# safeOp
#---------------------------
def safeOp(defaultValue, function, *params, **args):
    """
    """
    try:
        return function(*params, **args)
    except:
        return defaultValue

#--------------------------
# select
#--------------------------
def select(a,b):
    """
    returns b if a == None
    """
    if a == None:
        return b
    else:
        return a
  
  
###########################################
#          check family
###########################################

#------------------
#
#------------------
def isInt(value):
    """
    """
    try:
        _ = int(value)
        return True
    except:
        return False
   
#-----------------------
def checkRange(number, minVal, maxVal):
    """
    """  
    number = int(number)  
    return isInt(number) and number <= maxVal and number >= minVal


#-----------------------
# safeCall
#------------------------
def safeCall(default, fname, *args, **params):
    """
    safely calls a function, returning a result if an exception is raised
    """
    try:
        return fname(*args, **params)
    except:
        return default

  
#-------------------------
#
#--------------------------
def checkIntRange(value, low, high, exception, *args, **params):  
    """
    """
    if issubclass(exception, Exception):  
        if (value < low) or (value > high):      
            raise exception(*args, **params)
    elif callable(exception):
        if (value < low) or (value > high):      
            exception(*args, **params)
    else:
        raise ValueError("wrong parameter exception")
    
