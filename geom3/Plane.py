from softap3.geom3d.Vector import *
 
 
#-----------------------
class Plane:
  
  #--------------------------
  def __init__(self, origin, p2, p3):
    """
    """
    self.o = origin
    self.u, self.v = p2-self.o, p3-self.o
    
  #-------------------------
  def point(self, x, y):
    return self.o + self.u * x + self.v * y

  #-----------------------
  def polarPoint(self, angle, radius):
    x, y = radius * math.cos(angle), radius * math.sin(angle)
    return self.point(x,y)
  
  
#=========================
#  Plane
#=========================
class Plane2:
  """
  """
  
  #---------------------------------
  def __init__(self, a,b,c = None, up = None):
    """
    """
    if up == None: up = (0,0,1)
    
    if c == None: # point and normal
      p = a
      n = b
      
    else: # three points
      p = a
      n = (b - a) ** (c -a)
       
         
    self.p = p
    self.n = n
    
    self.up = self.project(up, relCoords = True).normalize()
    self.right = (self.n * self.up).project(relCoords = True).normalize()
    
    
  
  #----------------------------------
  def project(self, point, relCoords = False):
    """
    returns the coordinates of the point projected in the plane
    """
    if not relCoords: 
      v = (point - self.p)
    else: # is a vector starting in p
      v = point
    
    return self.p +  (v - (self.n * v))
  
  #----------------------------------
  def project2D(self, point):
    """
    returns the 2D coordinates of the point in the plane, 
    using the up vector provided in the contructor or here
    """ 
    point = self.project(point)
    v = point - self.p # relative to the origin of the plane
    x, y = self.right * v, self.up * v
    return x,y
    
  #-----------------------------------
  def distance(self, other):
    """
    distance to others
    """
    
    if isinstance(other, Vector):
      return self._distPoint(other)
    elif isinstance(other, segment):
      return self.distSegment(other)
      
  #------------------------------------
  def _distPoint(self, point):
    """
    """
    a = (point - self.p)
    distance =  a * self.n.normalize()
    return distance
  
  #-----------------------------------
  def _distSegment(self, segment):
    """
    """
    raise NotImplementedException()
  
  #-----------------------------------
  def intersection(self, segment):
    """
    returns the intersection 
    """
    u = (segment.p2 - segment.p1).normalize()
    a = segment.p1
    
    # condition is
    # t = (a + u * k)
    # (t - p_plano) * n = 0
    
    # (ax + ux * k - px) * nx + (ay + uy * k - py) * ny + (az + uz * k - pz) * nz = 0
    
    # ax * nx + ux * k * nx - px * nx +
    # ay * ny + uy * k * ny - py * ny +
    # az * nz + uz * k * nz - pz * nz  = 0     
    # k = ((px * nx + py * ny + pz * nz) - (ax * nx + ay * ny + az * nz) ) / (ux * nx + uy * ny + uz * nz) 
    
    alpha = self.p * self.n; beta = a * self.n; gamma = u * self.n
    k = (alpha - beta) / gamma
    #print a, u, k
    t = a + u * k    
    return t
    
  #--------------------------------