"""
@TODO
"""

from softap3.geom2.Point2 import Point2
from softap3.listTools import getPairs


class UniversalCanvas:
    """
    A universal canvas. Descendants of this
    class allow to draw in different formats (SVG, cairo, etc)
    """

    def __init__(self):
        """
        """
        self.style = None
        self.sx, self.dx = 1.0, 0.0
        self.sy, self.dy = 1.0, 0.0

    def setCoords(self, p1, p2, q1, q2):
        """
        sets the coords for the canvas
        """
        self.sx = (q2.x - q1.x) / (p2.x - p1.x)
        self.sy = (q2.y - q1.y) / (p2.y - p1.y)

    def transform(self, *points):
        """
        @TODO
        """
        r = []
        for p in points:
            r.append(Point2(p.x * self.sx + self.dx, p.y * self.sy + self.dy))

        return r[0] if len(r) == 1 else r

    def line(self, p1, p2, style=None):
        """
        @TODO
        """
        raise NotImplementedError("not implemented")

    def pline(self, points, style=None):
        """
        @TODO
        """
        for p, q in getPairs(points, cycle=True):
            self.line(p, q, style)

    def point(self, p, style=None):
        """
        @TODO
        """
        raise NotImplementedError("not implemented")

    def bezier(self, points, style=None):
        """
        @TODO
        """
        raise NotImplementedError()

    def arc(self, p1, p2, p3, style=None):
        """
        @TODO
        """

    def setStyle(self, style):
        """
        @TODO
        """
        raise NotImplementedError("not implemented")

    def write(self, x, y, text, align="e", style=None):
        """
        writes text
        """
        raise NotImplementedError("not implemented")
