from Point2D import Point2D as Point
from math import cos, sin
#======================
# Turtle
#======================
class Turtle:
  """
  """
  def __init__(self):
    """
    """
    self.pos = Point(0,0)
    self.angle = 0
  
  #------------------------------
  def turn(self, angle):
    self.angle -= angle   # because turning right is decreasing angle
  
  #-----------------------------
  def getPos(self):
    return self.pos
  
  #----------------------------
  def getAngle(self):
    return self.angle
    
  #------------------------------
  def turnTo(self, angle):
    self.angle = angle
    
  #-----------------------------
  def goTo(self, pos):
    """
    """
    if pos == None: raise ValueError("None passed as new position")
    self.pos = Point(pos.x, pos.y)
    
  #-------------------------------
  def advance(self, distance):
    """
    """
    self.pos += Point(cos(self.angle) * distance,  sin(self.angle) * distance)