# coding=utf-8

"""
@TODO <short description of the module>

@TODO <description of the module>

:copyright: 2016 by Angel Perea Martinez (angelpeream@yahoo.com), see AUTHORS
            for more details

:license: lGPLv3, see LICENSE for more details
"""
import os.path
import time

#--------------------------------
# printDir
#--------------------------------
def printDir(path):
    """
    """
    lines = []
    entries = os.listdir(path)
    for e in entries:
        p = path + "/" + e
        if os.path.isdir(p):
            lines.append("+- " + e)
            lines.extend(["   " + l for l in printDir(p).split("\n")])
        elif os.path.isfile(p):
            lines.append("+- " + e)
        else:
            lines.append(e + "(strange)")
    return "\n".join(lines)
#----------------------------
# some functions to isolate os layer
#----------------------------


def pathExists(path):
    return os.path.exists(path)

#---------------------------


def listDir(path):
    return os.listdir(path)

#-------------------------


def isDir(path):
    return os.path.isdir(path)

#---------------------------


def isFile(path):
    return os.path.isfile(path)

#----------------------------


def writeFile(path, data):
    g = open(path, "w")
    g.write(data)
    g.close()
    
#-----------------------------
def writeBinFile(path, data):
    g = open(path, "wb")
    g.write(data)
    g.close()

#-----------------------------

def readFile(filename, create=False, mode="r"):
    """
    reads a file, possibly creating it if not exists...
    """
    if create:
        # create dirs
        if not os.path.isdir(os.path.split(filename)[0]):
            os.makedirs(os.path.split(filename)[0])

        # create file
        if not os.path.isfile(filename):
            g = open(filename, "a")
            g.close()

    # 2) read contents

    f = open(filename, mode , encoding="utf8", errors="replace")
    r = f.read()
    f.close()
    return r


#---------------------------------
 

def readBinFile(path):
    f = open(path, "rb")
    a = f.read()
    f.close()
    return a


#------------------------------
def readCsv(filename, sep=":"):
    """
    """
    f = open(filename, "r")
    d = []
    for l in f.read().split("\n"):
        if l.strip() == "": continue
        v = l.strip().split(sep)
        d.append(v)
    f.close()
    return d


############################################
#            CPICKLE 
############################################

import pickle

#------------------------
#
#------------------------
def saveObj(obj, path):
    if (not isinstance(path, str)) and isinstance(obj, str):
        obj, path = path, obj  # guard because this was historically bad decided
    g = open(path, "wb")
    pickle.dump(obj, g, pickle.HIGHEST_PROTOCOL)
    g.close()


#-----------------------------  
#
#----------------------------
def loadObj(filename):
    f = open(filename, "rb")
    o = pickle.load(f)
    f.close()
    return o

pickleLoad = loadObj
pickleSave = saveObj



import shutil

# como debe ser una aplicacion sobre ficheros

#---------------------------
# findFiles
#---------------------------


def findFiles(root, filt):
    """
    return files in a directory that comply with some passed filter
    @param filt the filter function
    """
    selected = []
    for base, _dirs, files in os.walk(root):
        selected.extend(list(filter(filt, [base + "/" + f for f in files])))
    return selected

#----------------------------------------
#  deleteDir
#-----------------------------------------


def deleteDir(directory, confirm=True):
    """
    removes a directory allowing for interactive confirmation
    """
    if os.path.exists(directory):

        if confirm:
            x = input("delete directory '%s'?" % directory)
            if x.lower().strip() != "y":
                return
        shutil.rmtree(directory)

    else:
        pass

#------------------------------------
# isBelow
#------------------------------------


def isBelow(path, directory):
    return path.startswith(directory)


#--------------------------------------
# pathToRelative
#--------------------------------------
def pathToRelative(path, root):
    """
    """
    if not path.startswith(root):
        raise ValueError("path '%s' is not inside '%s' " % (path, root))

    return path[len(root) + 1:]
                                                                                    
#-------------------
# substInFile
#-------------------


def substInFile(file1, file2, template, newString):
    f = open(file1)
    t = f.read()
    f.close()

    f = open(file2, "w")
    f.write(t.replace( template, newString))
    f.close()


#------------------------------
# changeExt
#------------------------------
def changeExt(file_, newExt):
    return os.path.splitext(file_)[0] + '.' + newExt

#---------------------
# replicate
#---------------------


def replicate(rootDir, targetDir):
    """
    replicates a directory with source files, processing somehow each file
    """
    raise "not finished yet"
#     if not os.path.exists(targetDir):
#         os.mkdir(targetDir)
# 
#     for root, _dirs, files in os.walk(rootDir):
#         targetRoot = targetDir + root[len(rootDir):]
#         print targetRoot
#         os.mkdir(targetDir + "/" + root)
#         for f in files:
#             processFile(root + "/" + f, targetDir)

#----------------------
# filelog
#----------------------


def fileLog(fileName, colWidth, data):
    '''
    agnade una fila con datos a un fichero log
    '''
    f = open(fileName, "a")
    d = [time.strftime('%d/%m/%y')]
    d.extend(data)
    data = d

    s = "\n"

    for x in data:
        s = s + str(x).rjust(colWidth) + ", "
    f.write(s)
    f.close()


#----------------------------------
#
#----------------------------------
def dirSize(path):

    if os.path.isfile(path):
        s = os.path.getsize(path)
        # s = max(s, 4 * 1024) #kludge for getting real file size. Must investigate
        # s = quantizeUp(s, 4 * 1024)
        return s

    elif os.path.isdir(path):
        return sum([dirSize(path + "/" + e) for e in os.listdir(path)])
    else:
        raise ValueError("unknown path" + path)

#-------------------------------------


def xDir(path):
    """
    """
    dirs = []
    fileSize = 0

    for d in os.listdir(path):
        if os.path.isdir(path + "/" + d):
            dirs.append((d, dirSize(path + "/" + d)))

        elif os.path.isfile(path + "/" + d):
            fileSize += dirSize(path + "/" + d)

    dirs.append(("<REST OF FILES>", fileSize))

    return dirs


#=================================
# FileSize
#=================================
class FileSize:

    #--------------------------

    def __init__(self, bytes_):
        self.bytes = bytes_

    #---------------------------------
    def toString(self):
        if self.bytes < 1024:
            return str(self.bytes) + " b"
        elif self.bytes < 1000000:
            return "%2.2f Kb" % (self.bytes / 1024.0,)
        else:
            return "%2.2f Mb" % (self.bytes / (1024 * 1024.0),)

        return


#----------------------------
#  graph
#----------------------------
def graph(root):

    total = dirSize(root)

    dirInfo = xDir(root)
    #dirInfo.sort(lambda x, y: cmp(y[1], x[1]))
    largest = float(max([d[1] for d in dirInfo]))

    for d in dirInfo:
        name = d[0]
        s = FileSize(d[1]).toString()
        info = (name + " (" + s + ")")[-25:].rjust(25)
        print("%s %4.2f%% *%s" % (info, (d[1] * 100.0 / total), "*" * int(d[1] * 40.0 / largest), ))

############ FILE SYSTEM ##############
 


#----------------------
# treeSize
#----------------------
def treeSize(directory):
    """
    returns the number of subdirectories of a tree
    """
    totalDirs = 0
    for _baseDir, dirs, _files in os.walk(directory):
        for d in dirs:
            if d.startswith("."): dirs.remove(d)
        totalDirs += len(dirs)
        
    return totalDirs
 
# graph("f:/notes/otros documentos")
