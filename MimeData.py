import os 


class MimeData:
    """
    One of the allowed return data types for  
    """
  
    def __init__(self, filename, data):
        """
        Params
          mimeExt the mime-valid extension for the file
          data data for the file
        """
        if not "." in filename: raise ValueError("not a valid extension. needs dot")
        self.filename = filename
        self.data = data
        
        self.mimeTable = {
            ".jpg"  : "image/jpeg",
            ".jpeg" : "image/jpeg",
            ".bmp"  : "image/bmp",
            ".ico"  : "image/ico",
            ".gif"  : "image/gif",
            ".png"  : "image/png",
            ".pdf"  : "application/pdf",
            ".doc"  : "application/msword",
            ".zip"  : "application/zip",
            ".ps"   : "application/postscript",
            ".eps"  : "application/postscript", 
            ".gtar" : "application/x-gtar",  
            ".tar"  : "application/x-tar",
            ".gz"   : "application/x-gzip",
            ".latex": "application/x-latex", 
            ".html" : "text/html",
            ".htm"  : "text/html", 
            ".txt"  : "text/plain",
            ".asc"  : "text/plain",
            ".rtf"  : "text/rtf",
            ".xml"  : "text/xml",
            ".exe"  : "application/octet-stream",
            ".xxx"  : "binary/unknown",
            ".css"  : "text/css",
            ".js"   : "text/javascript"
          }
      
    

    def getLabel(self):
        """
        """
        ext = "." + self.filename.split(".")[1]
        return self.mimeTable.get(ext, self.mimeTable[".xxx"])
    

    def __str__(self):
        """
        """
        return f"{self.filename} ({self.getLabel()}):{str(self.data[:15])}..."




#---------------------------------
# getMime
#---------------------------------
def getMime(extension, default = "binary/unknown"):
    """
    returns the mime type for an extension
    """
    mimeTable = {
      ".jpg": "image/jpeg",
      ".jpeg": "image/jpeg",
      ".bmp": "image/bmp",
      ".ico": "image/ico",
      ".gif": "image/gif",
      ".png": "image/png",
      ".pdf": "application/pdf",
      ".doc": "application/msword",
      ".zip": "application/zip",
      ".ps": "application/postscript",
      ".eps": "application/postscript",
      ".gtar": "application/x-gtar",
      ".tar": "application/x-tar",
      ".gz": "application/x-gzip",
      ".latex": "application/x-latex",
      ".html": "text/html",
      ".htm": "text/html",
      ".txt": "text/plain",
      ".asc": "text/plain",
      ".rtf": "text/rtf",
      ".xml": "text/xml",
      ".exe": "application/octet-stream",
      ".js" : "application/javascript",
      ".tgz": "application/gzip",
      ".css"  : "text/css"
    }
    
    # if a full file passed:
    if not extension.startswith("."):
        extension = os.path.splitext(extension)[1]
        
    return mimeTable.get(extension.strip().lower(), default)
    

