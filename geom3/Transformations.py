import math
from softap3.geom3d.Vector import *

#======================
# Transformation
#======================
class Transformation:
    """
    A General Transformation
    """

    def __init__(self, a11, a12, a13, a14,
                       a21, a22, a23, a24,
                       a31, a32, a33, a34,
                       a41, a42, a43, a44):

        self.a11, self.a12, self.a13, self.a14 = a11, a12, a13, a14
        self.a21, self.a22, self.a23, self.a24 = a21, a22, a23, a24
        self.a31, self.a32, self.a33, self.a34 = a31, a32, a33, a34
        self.a41, self.a42, self.a43, self.a44 = a41, a42, a43, a44
          


    #----------------------------------------------
    def apply(self, other):
        return self.__mul__(other)

      
    #---------------------------------------------
    def __mul__(self, other):
      """
      """
        
      if isinstance(other, Point3D):
        p = other
        x, y, z = p.x, p.y, p.z

        nx = self.a11 * x + self.a12 * y + self.a13 * z + self.a14 * 1.0
        ny = self.a21 * x + self.a22 * y + self.a23 * z + self.a24 * 1.0
        nz = self.a31 * x + self.a32 * y + self.a33 * z + self.a34 * 1.0
        nc = self.a41 * x + self.a42 * y + self.a43 * z + self.a44 * 1.0

        if nc != 1.0:
          nx, ny, nz = nx / nc, ny / nc, nz / nc

        return Point3(nx, ny, nz)
        

      elif isinstance(other, Transformation):      
        a11 = self.a11 * other.a11 + self.a12 * other.a21 + self.a13 * other.a31 + self.a14 * other.a41
        a12 = self.a11 * other.a12 + self.a12 * other.a22 + self.a13 * other.a32 + self.a14 * other.a42
        a13 = self.a11 * other.a13 + self.a12 * other.a23 + self.a13 * other.a33 + self.a14 * other.a43
        a14 = self.a11 * other.a14 + self.a12 * other.a24 + self.a13 * other.a34 + self.a14 * other.a44
        
        
        a21 = self.a21 * other.a11 + self.a22 * other.a21 + self.a23 * other.a31 + self.a24 * other.a41
        a22 = self.a21 * other.a12 + self.a22 * other.a22 + self.a23 * other.a32 + self.a24 * other.a42
        a23 = self.a21 * other.a13 + self.a22 * other.a23 + self.a23 * other.a33 + self.a24 * other.a43
        a24 = self.a21 * other.a14 + self.a22 * other.a24 + self.a23 * other.a34 + self.a24 * other.a44

        a31 = self.a31 * other.a11 + self.a32 * other.a21 + self.a33 * other.a31 + self.a34 * other.a41
        a32 = self.a31 * other.a12 + self.a32 * other.a22 + self.a33 * other.a32 + self.a34 * other.a42
        a33 = self.a31 * other.a13 + self.a32 * other.a23 + self.a33 * other.a33 + self.a34 * other.a43
        a34 = self.a31 * other.a14 + self.a32 * other.a24 + self.a33 * other.a34 + self.a34 * other.a44

        a41 = self.a41 * other.a11 + self.a42 * other.a21 + self.a43 * other.a31 + self.a44 * other.a41
        a42 = self.a41 * other.a12 + self.a42 * other.a22 + self.a43 * other.a32 + self.a44 * other.a42
        a43 = self.a41 * other.a13 + self.a42 * other.a23 + self.a43 * other.a33 + self.a44 * other.a43
        a44 = self.a41 * other.a14 + self.a42 * other.a24 + self.a43 * other.a34 + self.a44 * other.a44

        return Transformation(a11, a12, a13, a14, a21, a22, a23, a24, a31, a32, a33, a34, a41, a42, a43, a44)
      
      elif isinstance(other, list):
        return [self * l for l in other]
                        
      else:
        return other.transformBy(self)
        

    #------------------------------------
    def inverse(self):
        """
        """
        
    
    #------------------------------------
    def transpose(self):
        """
        """
        
    #-----------------------------------
    def __str__(self):
        s = []
        
        s.append("| %5f %5f %5f %5f |" % (self.a11, self.a12, self.a13, self.a14) )
        s.append("| %5f %5f %5f %5f |" % (self.a21, self.a22, self.a23, self.a24) )
        s.append("| %5f %5f %5f %5f |" % (self.a31, self.a32, self.a33, self.a34) )
        s.append("| %5f %5f %5f %5f |" % (self.a41, self.a42, self.a43, self.a44) )
        return "\n".join(s)



#======================
#
#=======================
class Rotation3D(Transformation):
    """
    an special type of transformation that involves only rotation. Inverse is transpose
    """

    def __init__(self, a11, a12, a13,
                       a21, a22, a23, 
                       a31, a32, a33):


        self.a11, self.a12, self.a13, self.a14 = a11, a12, a13, 0.0
        self.a21, self.a22, self.a23, self.a24 = a21, a22, a23, 0.0
        self.a31, self.a32, self.a33, self.a34 = a31, a32, a33, 0.0
        self.a41, self.a42, self.a43, self.a44 = 0.0, 0.0, 0.0, 1.0
    

    #----------------------
    def inverse(self):
       """
       return the inverse
       """
       return Rotation3D(self.a11, self.a21, self.a31,
                         self.a12, self.a22, self.a32,
                         self.a13, self.a23, self.a33)


#===========================
#
#============================
class OrtogonalTransformation(Rotation3D):
  """
  The most general ortogonal transformation, that transforms from a set of axes to another
  """
  def __init__(self, u, v, w):
    """
    """
    u, v, w = u.normalize(), v.normalize(), w.normalize()
    Rotation3D.__init__(self, u.x, u.y, u.z,
                              v.x, v.y, v.z,
                              w.x, w.y, w.z,
                        )

    
#=============================
# Tr
#=============================
class Translation(Transformation):
  """
  """

  def __init__(self, dx, dy, dz):
        self.a11, self.a12, self.a13, self.a14 = 1.0, 0.0, 0.0, dx
        self.a21, self.a22, self.a23, self.a24 = 0.0, 1.0, 0.0, dy
        self.a31, self.a32, self.a33, self.a34 = 0.0, 0.0, 1.0, dz
        self.a41, self.a42, self.a43, self.a44 = 0.0, 0.0, 0.0, 1.0
        
  #-------------------------------
  def inverse(self):
        return Translation(-self.a14, -self.a24, -self.a34)


class ShiftTrans(Translation):
  pass

#=====================
#
#=====================
class RotZ(Rotation3D):
  
    def __init__(self, theta):
        s = math.sin(theta)
        c = math.cos(theta)
        Transformation.__init__(self,   c,  -s  , 0.0, 0.0,
                                        s,   c  , 0.0, 0.0,
                                      0.0,   0.0, 1.0, 0.0,
                                      0.0,   0.0, 0.0, 1.0)



#=====================
# RotY
#=====================
class RotY(Transformation):
  """
  a generic rotation arround Y axis
  """
  def __init__(self, theta):
    s = math.sin(theta)
    c = math.cos(theta)
    Transformation.__init__(self,   c,  0.0,  -s, 0.0,
                                  0.0,  1.0, 0.0, 0.0,
                                    s,  0.0,   c, 0.0,
                                  0.0,  0.0, 0.0, 1.0)

#=====================
# RotX
#=====================
class RotX(Transformation):
  """
  a generic rotation arround X axis
  """
  def __init__(self, theta):
    s = math.sin(theta); c = math.cos(theta)
    Transformation.__init__(self, 1.0, 0.0, 0.0, 0.0,
                                  0.0,   c,  -s, 0.0,
                                  0.0,   s,   c, 0.0,
                                  0.0, 0.0, 0.0, 1.0)    


#======================
# RotAxis
#=======================
class DEPRECATE_RotAxis(Transformation):
    """
    rotation theta degrees arround an arbitrary axis given by ux, uy, uz
    """
    def __init__(self, axis, theta):
        """
        """
        axis = axis.normalize()
        ux, uy, uz = axis.x, axis.y, axis.z
        ux2, uy2, uz2 = ux * ux, uy * uy, uz * uz
        cos = cos(theta)
        sin = sin(theta)
        
        Transformation.__init__(self, ux2 + cos * (1 - ux2)        , ux * uy * (1-cos) - uz * sin  , uz * ux * (1-cos) + uy * sin, 0.0,
                                      ux * uy * (1-cos) + uz * sin , uy2 + cos * (1 - uy2)         , uy * uz * (1-cos) - ux * sin, 0.0,
                                      uz * ux * (1-cos) - uy * sin , uy * uz * (1 -cos) + ux * sin , uz2 + cos * (1-uz2)         , 0.0,
                                      0.0                          , 0.0                           , 0.0                         , 1.0)

                                      

#=====================
# Rotaxis
#=====================
class DEPRECATE_OldRotAxis(Rotation3D):
  """
  A generic transformation arround an axis
  """
  
  def __init__(self, point1, point2, theta, isRel = False):
    """
    @param isRel if true, point2 is a vector based on point 1 
    """
    self.p1 = point1
    self.p2 = point2 - point1 if isRel else point2
    self.theta = theta
    
    translation = Traslation(-point1)
    
    
    Transformation.__init__(self, m.a11, m.a12, m.a13, m.a14,
                                  m.a21, m.a22, m.a23, m.a24,
                                  m.a31, m.a32, m.a33, m.a34,
                                  m.a41, m.a42, m.a43, m.a44)
                                                                        
#=====================
# Scale
#=====================
class Scale(Transformation):
  """  
  an scalation arround
  """
  def __init__(self, sx, sy, sz):
    sx, sy, sz = float(sx), float(sy), float(sz)
    Transformation.__init__(self,  sx, 0.0, 0.0, 0.0,
                                  0.0,  sy, 0.0, 0.0,
                                  0.0, 0.0,  sz, 0.0,
                                  0.0, 0.0, 0.0, 1.0 )
        
  
 
#-------------------------
DUMMY_TRANS = Transformation( 1.0, 0.0, 0.0, 0.0,
                              0.0, 1.0, 0.0, 0.0,
                              0.0, 0.0, 1.0, 0.0,
                              0.0, 0.0, 0.0, 1.0)

                              
unitTransformation = DUMMY_TRANS                              
trivTrans = unitTransformation

#-------------------------------------------------
class TrivTrans(Transformation):
  
  def __init__(self):
     Transformation.__init__(self, 
                              1.0, 0.0, 0.0, 0.0,
                              0.0, 1.0, 0.0, 0.0,
                              0.0, 0.0, 1.0, 0.0,
                              0.0, 0.0, 0.0, 1.0)



#-----------------------------------------------
def getFlatteningTrans(points):
    """
    """
    for p in points:
      if not isinstance(p, Point3):
        raise ValueError("wrong argument:" + str(p.__class__))
      
    if len(points) < 3:
      raise ValueError("need at least three points")

    p1, p2, p3 = points[0], points[1], points[2]
    v1 = (p2 - p1).normalize()
    v2 = (p3 - p1).normalize()
    v3 = (v1 ** v2).normalize()
    v2 = v3 ** v1 # this makes all ortogonal
    t1 = Translation(-p1.x, -p1.y, -p1.z)
    t2 = OrtogonalTransformation(v1,v2,v3)
    trans = t2 * t1
    error = 0     #TODO
    return error, trans


