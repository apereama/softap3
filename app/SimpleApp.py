import sys, os.path

from softap3.stringTools import colorize

from os.path import expanduser
import logging
import webbrowser
from .AppLogFormatter import AppLogFormatter
from softap3 import devTools 

RAISE_ALWAYS = True


#===============================
#    AppException
#===============================
class AppException(Exception):
    """
    the main class for exceptions raised by the app
    """
    pass 

#---------- those are values for sysexit.h -----------------

EXITCODE_OK = 0                        # successful termination
EXITCODE_BASE = 64                 # base value for error messages
EXITCODE_USAGE = 64                # command line usage error
EXITCODE_DATAERR = 65            # data format error
EXITCODE_NOINPUT = 66            # cannot open input
EXITCODE_NOUSER = 67             # addressee unknown
EXITCODE_NOHOST = 68             # host name unknown
EXITCODE_UNAVAILABLE = 69    # service unavailable
EXITCODE_SOFTWARE = 70         # internal software error
EXITCODE_OSERR = 71                # system error (e.g., can't fork)
EXITCODE_OSFILE = 72             # critical OS file missing
EXITCODE_CANTCREAT = 73        # can't create (user) output file
EXITCODE_IOERR = 74                # input/output error
EXITCODE_TEMPFAIL = 75         # temp failure; user is invited to retry
EXITCODE_PROTOCOL = 76         # remote error in protocol
EXITCODE_NOPERM = 77             # permission denied
EXITCODE_CONFIG = 78             # configuration error
EXITCODE_BREAK = 79                # this one added for edn

    
def fatalError(message, exitCode):
    print(message)
    sys.exit(exitCode)
    
#==========================
#    SimpleApplication
#==========================
class SimpleApp:
    """
    Features:
        * File configuration for the application, allowing different places and 
            a hierarchy of them        
        * Provides a file for storing other data (not configurable)
        * Parameters come as a merge of the line parameters and file configuration
        * Banner, help, etc
        * Error messages, etc
        * Logging capabilities

    Child classes:
        * CommandApp: For applications that encapsulate other
             tinny applications. Each command is a SimpleApplication that
                _independently_ parses the configuration from the command line
                    
    """
    
    #---------------------------
    def __init__(self, name, isDaemon = False, allowDupParams = False):
        """
        @param isDaemon    if set to true, runs the application in background
        """
        
        self.args = []
        self.params = {}
        self.appName = name                 # used to save app info, and preferences
        self.helpStrings = {}             # to be implemented by child classes
        self.debugMode = False            # setting it to true, prints tracebacks
        
        try:                                        # context allow storing data between invocations
            self.context = self._loadContext()
        except:
            self.warning("context file not found.resetting")
            self.context = self._getNewContext()
            self._saveContext()
            
        self.isDaemon = isDaemon
        self.allowDupParams = allowDupParams
        self.logger = self.getLogger()
        self.debug = False  # setting it to true raises always
     
        
    
    #----------------------------------
    # override this functions to make app 
    #
    # mandatory overriding
    #-----------------------------------

    def init(self):
        """
        initialize the application
        """
        pass


    #---------------------------------
    def run(self):
        """
        this is the main function called
        """        
        print("run() function unimplemented")

    #----------------------------------
    def setLogging(self, value=True):
        print("SET LOGGING!!!")
        if value: devTools.setupLogging()

                 
    #----------------------------------
    def finalize(self):
        """
        things to do before finishing, optional
        for each class
        """
        pass

    #-----------------------------------
    def checkBefore(self):
        """
        a generic function to check parameters before doing anything
        """
        #if incompatible(args, "asdf", "asdf"): self.fatalError("asdfasfsfsd")
        pass
        
    
    #-----------------------------------    
    def getBanner(self):
        """
        function called to print the banner of the application
        """
        return "generic banner"


    #-----------------------------------
    def getHelp(self, topic):
        """
        function called to assist in the help of the application
        """
        return "help"

        
    #--------------------------------------
    def getUsage(self):
        """
        function called to get the usage
        """
        return "(no usage info)"
    
    #--------------------------------
    def parseConfFile(self, filename):
        """
        parses a configuration file, setting the parameters, but do not raise anything
        because configuration file could have much more parameters than command line
        """
        raise NotImplemented("not implemented")
         
    
    #-------------------------------------
    # public functions
    #
    # Already implemented, overridable
    #-------------------------------------
    
    #------------------------------------
    def setParam(self, name, value):
        """
        """
        if hasattr(self, "paramset_" + name):            
            getattr(self, "paramset_" + name)(value)
                            
        else:
            self.setDefaultParam(name, value)
     
    #---------------------------------
    def setDefaultParam(self, name, value):
            """
            """            
            self.params[name] = value
        
    #-----------------------------------
    def setArg(self, value):
        """
        """ 
        self.args.append(value)
         
    #-----------------------------------
    def main(self, lineArgs, configFiles = None, configFileOption = None):
        """
        main function
        @param lineArgs a list with argument and parameters
        @param configFiles path to configuration files
        @param configFileOption ???
        """
        
        
        # 1) get configuration from filename, if exists        
        
        #config = self._parseParams(lineArgs, configFiles, configFileOption = None)
        # THIS IS NOT WORKING
                                   
        # 2) parse lineParameters. If not found, use configuration
        args, params = self.parseCommandLine(lineArgs)
        
        for a in args:
            self.setArg(a)
        
        for name, value in list(params.items()):    
                self.setParam(name, value)
                
        # 3) execute the main function of the application
        self.checkBefore()
        self.init()
        try:
            exitCode = self.run()
            
        except AppException as e:
            if self.debugMode: raise
            self.error(str(e))
            exitCode = -1
            
        except Exception as e:
            if self.debug: raise
            print(e)
            if self.debugMode: raise
            self.error("uncatched exception (%s)" % (str(e)))
            import traceback
            traceback.print_exc
            #print((getTraceback()))
            exitCode = -128
            
        except KeyboardInterrupt as e:

            self.error("user break. Exiting...")
            exitCode = -200
            
        except SystemExit as e: # reraise , because sys.exit(...) raises this exception
            raise 
            
        except BaseException as    e: # other exception result in an uncatched exception
            if RAISE_ALWAYS or self.debug: raise
            print(e)
            if self.debugMode: raise
            self.error("uncatched error in application")
            exitCode = -128
            
                
        self._finalize() # this. (will call to the app finalize() method)
        sys.exit(exitCode)        
    
    
    #----------------------------------
    #    comodity functions 
    #
    #    those functions are provided for quick and understandable parsing
    #    and validation of parameters    
    #----------------------------------
    def incompatible(self, args, name1, name2):
        """
        return true if both names are defined
        """
        pass
    
    
    #---------------------------------------
    def checkRange(self, value, low = None, high = None,
                                 fatalError=None, exception=None):
        """
        returns true if the value lies in the given range
        @fatalError. If passed, raises a fatal error if value is out of range
        @exception. if passed, raises the passed exception if out of range
        raises an exception
        """
        return value > low and value < high
        pass
        

    #----------------------------------------------
    def inSet(self, args, name, possibleValues, ignoreCase = True):
        """
        returns true if the argument stays in the set
        @ignoreCase ignore the case of the parameter
        """
        pass    


    #----------------------------------
    def openBrowser(self, url):
        """
        """
        webbrowser.open(url)        

        
    #-----------------------------------
    def displayHtml(self, html):
        """
        displays a given html
        """
        f = self.getTempFile()
        f.write(html)
        webbrowser.open(f)



    #---------------------------------------------
    # context
    #---------------------------------------------
    
    def _saveContext(self):
        """
        saves the context
        """
        import pickle
        g = open(self.getUserDir() + "/.%scontext" % self.appName, "wb")
        pickle.dump(self.context, g)
        g.close()

    #---------------------------------------------
    def _loadContext(self):
        """
        loads the context
        """
        import pickle
        f = open(self.getUserDir() + "/.%scontext" % self.appName, "rb")
        context = pickle.load(f);
        f.close()
        return context


    #---------------------------------------------
    def _getNewContext(self):
        """
        resets the context (the info that stays from one execution to another)
        """
        return object()


    #--------------------------------
    def exit(self, exitCode):
        """
        """
        self.finalize() 
        sys.exit(exitCode)
        
        
    #--------------------------------------
    #    Error handling and messages
    #---------------------------------------
    def fatalError(self, title, description = "", exitCode = -1):
        """
        issues an error to the user and exits the program
        """
        print((colorize("ERROR:", "red") + title))
        print(description        )
        print((self.getUsage()))
        try:
            self.finalize()
        finally:
            sys.exit(exitCode)
    
    fatal = fatalError
        
        
    #----------------------------------------
    def error(self, title, description = ""):
        """
        issues an error to the user and exits the program
        """
        if self.debugMode: raise
    
        print(("%s : %s " % (colorize("ERROR:", "lightred") , title )))
        print( description )

        
    #---------------------------------------
    def warning(self, title, description = ""):
        """
        issues a warning to the user
        """
        print(( colorize("WARNING:", "yellow") + title ))
        print( description )

        
    #-------------------------------------
    def info(self, string):
        """
        """
        print( string )

        
    #-----------------------------------
    def display(self, *args):
        """
        """
        print(( " ".join(args) ))
        
    #-------------------------------------
    def debug(self, string, module = None):
        """
        """
        if module == None: module = "debug"
        print(( "%s: %s" % (colorize(module, "grey"), string) ))
    
    
    #----------------------------
    # misc.
    #-----------------------------
    def getUserDir(self):
        """
        """        
        return expanduser("~")
        
    #------------------------------
    def getLogger(self, name= "app"):
        """
        """
        return logging.getLogger(name)

    #------------------------------
    def setupLogging(self):
        """
        """
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(AppLogFormatter())
        logging.getLogger().addHandler(ch)
        logging.getLogger().setLevel(logging.INFO)

    #-------------------------------
    def setDebug(self, mode=True):
        """
        """
        self.debugMode = mode     
        
        
    ##---------------------------------------
    #def setParameters2(self, args, params):
        #"""
        #intermediate function to set parameters. Not to be used by child clases
        #"""

        #### 1) prepare the parameters
        #for name, value in params.items():
            #if has_name(self, "setParam_" + name):
                #get_name(self, "setParam_" + name)(value)
            #else:
                #self.fatalError("wrong parameter '%s'" % (name,))

                
    #-----------------------------------
    #                 Parameters
    #-----------------------------------
    def parseParamAsInteger(self, params, name, limits = (None, None), 
                                                    default = None, errorMsg = None):
        """
        """
        value = params.pop(name, default)    # in case it doesnt exists
        try:
            value = int(value)
        except:
            fatalError(errorMsg)
    
        if (limits[0] != None) and (limits[0] > value):
            fatalError("value too low")

        if (limits[1] != None) and (limits[1] < value):
            fatalError("value too high")
            
        value = value.split(",") if    value != None else None
        if value == None:
            self.fatalError(errorMsg)
        else:
            return value

    #---------------------------
    def readInt(self, prompt = ""):
        """
        reads a number as an int
        """
        while 1:
            i = eval(input(prompt))
            try:
                r = int(i)
                return r
            except:
                continue

    #----------------------------------
    def checkInt(self, value, exception , limits = (None, None), default = None):
        """
        checks that a number is an integer
        """
        try:
            value = int(value)
        except:
            if default != None:
                value = default
            else:
                raise exception
        
        if (limits[0] != None and value < limits[0]) or \
             (limits[1] != None and value > limits[1]):
            raise exception
        
        return value
    
    #-------------------------------
    
    def parseParamAsBoolean(self, params, name, default = None, errorMsg = None):
        """
        """
        value = params.pop(name, default).strip().lower()    # in case it doesnt exists
        if value == "y" or value=="yes":
            value = True
        elif value==None:
            self.fatalError("wrong bool value")
        else:
            value = False

        return value
    
    #----------------------------
    def parseParamAsList(self, params, name, default = None, errorMsg = None):
        """
        """
        value = params.pop(name, default)
        value = value.split(",") if    value != None else None
        if value == None:
            self.fatalError(errorMsg)
        else:
            return value

            
    #---------------------------------------
    def parseParamAsNewDir(self, params, name, default = None, 
                                                 errorMsg = "parent directory doesnt exist"):
        """
        """
        value = params.pop(name, default)
        if value == None:
            self.fatalError("parameter %s not found" % name)
        parentDir = "/".join(value.split("/")[:-1])
        if not os.path.exists(parentDir):
            self.fatalError(errorMsg)
        return value

    #---------------------------------------
    def parseParamAsNewFile(self, params, name, default = None, errorMsg = None):
        """
        """
        return self.parseParamAsNewDir(params, name, default, errorMsg)

        
    #---------------------------------------
    def parseParamAsExistingDir(self, params, name, default= None, 
                                                            errorMsg = "directory doesnt exist"):
        """
        parses a parameter as an existing directory
        """
        value = params.pop(name, default)
        if not os.path.isdir(value):
            self.fatalError(errorMsg)
        return value

        
    #---------------------------------------
    def parseParamAsExistingFile(self, params, name, default= None, 
                                                             errorMsg = "file doesnt exist"):
        """
        parses a parameter as an existing file
        """
        value = params.pop(name, default)
        if not os.path.isfile(value):
            fatalError(errorMsg, 3)
        return value

    
    #----------------------------------
    def checkParam(self, param, possibilities):
        """
        checks a parameter
        """
        param = param.lower().strip()
        if not param in possibilities.key:
                ValueError("parameter must one of %s" % possibilities)
        return possibilities[param]

    #--------------------------------
    # private functions
    #---------------------------------

    def parseCommandLine(self, lineArgs, allowRepeats = False):
        """
        parses command line arguments transforming into arguments and parameters         
        """        
        
        # 1) args
        if isinstance(lineArgs, str): lineArgs = lineArgs.split(" ")
        lineArgs = [l for l in lineArgs if str(l) != ""]
        

        args = [x for x in lineArgs if not x.startswith("-")]
        
        # 2) params
        
        pList = [x for x in lineArgs if x.startswith("--") or x.startswith("-")]
        params = {}
        
        for p in pList:
            if p.startswith("--"): p = p[2:]
            if p.startswith("-"): p = p[1:]
                
            if "=" in p:
                a, b = p.split("=", 1)
                name, value = a,b
            else:
                name, value = p, True
                
                
            if name in params:
                if allowRepeats:
                    if not isinstance(params[name], list):
                        params[name] = [params[name]]
                    params[name].append(value) 
                
                else:
                    raise AppException("repeated parameter %s" % name)
            else:
                params[name] = value
                                             
        
        return args, params
    
    
    #-------------------------------------    
    def _getFileConfiguration(self, configFiles):
        """
        tries to find one of the files
        """
        if configFiles != None: # there are default config files
            self.configuration = None
            for filename in configFiles:
                if os.path.exists(filename):
                    return filename

                    
    #-------------------------------------------
    def _parseParams(self, lineArgs, configFiles, configFileOption = None):
        """
        configures the application
        """
        #config = self.getConfiguration(lineArgs, configFiles, configFileOption)

        if configFiles == None: configFiles = []

        # 1) if a filename is specified or there is an option for 
        #        providing it, get the filename
        
        args, params = self.parseCommandLine(lineArgs,
                                             allowRepeats = self.allowDupParams)
        for a in args:
            self.setArg(a)
        
        for name, value in list(params.items()):    
                self.setParam(name, value)
     
        if configFileOption != None and configFileOption in params:
            filename = params[configFileOption]
            if not os.path.isfile(filename):
                self.fatalError("configuration file doesn't exist")
            else:
                configFiles = [filename] # overrides the possibly existing ones
                
        # 2) now, get configuration from configFiles
        
        if configFiles != None and configFiles != []:
            filename = self._getFirst(configFiles)
            if filename == None:
                self.fatalError("configuration", 
                                                "no default configuration file found from the list ", 
                                                exitCode = -2)

            try:
                #config = self.parseConfFile(filename)
                pass
            except:
                self.fatalError("format error during configuration", "" , exitCode = -3)


    #---------------------------------
    def _finalize(self):
        """
        this is the true called function for
        shutdown that will call the
        ancestor class' finalize()
        """
        self._saveContext()
        self.finalize()
