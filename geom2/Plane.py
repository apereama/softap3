import math

#-----------------------
class Plane:
  
  #--------------------------
  def __init__(self, origin, p2, p3):
    """
    """
    self.o = origin
    self.u, self.v = p2-self.o, p3-self.o
    
  #-------------------------
  def point(self, x, y):
    return self.o + self.u * x + self.v * y

  #-----------------------
  def polarPoint(self, angle, radius):
    x, y = radius * math.cos(angle), radius * math.sin(angle)
    return self.point(x,y)