import os
import tempfile

from rongo.Common import guard, bashCall, readFile, writeFile
from rongo.core.MimeData import MimeData
from rongo.extlibs.softap.oldReport.Document import Document

#====================
#   HtmlDocument
#====================
class HtmlDocument(Document):
    """
    a single file Html document
    """    
    
    #--------------------
    def __init__(self):
        """
        @param  
        """
        self.html = []
        self.language = "html"
        self.body = Document()
        self.head = Document()
        self.footer = Document()
        self.resources = {}
        
    #--------------------
    def write(self, text):
        """
        writes text
        """
        self.body.write(self._protect(text))
 
 
    #--------------------
    def indent(self):
        """
        """
        self.body.indent()
    
    
    #--------------------
    def dedent(self):
        """
        """
        self.body.dedent()
    
    
    #--------------------
    def addSection(self, title, level):
        """
        adds a new section
        """
        tags = {1: ("<h1>", "</h1>"),
                2: ("<h2>", "</h2>"),
                3: ("<h3>", "</h3>"),
                4: ("<h4>", "</h4>")
                }
        
        tag = tags[guard(level, 1, 4)]
        self.writeLine(tag[0] + title + tag[1])
        
     
    #---------------------
    def getFile(self, name = "document", tempDir = None):
        """
        returns a full html document in a single file
        """
        if tempDir == None:
            tempDir = tempfile.gettempdir()
             
        with tempDir as directory:                        
            current = os.getcwd(); 
            os.chdir(directory)
        
        # 2) save the images and files to the working dir
                
        self.saveResources(directory)
        
        # 1) execute latex command
        
        tarFilename = name + ".tgz"
        htmlFilename = "main.html"        
        writeFile(htmlFilename, self._getHtml())
        cmd = "tar -czf '%s' '%s'" % (tarFilename, tempDir)        
        code, std, err = bashCall(cmd)
        
        if code != 0:
            print(std)
            print("--------")
            print(err)
            raise ValueError("there was an error producing the ouput")
        
        # 2) return the generated file        
        
        data = readFile(tarFilename)
        data = MimeData(tarFilename, data)
        os.chdir(current) # return to the previous directory
        return data
    
