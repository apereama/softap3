from softap3.geom3d.Transformations import *
from softap3.geom3d.Vector import *
#----------------------------------
#
#----------------------------------
def geantTrans(a,b):
  """
  two vectors are passed. They could not be normal, 
  but they define a plane. That plane will be z=0
  """
  a,b,c = getTriedron(a,b)
  t = Transformation(
      a.x, b.x, c.x, 0.0,
      a.y, b.y, c.y, 0.0,
      a.z, b.z, c.z, 0.0,
      0.0 , 0.0, 0.0, 1.0)
      
  t = t.invert()
  return t
      

      
#--------------------------   
def test1():
  a = Vector(1,4,2); b = Vector(2,3,4)
  a,b,c = getTriedron(a,b)  
  print a*b, b*c, a*c

  
#----------------------
def test2():
  a,b,c = Vector(1,1,1), Vector(2,2,2), Vector(3,2,1)
  t = flatten(a,b,c)
  print t
  
test2()