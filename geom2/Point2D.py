"""
@TODO
"""

import math


def polar(alpha, length):
    """
    @TODO
    """
    return Point2D(length * math.cos(alpha), length * math.sin(alpha))


class Point2D:
    """
    This is a mathematical point plus the given commands to make it primitive
    """

    def __init__(self, x, y=None, name=""):
        """
        """
        if y is None:
            if isinstance(x, Point2D):
                x, y = x.x, x.y
            elif isinstance(x, tuple):
                x, y = x[0], x[1]
            else:
                raise ValueError("x is wrong type" + str(x))

        if not isinstance(name, str):
            raise ValueError("Wrong value for name" + str(name))

        self.name = name

        if y is None and isinstance(x, tuple):
            x = x[0], y = x[0]

        try:
            self.x = float(x)
            self.y = float(y)
        except ValueError:
            raise ValueError("x:%s y:%s don't seem to be floats" % (x, y))

    def midPoint(self, other, factor=0.5):
        """
        returns a point along the line joining this and another point
        """

        return Point2D(other.x * factor + self.x * (1.0 - factor),
                       other.y * factor + self.y * (1.0 - factor))

    def __getitem__(self, i):
        if i == 0:
            return self.x
        elif i == 1:
            return self.y
        else:
            raise ValueError("wrong index")

    def distance(self, other):
        """
        returns the distance to other element
        """
        if isinstance(other, Point2D):
            a = other.x - self.x
            b = other.y - self.y
            return math.sqrt(a * a + b * b)

    def dist2(self, other):
        """@TODO """
        if isinstance(other, Point2D):
            a = other.x - self.x
            b = other.y - self.y
            return a * a + b * b

    def toTuple(self):
        """@TODO"""
        return self.x, self.y

    def scale(self, x, y):
        """
        @TODO
        """
        return Point2D(self.x * x, self.y * y, self.name)

    def modulus(self):
        """
        @TODO
        """
        return self.distance(Point2D(0, 0, self.name))

    def mod2(self):
        """@TODO"""
        return self.dist2(Point2(0, 0))

    def length(self):
        """@TODO"""
        return self.modulus()

    def normalize(self, value=1.0):
        """
        @TODO
        """
        n = float(value) / self.modulus()
        return Point2D(self.x * n, self.y * n, self.name)

    def shift(self, dx, dy):
        """
        @TODO
        """
        return Point2D(self.x + dx, self.y + dy, self.name)

    def moveTo(self, x, y):
        """
        @TODO
        """
        self.x = x
        self.y = y

    def toPolar(self):
        """@TODO"""
        return Point2D(self.length(), math.atan2(self.y, self.x))

    def shiftPolar(self, angle, distance):
        """
        @TODO
        """
        return Point2D(self.x + math.cos(angle) * distance, self.y + math.sin(angle) * distance, self.name)

    def clone(self):
        """
        returns a clone of this point
        """
        return Point2D(self.x, self.y, self.name)

    def _adapt(self, other):
        """@TODO"""
        if isinstance(other, tuple):
            other = Point2D(float(other[0]), float(other[1]))

        return other

    def __add__(self, other):
        """
        @TODO
        """
        other = self._adapt(other)
        if not isinstance(other, Point2D): raise ValueError("wrong class for other: " + str(other.__class__))
        return Point2D(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        """
        @TODO
        """
        other = self._adapt(other)
        return Point2D(self.x - other.x, self.y - other.y)

    def __mul__(self, factor):
        """
        @TODO
        """
        if isinstance(factor, Point2D):
            return self.x * factor.x + self.y * factor.y

        factor = float(factor)
        return Point2D(self.x * factor, self.y * factor, self.name)

    def __truediv__(self, factor):
        """@TODO"""
        return self.__mul__(1.0 / factor)

    def __eq__(self, other):
        """
        @TODO
        """
        other = self._adapt(other)
        if isinstance(other, Point2D):
            return self.dist2(other) / max(.00001, self.mod2()) < 1e-4
        else:
            return False

    def __str__(self):
        """
        @TODO
        """
        if self.name != "":
            return str(self.name)
        else:
            return "p(%2f, %2f)" % (self.x, self.y)

    def __repr__(self):
        """@TODO"""
        return str(self)

    def getBBox(self):
        """
        @TODO
        """
        from softap3.geom2.BBox import BBox2
        return BBox2(self, self)

    def getCoords(self):
        """@TODO"""
        return self.x, self.y

    def coords(self):
        """@TODO"""
        return self.x, self.y

    # -------------------------
    # as vector
    # -------------------------

    def getNormal(self, right=True):
        """@TODO"""
        return Point2D(-self.y, self.x) if right else Point2D(self.y, -self.x)

    def inverse(self):
        """@TODO"""
        return Point2D(-self.x, -self.y)

    def vProd(self, other):
        """@TODO"""
        return self.x * other.y - self.y * other.x

    def argument(self):
        """
        @TODO
        """
        return math.atan2(self.y, self.x)

    def angle(self, other):
        """
        angle with other vector, taken the points as vectors
        """
        return other.argument() - self.argument()


Point2 = Point2D
Vector2 = Point2D
