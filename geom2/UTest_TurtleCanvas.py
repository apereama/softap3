#!/usr/bin/python
# -*- coding: utf-8 -*-
# ====================
#  HURA 0.9
# ====================
#
#
# ====================
#  License
# ====================
#
#    Hura v0.9
#    Copyright (C)2013 Angel Perea Martinez ( angelpeream @ yahoo.com )
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
#    MA  02110-1301  USA
#
# (see gpl.txt for the complete licence text).
#

import unittest
from softap3.geom2.TurtleCanvas import *
from softap3.geom2.Point2D import Point2


#===========================
# THTestCase
#===========================
class TurtleCanvasTestCase(unittest.TestCase):
  """
  test of the pushkin facilitiess
  """

  #----------------------------------------
  def setUp(self):
    pass

  #-----------------------------------------
  def tearDown(self):
    pass

  #-----------------------------------------
  def test1(self):
    """
    """
    c = TCanvas((100,100))
    c.setTransform(Point2(-2,-2), Point(0,0), Point2(2,2), Point(100,100))    
    self.assertEqual(c.pointToPixel(Point2(0,0)), Point2(50,50))
    self.assertEqual(c.pixelToPoint(50,50), Point2(0,0))

    
#------------------------------
#        getTestSuite
#------------------------------
def getTestSuite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TurtleCanvasTestCase))
    return suite



if __name__ == '__main__':
   unittest.TextTestRunner(verbosity=2).run(getTestSuite())