"""
@TODO
"""

from Point3 import Point3
import math
from Body import Body

PI = 3.1415


class Triangle:
    """@TODO"""

    def __init__(self, p1, p2, p3):
        """@TODO"""
        self.p1, self.p2, self.p3 = p1, p2, p3


class Cylinder(Body):
    """
  @TODO
  """

    def __init__(self, height, radius, apexShift=(0, 0)):
        """
        """
        Body.__init__(self)
        self.height = height
        self.radius = radius
        self.apex = Point3(apexShift[0], apexShift[1], height)

    def getTriangles(self):
        """
         @TODO
        """
        triangles = []
        detail = 20
        h2 = self.height / 2.0

        for i in range(detail):
            theta = (i * 2 * PI) / detail
            thetaOld = ((i - 1) * 2 * PI) / detail

            # bottom
            triangles += Triangle(Point3(0, 0, -h2),
                                  Point3(self.radius * math.cos(theta),
                                         self.radius * math.sin(theta),
                                         -h2),

                                  Point3(self.radius * math.cos(thetaOld),
                                         self.radius * math.sin(thetaOld),
                                         -h2)
                                  )
            # top
            triangles += Triangle(Point3(0, 0, h2),
                                  Point3(self.radius * math.cos(theta),
                                         self.radius * math.sin(theta),
                                         h2),

                                  Point3(self.radius * math.cos(thetaOld),
                                         self.radius * math.sin(thetaOld),
                                         h2)
                                  )

            # side
            triangles += Triangle(Point3(self.radius * math.cos(theta),
                                         self.radius * math.sin(theta),
                                         h2),
                                  Point3(self.radius * math.cos(theta),
                                         self.radius * math.sin(theta),
                                         -h2),
                                  Point3(self.radius * math.cos(thetaOld),
                                         self.radius * math.sin(thetaOld),
                                         -h2)
                                  )

        return triangles
