currentConf = None  # will store the current configuration
                    # @KLUDGE could spoil multi-instances


class ConfigurationException(Exception):
    pass

NOT_GIVEN = object()
#=========================
# AppConfiguration
#=========================
class AppConfiguration:
    """
    class to handle application configurations. 
    Usually, line parameters override this file configuration
    """
    
    def __init__(self, text, overriden = None):
        """
        """
        if overriden == None: overriden = {}
       
        self.text = text
        self.params = {}
        self.parse(text)
        for name, value in list(overriden.items()):
            self.setParam(name, value)
                 
    #--------------------------------------
    def parse(self, text):
        """
        """
        lineNumber = 0
        for l in text.split("\n"):
            lineNumber += 1
            l = l.strip()
            if l == "": continue
            if l.startswith("#"): continue
            if "#" in l: l = l[:l.index("#")-1]  # strip comments
            try:
                name, value = l.split("=")
                name, value = name.strip(), value.strip()
                if value.lower() in ["NO", "FALSE", "DISABLED"]: value = False
                if value.lower() in ["YES", "TRUE", "ENABLED"]: value = True        

                # allows for name,a,b,c getting to namea, nameb, namec
                name = name + ","  
                names = name.split(",")[:-1] 
                for name in names:
                    self.params[name] = value
                    
            except:
                print("Error in line %i: '%s' " % (lineNumber, l))
                raise
        
           
    #-------------------------------------
    def getOption(self, name, default = NOT_GIVEN):
        """
        """
        if name in self.params:
            return self.params[name]
        
        elif default != NOT_GIVEN:
            return default
        else:
            raise KeyError("param '%s' not found" % name)
           
           
    #--------------------------------
    def getOptionAsInt(self, name, default = None):
        """
        """
        try:
            return int(self.getOption(name, default))       
        except ValueError:
            raise ConfigurationException("Option %s is not an integer" % name)
    
    #--------------------------------
    def getOptionInt(self, name, default = None):
        return self.getOptionAsInt(name, default)
    
    #------------------------------------
    def setOption(self, name, value):
        self.params[name] = value
        
    #-----------------------------------
    def update(self, d):
        """
        updates the options with a dictionary
        """
        if isinstance(d, dict):
            for key, value in list(d.items()):
                self.setOption(key, value)
              
        elif isinstance(d, AppConfiguration):
            for key, value in list(d.params.items()):
                self.setOption(key, value)
        else:
            raise ValueError("wrong class for param")
            
    #--------------------------------
    def getOptions(self):
        a = list(self.params.keys())
        a.sort()
        return a
        
