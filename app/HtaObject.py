#======================
#   HtaObject
#======================
class HtaObject:
    """
    """

    #--------------------------
    def __init__(self, name):
        """
        """
        self.name = name
        self.children = []
  
   
    #--------------------------
    def call(self, path, *args, **params):
        """
        """
        if "." in path:
            return self.getChild(path.split(".", 1)).call(*args, **params)
    
        elif hasattr(self, "fn_" + path):
            try:
                return getattr(self, "fn_" + path)(*args, **params)
            except:
                raise ValueError("error")
        
        else:
            raise ValueError("no function '%s' found" % path)
        
  
  
    #--------------------------
    def getChild(self, name):
        """
        """
        return self.children[name]
  
  
    #--------------------------
    def finalize(self):
        """
        """
        pass
  
    #-------------------------
    def warning(self, message):
        print("warning", message)
  
    #---------------------------
    def error(self, message):
        print("error", message)
  
    #--------------------------
    def info(self, message):
        print("info", message)
  
    #-------------------------
    def debug(self, message):
        print("debug", message)
    
    #---------------------------
    def fn_(self):
        """
        """
        return "default"