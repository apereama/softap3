from softap3.geom2.BBox import BBox as BBox2
from Point3 import Point3


#==================
# TriangleList
#==================
class TriangleList:
  """
  """
  def __init__(self, triangles):
    """
    """
    self.triangles = triangles
        
  #-----------------------
  def demaclate(self):
    """
    """
    return TriangleList(self.triangles)

  #-----------------------
  def flatten(self):
    """
    flatten the triangles cutting them
    """
    
    # 1) calc bboxes for fast calculations
    
    for t in self.triangles:
      t.visible = True
      t.bbox = BBox2()
      t.bbox.engulf(t.getBbox())
        
    #for  i in range(len(self.triangles)):
      #for j in range(i, len(self.triangles)):
        #t, s = self.triangles[i], self.triangles[j]
        #if s == t: continue
        #if not s.bbox.overlaps(t.bbox): continue
        
        #tMaxZ = max(t.p1.z, t.p2.z, t.p3.z)
        #tMinZ = min(t.p1.z, t.p2.z, t.p3.z)
        #sMaxZ = max(s.p1.z, s.p2.z, s.p3.z)
        #sMinZ = min(s.p1.z, s.p2.z, s.p3.z)
        #if tMaxZ < sMinZ: # t in front of s
          #s.visible = False
        #if tMinZ > sMaxZ: # s in front of t
          #t.visible = False
          
    flattened =  [t for t in self.triangles if t.visible == True]
    flattened.sort(key = lambda t: Point3(0,0,0).dist2(t.getMidPoint()))
    flattened.reverse()
    
    for t in self.triangles:
      del t.visible
      del t.bbox  
      
    return TriangleList(flattened)
  
  #------------------------------
  def draw(self, canvas):
    """
    """
    for t in self.triangles:
      t.draw(canvas)
      
  #---------------------------------
  def intersects(self, other):
    """
    """
    
    
    

