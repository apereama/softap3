# -*- coding: utf-8 -*-
import math
import unittest
from BBox import BBox3


#==================
# Primitive3
#==================
class Primitive3:
  pass



#===================
# Segment
#===================
class Segment(Primitive3):
  """
  an edge
  """
  
  #-----------------------
  def __init__(self, p1, p2, color="black"):
     self.p1 = p1
     self.p2 = p2
     self.color = color
     
  #-----------------------
  def draw(self, canvas):
    canvas.setColor(self.color)
    canvas.line(self.p1.coords()[:2], self.p2.coords()[:2])

  #-----------------------
  def transform(self, matrix):
    return Segment(matrix.apply(self.p1), matrix.apply(self.p2))
  
  #-----------------------
  def getBbox(self):
    """
    """
    b = BBox3()
    b.addPoints([self.p1, self.p2])
    return b

  #-----------------------  
  def getMidPoint(self):
    return (self.p1 + self.p2) / 3.0
  
  def transformBy(self, matrix):
      return self.transform(matrix)
  
  
#===================
# Triangle
#===================
class Triangle(Primitive3):
  """
  A triangle primitive
  """
  
  #------------------------------------------------
  def __init__(self, p1, p2, p3, color = (0,0,0), vis = "***"):
      self.color = color
      self.p1 = p1
      self.p2 = p2
      self.p3 = p3
      self.vis = vis

  #------------------------------------------------        
  def draw(self, canvas):
      #print self.p1, self.p2, self.p3
    
      canvas.setColor(self.color)
      if self.vis[0] == "*": canvas.line(self.p1.coords()[:2], 
                                          self.p2.coords()[:2])
      if self.vis[1] == "*": canvas.line(self.p2.coords()[:2], 
                                          self.p3.coords()[:2])
      if self.vis[2] == "*": canvas.line(self.p3.coords()[:2], 
                                          self.p1.coords()[:2])


  #------------------------------------------------
  def transform(self, matrix):
      return Triangle(matrix.apply(self.p1), matrix.apply(self.p2), 
                      matrix.apply(self.p3), self.color, self.vis)


  def transformBy(self, matrix):
      return self.transform(matrix)
  #------------------------------------------------
  def normal(self):
      return (self.p2 - self.p1) ** (self.p3 - self.p1)


  #----------------------------------
  def getBbox(self):
    """
    """
    b = BBox3()
    b.addPoints([self.p1, self.p2, self.p3])
    return b
  
  #--------------------------------------
  def getMidPoint(self):
    """
    returns the center of the triangle
    """
    return (self.p1 + self.p2 + self.p3) / 3.0
      
    
#=================
# VertexPool
#=================
class VertexPool:
  """
  """
  pass


#===============
# EdgePool
#===============
class EdgePool:
  """
  """
  pass



#================
# TrianglePool
#================
class TrianglePool:
  """
  """
  pass

