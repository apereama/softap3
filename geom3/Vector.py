"""
@TODO
"""

# =====================
# Vector3
#
# Base class for vectors and points in R3
#
#
# Author: A.Perea (a.perea@csic.es)
# Revision: 08/2016
# License: lgpl2
# ====================

import math

toDegrees = 360 / (2 * math.pi)

MIN_DIST = 1E-4  # min distance to be considered same point


class Vector:
    """
    A generic vector or a point
    """

    def __init__(self, x, y, z):
        """@TODO"""
        self.x, self.y, self.z = float(x), float(y), float(z)

    def toSDL(self, w):
        """@TODO"""
        w.write("<%4.4f, %4.4f, %4.4f>" % (self.x, self.y, self.z))

    def dist2(self, other):
        """
        returns the squared distance
        """
        dx = self.x - other.x
        dy = self.y - other.y
        dz = self.z - other.z

        return dx * dx + dy * dy + dz * dz

    def dist(self, other):
        """
        returns the distance
        """
        return math.sqrt(self.dist2(other))

    def distance(self, other):
        """
        alias for dist
        """
        return self.dist(other)

    def shift(self, dx, dy, dz):
        """
        same as operator +
        """
        return Vector(self.x + dx, self.y + dy, self.z + dz)

    def coords(self):
        """
        returns a tuple with the coordinates
        """
        return self.x, self.y, self.z

    def invert(self):
        """
        inverts this point around 0,0,0
        """
        return Vector(-self.x, -self.y, -self.z)

    def toStr(self):
        """
        @TODO
        """
        print("(%f,%f,%f)" % (self.x, self.y, self.z))

    def equal(self, other, strict=False):
        """
        @TODO
        """
        if not isinstance(other, Vector):
            return False

        if strict:
            if self.x != other.x: return False
            if self.y != other.y: return False
            if self.z != other.z: return False
            return True
        else:
            if abs(self.x - other.x) > MIN_DIST: return False
            if abs(self.y - other.y) > MIN_DIST: return False
            if abs(self.z - other.z) > MIN_DIST: return False
            return True
            # return self.distance(other) < 1e-3

    def vProd(self, other):
        """
        vectorial product
        """
        a = self.y * other.z - self.z * other.y
        b = self.z * other.x - self.x * other.z
        c = self.x * other.y - self.y * other.x
        return Vector(a, b, c)

    def __add__(self, other):
        """
        sum with other vector
        """
        return Vector(self.x + other.x, self.y + other.y, self.z + other.z)

    def __sub__(self, other):
        """
        """
        return Vector(self.x - other.x, self.y - other.y, self.z - other.z)

    def __pow__(self, other):  # vectorial product
        """
        vectorial product
        """

        return Vector(self.y * other.z - self.z * other.y,
                      self.z * other.x - self.x * other.z,
                      self.x * other.y - self.y * other.x)

    def __eq__(self, other):
        """
        """
        return self.equal(other)

    def __mul__(self, other):  # scalar product
        """
        scalar product
        """

        if isinstance(other, float):
            return Vector(self.x * other, self.y * other, self.z * other)
        elif isinstance(other, int):
            other = float(other)
            return Vector(self.x * other, self.y * other, self.z * other)
        else:
            return self.x * other.x + self.y * other.y + self.z * other.z

    def __div__(self, other):
        """
        """
        if isinstance(other, float):
            return Vector(self.x / other, self.y / other, self.z / other)

        elif isinstance(other, int):
            other = float(other)
            return Vector(self.x / other, self.y / other, self.z / other)

        else:
            raise ValueError("invalid class for operand")

    def __neg__(self):
        """
        """
        return Vector(-self.x, -self.y, -self.z)

    def __rmul__(self, other):
        """
        """
        return other.__mul__(self)

    def __uminus__(self):
        """
        """
        return Vector(-self.x, -self.y, -self.z)

    def modulus(self):
        """
        @TODO
        """
        return math.sqrt(self.x * self.x + self.y * self.y + self.z * self.z)

    def size(self):
        """
        @TODO
        """
        return self.modulus()

    def length(self):
        """
        @TODO
        """
        return self.modulus()

    def scale(self, s):
        """
        @TODO
        """
        return Vector(self.x * s, self.y * s, self.z * s)

    def normalize(self, factor=1.0):
        """
        @TODO
        """
        modulus = math.sqrt(self.x * self.x + self.y * self.y + self.z * self.z)
        modulus += 1e-45
        return Vector(factor * self.x / modulus, factor * self.y / modulus, factor * self.z / modulus)

    def angle(self, other):
        """
        @TODO
        """
        return math.acos((self * other) / (self.modulus() * other.modulus()))

    def project(self, other):
        """
        @TODO
        """
        other.project(self)

    def between(self, other, alfa):
        """
        @TODO
        """
        return Vector(other.x * alfa + (1.0 - alfa) * self.x,
                      other.y * alfa + (1.0 - alfa) * self.y,
                      other.z * alfa + (1.0 - alfa) * self.z)

    def midPoint(self, other, alpha=0.5):
        """
        @TODO
        """
        return self.between(other, alpha)

    def toPolar(self, inDegrees=False):
        """
        @TODO
        """
        theta = math.atan2(math.sqrt(self.x * self.x + self.y * self.y), self.z)
        phi = math.atan2(self.y, self.x)
        radius = math.sqrt(self.x * self.x + self.y * self.y + self.z * self.z)

        if inDegrees:
            theta = theta * toDegrees
            phi = phi * toDegrees
        return radius, theta, phi

    def __str__(self):
        """
        @TODO
        """
        return "(%f, %f, %f)" % (self.x, self.y, self.z)

    def __repr__(self):
        """
        @TODO
        """
        return str(self)


def centerOfMass(*points):
    """
    @TODO
    """
    ll = float(len(points))
    x = sum([p.x for p in points]) / ll
    y = sum([p.y for p in points]) / ll
    z = sum([p.z for p in points]) / ll
    return Point3(x, y, z)


Point3D = Vector
Point3 = Vector
Vector3 = Vector
