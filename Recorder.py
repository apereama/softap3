# coding=utf-8

"""
@TODO <short description of the module>

@TODO <description of the module>

:copyright: 2016 by Angel Perea Martinez (angelpeream@yahoo.com), see AUTHORS
            for more details

:license: lGPLv3, see LICENSE for more details
"""
import time

#=================
#  Recorder
#=================
class Recorder:
    """
    a container to store data about a running application
    Is able to store events? or each independently??
    """

    #--------------------------------

    def __init__(self, name="blackbox"):
        self.name = name
        self.data = {}

    #-----------------------------------
    def command(self, command, *params, **args):
        """
        """
        return "command"

    #--------------------------------
    def log(self, name, value):
        """
        logs a changing value
        """
        if not name in self.data:
            self.data[name] = []
        self.data[name].append((time.time(), value))

    #------------------------------
    def logValue(self, name, value):
        """
        """
        return self.log(name, value)

    #----------------------------
    def getValue(self, name):
        """
        """
        return self.data[name]
    
    #-----------------------------------
    def setValue(self, name, value):
        """
        sets an static value
        """
        self.data[name] = value

    #------------------------------------
    def incValue(self, name, increment=1):
        """
        """
        self.data[name] += increment

    #----------------------------------
    def save(self, filename=None):
        """
        """
#         fname = filename if filename is not None else "blackbox"
        raise NotImplemented("not implemented yet")

    #------------------------------------
    def getHist(self, logName, minValue, maxValue, nBins):
        """
        returns an histogram of the values
        """
        raise NotImplemented("not implemented yet")

    #-------------------------------------
    def getLog(self, logName):
        """
        returns a list of pairs
        """
        raise NotImplemented("not implemented yet")

    #--------------------------------------
    def plotLog(self, logName, start, end, mode="text"):
        """
        plots the given logName(s)
        """
        raise NotImplemented("not implemented yet")

    #---------------------------------------
    def plotHist(self, logName, minValue, maxValue, nBins):
        """
        """
        raise NotImplemented("not implemented yet")
