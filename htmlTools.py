from .shellTools import getTraceback
#------------------------
# formatException
#------------------------
def formatExceptionHtml(msg, exception):
    """
    returns an exception and the
    traceback formatted for html
    """
    return "%s (%s)<br><pre>%s</pre>" % (msg, exception, getTraceback())


#------------------------
# doOrExplain
#-----------------------
def doOrExplainHtml(fn, msg):
    """
    calls a function and if not succeeds, displays the error
    """
    try:
        return fn()
    except BaseException as e:
        return formatExceptionHtml(msg, e)


#------------------------------------
#  xmlTag
#------------------------------------
def xmlTag(tagname, parameters=None, content=None):
    """
    produces xml tags. If content given, returns also the closing tag
    """
    s = "<" + tagname
    if parameters is not None:
        s += " " + parameters

    s = s + ">"

    if content is not None:
        s = s + content + "</" + tagname + ">"

    return s


#------------------------
# gettextBox
#------------------------
def getTextBoxHtml(text):
    return   """
<table align="center" width="70%" cellpadding="20"  bgcolor=#ffffea
       style="border-width: 1px 1px 1px 1px; border-spacing:;
              border-style: outset outset outset outset;  border-color:#aaaaaa;
              border-collapse: collapse;
              bgcolor:rgb(250,200,200);
              align:center;"
><tr><td> <pre>""" + text + "</pre></td></tr></table>"


