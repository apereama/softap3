"""
@TODO
"""

import math
from softap3.geom2.Point2D import Point2D as Point2
from softap3.geom2.BBox import BBox

INDEG = 360 / 6.28


class Matrix3:
    """
    A General Transformation in 2 dimensions
    """

    def __init__(self, a11, a12, a13,
                 a21, a22, a23,
                 a31, a32, a33):

        self.a11, self.a12, self.a13 = float(a11), float(a12), float(a13)
        self.a21, self.a22, self.a23 = float(a21), float(a22), float(a23)
        self.a31, self.a32, self.a33 = float(a31), float(a32), float(a33)

    def apply(self, p, c=1.0):
        """
        @TODO
        """
        if isinstance(p, BBox):
            points = p.getPoints()
            p1, p2 = points[0], points[2]
            return BBox(self * p1, self * p2)

        elif isinstance(p, Point2):
            nx = self.a11 * p.x + self.a12 * p.y + self.a13 * c
            ny = self.a21 * p.x + self.a22 * p.y + self.a23 * c
            _c = self.a31 * p.x + self.a32 * p.y + self.a33 * c
            return Point2(nx, ny)

        else:
            p.transform(self)

    def getMatrix(self):
        """
        because TMatrix is a generic placement
        """
        return self

    def combine(self, other):
        """
        @TODO
        """
        if other is None:
            return self

        if isinstance(other, Point2):
            return self.apply(other)

        elif isinstance(other, BBox):
            points = other.getPoints()
            p1, p2 = points[0], points[2]
            return BBox(self * p1, self * p2)

        elif isinstance(other, tuple) and (isinstance(other[0], int) or isinstance(other[0], float)):
            return self.combine(Point2(other[0], other[1]))

        elif isinstance(other, tuple) or isinstance(other, list):
            return [self.combine(o) for o in other]

        elif isinstance(other, Matrix3):
            a11 = self.a11 * other.a11 + self.a12 * other.a21 + self.a13 * other.a31
            a12 = self.a11 * other.a12 + self.a12 * other.a22 + self.a13 * other.a32
            a13 = self.a11 * other.a13 + self.a12 * other.a23 + self.a13 * other.a33

            a21 = self.a21 * other.a11 + self.a22 * other.a21 + self.a23 * other.a31
            a22 = self.a21 * other.a12 + self.a22 * other.a22 + self.a23 * other.a32
            a23 = self.a21 * other.a13 + self.a22 * other.a23 + self.a23 * other.a33

            a31 = self.a31 * other.a11 + self.a32 * other.a21 + self.a33 * other.a31
            a32 = self.a31 * other.a12 + self.a32 * other.a22 + self.a33 * other.a32
            a33 = self.a31 * other.a13 + self.a32 * other.a23 + self.a33 * other.a33

            return Transformation(a11, a12, a13, a21, a22, a23, a31, a32, a33)

        else:
            return other.transform(self)

        # raise ValueError("combine called with %s" % other.__class__)

    def __str__(self):
        """
      """
        s = ["%f %f %f " % (self.a11, self.a12, self.a13),
             "%f %f %f " % (self.a21, self.a22, self.a23),
             "%f %f %f " % (self.a31, self.a32, self.a33)]
        return "\n".join(s)

    def __mul__(self, other):
        """
        """
        return self.combine(other)

    def determinant(self):
        """
        @TODO
        """
        a11, a12, a13 = self.a11, self.a12, self.a13
        a21, a22, a23 = self.a21, self.a22, self.a23
        a31, a32, a33 = self.a31, self.a32, self.a33

        det = a11 * (a22 * a33 - a23 * a32) + a12 * (a23 * a31 - a21 * a33) + a13 * (a21 * a32 - a22 * a31)
        return det

    def inverse(self):
        """
        a11 a12 a13
        a21 a22 a23
        a31 a32 a33
        """
        a11, a12, a13 = self.a11, self.a12, self.a13
        a21, a22, a23 = self.a21, self.a22, self.a23
        a31, a32, a33 = self.a31, self.a32, self.a33
        det = self.determinant()

        b11 = (a22 * a33) - (a23 * a32)
        b12 = (a21 * a33) - (a23 * a31)
        b13 = (a21 * a32) - (a22 * a31)
        b21 = (a12 * a33) - (a13 * a32)
        b22 = (a11 * a33) - (a13 * a31)
        b23 = (a11 * a32) - (a12 * a31)
        b31 = (a12 * a23) - (a13 * a22)
        b32 = (a11 * a23) - (a13 * a21)
        b33 = (a11 * a22) - (a12 * a21)
        det = float(det)
        m = Matrix3(b11 / det, -b21 / det, b31 / det,
                    -b12 / det, b22 / det, -b32 / det,
                    b13 / det, -b23 / det, b33 / det)

        return m


class Transformation(Matrix3):
    """@TODO"""
    pass


def getTMatrix(rot=None, offset=None):
    """@TODO"""
    return RotationTrans(rot) * Offset(offset[0], offset[1])


class RotationTrans(Transformation):
    """
    @TODO
    """

    def __init__(self, theta=0.0):
        s = math.sin(theta)
        c = math.cos(theta)
        Transformation.__init__(self, c, -s, 0.0,
                                s, c, 0.0,
                                0.0, 0.0, 1.0)


def makeRotation(theta, center=None):
    """@TODO"""
    r = RotationTrans(theta)
    if center != None:
        x, y = center.getCoords()
        r = r * Offset(-x, -y)
    return r


RotateTrans = RotationTrans


class Offset(Transformation):
    """@TODO"""
    def __init__(self, dx, dy=None):
        """@TODO"""
        if dy is None and isinstance(dx, tuple):
            dx, dy = dx[0], dx[1]
        elif dy is None:
            dx, dy = dx.x, dx.y

        dx, dy = float(dx), float(dy)
        Transformation.__init__(self, 1.0, 0.0, dx,
                                0.0, 1.0, dy,
                                0.0, 0.0, 1.0)

def makeShiftTrans(self, dx, dy):
    """@TODO"""
    return Matrix3(0.0, 0.0, dx,
                   0.0, 0.0, dy,
                   0.0, 0.0, 1.0)


OffsetTrans = Offset
ShiftTrans = Offset


class Scale(Transformation):
    """@TODO"""

    def __init__(self, sx, sy):
        """@TODO"""
        sx, sy = float(sx), float(sy)
        Transformation.__init__(self, sx, 0.0, 0.0,
                                0.0, sy, 0.0,
                                0.0, 0.0, 1.0)


ScaleTrans = Scale


class FourPoints(Transformation):
    """
    @TODO
    """

    def __init__(self, a1, b1, a2, b2, x1, y1, x2, y2):
        """@TODO"""
        ra, rb = float(a2 - a1), float(b2 - b1)
        rx, ry = float(x2 - x1), float(y2 - y1)

        s1 = ra / rx
        o1 = a1 - x1 * ra / rx
        s2 = rb / ry
        o2 = b1 - x2 * ra / ry

        Transformation.__init__(self, s1, 0.0, o1,
                                0.0, s2, o2,
                                0.0, 0.0, 1.0)

def getViewportTransformation(bb1: BBox, bb2: BBox, stretch=False):
    """@TODO"""
    return FourPoints(bb1.xMin, bb1.yMin, bb1.xMax, bb1.yMax,
                      bb2.xMin, bb2.yMin, bb2.xMax, bb2.yMax)

class DummyTrans(Transformation):
    """
    @TODO
    """
    def __init__(self):
        """@TODO"""
        Transformation.__init__(self, 1.0, 0.0, 0.0,
                                0.0, 1.0, 0.0,
                                0.0, 0.0, 1.0)


UnitTransformation = DummyTrans


def getJoiningTransformation(p1, p2, q1, q2):
    """
    returns a transformation that brings p1 to q1 and p2 to the line q1..q2
    """
    for p in [p1, p2, q1, q2]:
        if not isinstance(p, Point2):
            raise ValueError("p is ", p, "this is only for points2")

    d1, d2 = p2 - p1, q2 - q1
    if abs(d1.lenght() - d2.lenght()) > 0.1: raise "segments do not match"
    angle = d2.angle(d1)
    t1 = Offset(-p1.x, -p1.y)
    r2 = RotationTrans(-angle)
    t3 = Offset(q1.x, q1.y)

    return t3 * r2 * t1


def joinTransform(p1, p2, q1, q2):
    """
    @TODO
    """
    return getJoiningTransformation(p1, p2, q1, q2)


def getCenteringTransformation(center1, center2, scale):
    """
    @TODO
    """
    t1 = Offset(center2.x - center1.x, center2.y - center1.y)
    t2 = Scale(scale, scale)
    return t2 * t1


def getTransBBox(bbox1, bbox2, stretch=False):
    """
      returns a transformation that moves p1 p2 to q1 q2
      @param bbox1
      @param bbox2
      @param stretch

      | sx 0  dx  | p1x | = q1x ;
      | 0 sy  dy  | p1y | = q1y ;
      | 0  0   1  |  1  | = 1   ;
      
      sx  = (q2x - q1x) / (p2x - p1x)
      sy  = (q2y - q1y) / (p2y - p1y)
      
      
      if no stretch
      
          this transformation sends the center of bbox1 to the center of bbox2
      bb1.east => bb2.east
      bb1.west => bb2.west
    
      OR 
    
      bb1.north => bb2.north
      bb1.south => bb2.south

      """
    # @TODO STRETCH IS NOT USED

    if stretch:
        p1, p2 = bbox1.getCorners()
        q1, q2 = bbox2.getCorners()

        sx = (q2.x - q1.x) / float(p2.x - p1.x)
        sy = (q2.y - q1.y) / float(p2.y - p1.y)
        dx = q1.x - sx * p1.x
        dy = q1.y - sy * p1.y

        return Matrix3(sx, 0, dx,
                       0, sy, dy,
                       0, 0, 1)

    else:
        w1, h1 = bbox1.getSize()
        w2, h2 = bbox2.getSize()

        alpha = min(w2 / w1, h2 / h1)
        dx, dy = (bbox2.getCenter() - bbox1.getCenter())
        t = ScaleTrans(alpha, alpha) * ShiftTrans(dx, dy)
        return t
