#from rongo.Common import guard, MimeData

#====================
# TextDocument
#====================
class TextWriter:
    """
    A basic Text writer. just indentation, lines, etc 
    """    
    #--------------------------
    def __init__(self):
        """
        """
        self.lines = []
        self.lastLine = ""
        self.indentation = ""
   
    #--------------------------
    def write(self, text):
        """
        """     
        self.lastLine += text
    
   
    #--------------------------
    def writeLine(self, line):
        """
        """
        self.write(line)
        for l in self.lastLine.split("\n"):            
            self.lines.append(self.indentation + l)
            
        self.lastLine = ""
    
    #---------------------------
    def writeValue(self, name, value):
        self.writeLine(f"{name}:{value}")
        
    #---------------------------
    def indent(self):
        self.indentation += "    "
        
    #---------------------------
    def dedent(self):
        if self.indentation != "":
            self.indentation = self.indentation[:-4]
            
    #------------------------
    def getText(self):
        return "\n".join(self.lines + [self.lastLine])
    
    def openSection(self, name):
        self.write(name)
        self.indent()
        
    def closeSection(self):
        self.dedent()

    def flush(self):
        self.writeLine("")
