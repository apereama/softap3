from softap3.geom2.Point2D import Vector2
from softap3.listTools import getPairs
from Point2D import Point2
from Segment import Segment


def getTriples(ll):
  return []

#==========================
# Poygon2
#==========================
class Polygon2:
  """
  A Closed, possibly convex Polygon
  """
  
  #------------------------------------
  def __init__(self, points, color=(0,0,0)):
    """
    """
    self.points = points
    self.color=color

    
  #-------------------------------------
  def allowed(self, points, p1, p2, p3):
    """
    returns True if the triangle given by p1, p2, p3, doesnt contain
    any point of the polygon (except themselves)

    This function is used by triangulate to asses potential earclips
    """
    
    for p in points:      
      if p == p1 or p == p2 or p == p3:
        continue
      
      a = (p2 - p1).vProd(p - p1)
      b = (p3 - p2).vProd(p - p2)
      c = (p1 - p3).vProd(p - p3)
      
      if ((a>0.0) and (b>0.0) and (c>0.0)) or ((a<0.0) and (b<0.0) and (c<0.0)):
        return False
            
    return True

  #---------------------------------------
  def isPositive(self):
    """
    returns true if is a closed polygon with points in anticlockwise
    """        
    for p1, p2, p3 in getTriples(self.points, cycle=True):
      a = p2 - p1
      b = p3 - p2

  #------------------------------------
  def isPositive(self):
    """
    returns true if is orientated anticlockwise
    DOESNT WORK
    """
    tot = 0
    for p1, p2, p3 in getTriples(self.points, cycle=True):
      a, b = (p3 - p2), (p2-p1)       
      tot + a.angle(b)

    print("tot", tot)
    return tot > 0.0
      
  #------------------------------------
  def triangulate(self):
    """
    Clips a polygon returning a list of triples (triangles)
    returns a list of triangles
    """
    
    points = list(self.points[:])            
    triangles = []
    
    while len(points) > 3:      
      l = len(points)
      
      bestAngle ,bestPoints = 1E-26, None # the index and the point for the best ear
      
      for p1, p2, p3 in getTriples(points, cycle=True):
        a,b = p2 - p1, p3 - p2        
        t = a.normalize().vProd(b.normalize()) # maximize sen
        s = a.normalize() * b.normalize()
        
        #print "--------"
        #print p1, p2, p3
        #print "t=", t
        #print "allowed=", self.allowed(points, p1, p2, p3)
        #print "best angle=", bestAngle
        #print "points", len(points)
        #print "sign", Polygon2(points).isPositive()

        
        if  (t > 0) and (t > bestAngle) and self.allowed(points, p1, p2, p3): # allowed cares that taking out
                                                                   # this triangle doesnt catch any
                                                                   # polygon point inside it
          bestAngle = t;  bestPoints = p1, p2, p3;


      if bestPoints == None: # hack. The polygon is reversed, so all the angles are negatives.      
          points.reverse()   # Reverse
          continue           # and start again (next iteration without taking out any point)

      # now take the biggest angle
      
      a,b,c = bestPoints # take the triangle      
      points.remove(b);
      triangles.append((a,b,c))

    triangles.append(tuple(points))
    return triangles # the last one is invalid

    
  #-----------------------------
  def shift(self, x, y):
    """
    shifts this polygon
    """
    for p in self.points:
      p.x += x;
      p.y += y;
    return self

    
  #----------------------------------
  def center(self):
    """
    returns the center of mass of this polygon
    """
    l = float(len(self.points))
    return Point2(sum([p.x for p in self.points])/l, sum([p.y for p in self.points]) / l)

    
  #----------------------------------
  def scale(self, scale):
    """
    scales this polygons with respect to the center-of-mass
    """
    c = self.center()
    points = [Segment(c,p).midPoint(scale) for p in self.points]
    return Polygon2(points, self.color)

    
  #-------------------------------
  def reverse(self):
    """
    Reverses the direction of this polygon
    """
    self.points.reverse()
         

  #-------------------------------
  def paint(self, canvas):
    """
    @param canvas canvas to paint on
    """    

    canvas.setColor(self.color);
    for a, b in getPairs(self.points, cycle=True):      
      canvas.line(a,b)

      
  #---------------------------
  def __str__(self):
    return str(self.earclip())
  
      
      
