"""
@TODO
"""

from .Point2D import Point2D
from .BBox import BBox2


class Segment:
    """
    @TODO
    """

    def __init__(self, p1, p2):
        """
    """
        if isinstance(p1, tuple): p1 = Point2D(*p1)
        if isinstance(p2, tuple): p2 = Point2D(*p2)

        if p1 == p2:
            raise ValueError("same point")

        if not (isinstance(p1, Point2D) and isinstance(p2, Point2D)): raise ValueError(
            "wrong parameters %s %s" % (p1, p2))

        self.p1, self.p2 = p1, p2

    def trim(self, other):
        """
        @TODO
        """
        raise NotImplemented()
        # if isinstance(other, BBox2):
        #     return other.trimSegment(self)
        # else:
        #     raise ValueError("Unknown object (%s) to trim unto" % other.___class__)

    def intersection(self, other):
        """
        returns the point of intersection of this segment plus the two percents of the segment, or None
        """
        # a*p1 + (1-a)*p2 = bq1 + (1-b) q2
        # a * p1x + p2x - a*p2x = b*q1x + q2x - b*q2x
        # a * p1y + p2y - a*p2y = b*q1y + q2y - b*q2y

        # x (p1x - p2x) + y (q2x - q1x) = (q2x - p2x)
        # x (p1y - p2y) + y (q2y - q1y) = (q2y - p2y)

        # A x + B y = P
        # C x + D y = Q

        p1, p2 = self.p1, self.p2
        q1, q2 = other.p1, other.p2

        A = p1.x - p2.x
        B = q2.x - q1.x
        P = q2.x - p2.x
        C = p1.y - p2.y
        D = q2.y - q1.y
        Q = q2.y - p2.y

        DET = D * A - B * C
        if DET == 0:
            return None, None, None  # they are parallel

        alpha = (P * D - B * Q) / DET
        beta = (A * Q - P * C) / DET

        if (0 < alpha < 1) and (0 < beta < 1):
            return p1 * alpha + p2 * (1.0 - alpha), 1.0 - alpha, 1.0 - beta  # alfa is wrongly used
        else:
            return None, None, None

    def invert(self):
        """
        @TODO
        """
        return Segment(self.p2, self.p1)

    def midPoint(self, factor=0.5):
        """
    returns a point along the line
    """
        if self.getLength() < 1e-10: return self.p1
        return self.p1 * (1.0 - factor) + self.p2 * factor

    def isOrto(self):
        """
        @TODO
        """
        return (self.p1.x == self.p2.x) or (self.p1.y == self.p2.y)

    def getPoint(self, pos):
        """
        @TODO
        """
        if self.getLength() < 1e-10:
            print("warning. called with small segment")
            return self.p1

        if pos < 0: pos += self.getLength()
        factor = abs(float(pos)) / self.length()

        return self.p1 * (1.0 - factor) + self.p2 * factor

    def distance(self, other):
        """
        returns the distance to other element
        """
        raise NotImplemented("Not Implemented")
        # if isinstance(other, Point2D):
        #  a = other.x - self.x
        #  b = other.y - self.y
        #  return math.sqrt(a * a + b * b)

    def length(self):
        """@TODO
        """
        return self.p1.distance(self.p2)

    def getLength(self):
        """@TODO"""
        return self.length()

    def chop(self, a, b):
        """
        cuts a, b from the ends of the segment
        """
        ll = float(self.length())
        return Segment(self.midPoint(a / ll), self.midPoint(1 - (b / ll)))

    def chopRel(self, a, b):
        """@TODO"""
        return Segment(self.midPoint(a), self.midPoint(1 - b))

    def scale(self, x, y):
        """@TODO"""
        raise NotImplemented("Not Implemented")
        # return Point2D(self.x * x, self.y * y)

    def shift(self, dx, dy):
        """@TODO"""
        raise NotImplemented("Not Implemented")
        # return Point2D(self.x + dx, self.y + dy)

    def clone(self):
        """
        returns a clone of this point
        """
        raise NotImplemented("Not Implemented")
        # return Point2D(self.x, self.y)

    def _adapt(self, other):
        """@TODO
        """

        if isinstance(other, tuple):
            other = Point2D(other[0], other[1])

        return other

    def __add__(self, other):
        """
        @TODO
        """
        raise NotImplemented("Not Implemented")
        # other = self._adapt(other)
        # return Point2D(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        """
        """
        raise NotImplemented("Not Implemented")
        # other = self._adapt(other)
        # return Point2D(self.x - other.x, self.y - other.y)

    def __eq__(self, other):
        """
        @TODO
        """
        # raise NotImplemented("Not Implemented")
        other = self._adapt(other)
        return (self.p1 == other.p1 and self.p2 == other.p2) or (self.p1 == other.p2 and self.p2 == other.p1)

    def branch(self, midPoint, alpha, length):
        """@TODO"""
        raise NotImplemented()

    def toVector(self):
        """
        @TODO
        """
        print("----------------", self.p2, self.p1)
        return self.p2 - self.p1

    def __str__(self):
        """
        """
        # raise NotImplemented("Not Implemented")
        return "%s-%s" % (self.p1, self.p2)

    def __repr__(self):
        """@TODO"""
        return str(self)

    def getBBox(self):
        """@TODO
        """
        return BBox2(self.p1, self.p2)
