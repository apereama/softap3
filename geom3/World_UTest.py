
import unittest

#############################
#  TESTS
############################

#------------------------------------------
from Point3 import *
from Camera import *
from World import *
from Brick import *
from Cone import *


#===============
#
#================
class WorldTestCase(unittest.TestCase):
    """
    tests sobre otras funciones de la base
    """
    
    def setUp(self):
        pass
    
    #--------------------------
    def tearDown(self):
        pass        
   
    #---------------------------
    def test1(self):
      """
      """
      w = World()
      #w.addObject(Brick(120,120,120))
      w.addObject(Cone(22,12))
      
      cam = Camera(Point3(7,5,3), Point3(0,0,0))
      w.project(cam, (1000,1000), "testWorld.png")
      
      
                
      
        

if __name__ == '__main__':
     suite = unittest.TestSuite()
     suite.addTest(unittest.makeSuite(WorldTestCase))
     unittest.TextTestRunner(verbosity=2).run(suite)


