"""
@TODO
"""

from softap3.geom2.Point2D import Point2D as Point
#from softap3.geom3.BBox import BBox3
from softap3.safeCheck import safeDiv

Point2 = Point

class BBox:
    """
    a bounding box
    """

    def __init__(self, p1=None, p2=None):
        """
        """
        self.d = None
        self.c = None
        self.b = None
        self.a = None
        p1, p2 = self.checkPoint(p1), self.checkPoint(p2)

        if isinstance(p1, list) and p2 is None:
            self.empty = True
            self.x1, self.y1, self.x2, self.y2 = None, None, None, None
            for p in p1:
                self.addPoint(p)

        elif p1 is None and p2 is None:
            self.empty = True
            self.x1, self.y1, self.x2, self.y2 = None, None, None, None

        elif p2 is None:
            self.x1 = p1.x
            self.y1 = p1.y
            self.x2 = self.x1
            self.y2 = self.y1
            self.empty = False
        elif p1 is None:  # on has a value
            self.x1 = p2.x
            self.y1 = p2.y
            self.x2 = self.x1
            self.y2 = self.y1
            self.empty = False
        else:
            self.x1, self.x2, self.y1, self.y2 = p1.x, p2.x, p1.y, p2.y
            self.empty = False

        self.handles = {}
        self.update()

    def update(self):
        """
        @TODO
        """
        if None in (self.x1, self.x2, self.y1, self.y2):
            self.empty = True
            self.handles = {}
        else:
            self.a, self.b = Point(self.x1, self.y1), Point(self.x2, self.y1)
            self.c, self.d = Point(self.x1, self.y2), Point(self.x2, self.y2)
            self.empty = False            
            self.handles = {"top.center": (self.a + self.b) / 2.0,
                            "top.left": self.a,
                            "top.right": self.b,
                            "bottom.center": (self.c + self.d) / 2.0,
                            "bottom.right": self.d,
                            "bottom.left": self.c,
                            "left.top": self.a,
                            "left.bottom": self.c,
                            "left.center": (self.a + self.c) / 2.0,
                            "right.top": self.b,
                            "right.bottom": self.d,
                            "right.center": (self.b + self.d) / 2.0,
                            "center": (self.a + self.d) / 2.0,
                            }

    def checkPoint(self, point):
        """
        @TODO
        """
        if isinstance(point, tuple):
            return Point(point[0], point[1])
        else:
            return point

    def getCorners(self):
        """
        return the minimum and maximum corners
        """
        return Point2(self.x1, self.y1), Point2(self.x2, self.y2)

    def display(self, canvas, trans):
        """
        @TODO
        """
        canvas.setColor(1, 0, 0)
        p1, p2, p3, p4 = self.getPoints()
        delta = self.getSize()[0] / 10
        canvas.rectangle(trans * (p1 - Point(delta, delta)),
                         trans * (p3 + Point(delta, delta)))

    def addPoint(self, p):
        """
        @TODO
        """
        if self.empty:
            self.x1, self.y1, self.x2, self.y2 = p.x, p.y, p.x, p.y

        else:
            self.x1, self.x2 = min(self.x1, p.x), max(self.x2, p.x)
            self.y1, self.y2 = min(self.y1, p.y), max(self.y2, p.y)

        self.update()

    def addPoints(self, points):
        """
        @TODO
        """
        for p in points:
            self.addPoint(p)

    def area(self):
        """@TODO"""
        return self.height() * self.width() if not self.isEmpty() else 0

    def engulf(self, other):
        """@TODO
        """
        if isinstance(other, list):
            for o in other:
                self.engulf(o)
            return

        if isinstance(other, tuple) and len(other) == 2:
            self.addPoint(Point2(other[0], other[1]))
            return

        if not (isinstance(other, BBox) or isinstance(other, BBox3)):
            print(other.__class__)
            print(isinstance(other, BBox3))
            other = other.getBBox()

        if other.empty: return
        # if the other is not a valid BBox, do not do anything

        if self.empty:
            self.x1, self.y1 = other.x1, other.y1
            self.x2, self.y2 = other.x2, other.y2
        else:
            self.x1 = min(self.x1, self.x2, other.x1, other.x2)
            self.x2 = max(self.x1, self.x2, other.x1, other.x2)
            self.y1 = min(self.y1, self.y2, other.y1, other.y2)
            self.y2 = max(self.y1, self.y2, other.y1, other.y2)

        self.update()
        self.empty = self.empty and other.empty

    def expand(self, dx, dy):
        """
        @TODO
        """
        if self.empty: raise ValueError("Bbox is empty")
        self.x1 -= dx
        self.y1 -= dy
        self.x2 += dx
        self.y2 += dy

        self.update()

    def expandRel(self, a, b):
        """
        @TODO
        """
        if self.empty: raise ValueError("Bbox is empty")
        dx = (self.x2 - self.x1) * float(a) / 2.0
        dy = (self.y2 - self.y1) * float(b) / 2.0
        self.expand(dx, dy)
        self.update()

    def size(self):
        """
        @TODO
        """
        if self.empty: return 0.0, 0.0
        return (self.x2 - self.x1), (self.y2 - self.y1)

    def height(self):
        """@TODO"""
        return self.y2 - self.y1

    def width(self):
        """@TODO"""
        return self.x2 - self.x1

    def getBBox(self):
        """@TODO"""
        return self

    def isValid(self):
        """
        @TODO
        """
        return not self.empty

    def isEmpty(self):
        """@TODO"""
        return self.empty

    def inside(self, point):
        """@TODO"""
        return self.x2 > point.x > self.x1 and self.y2 > point.y > self.y1

    def shift(self, vector, dy=None):
        """
      shifts the bounding box
      if p2 given, then is x, y
      """
        if dy is not None:
            vector = Point(vector, dy)

        self.x1 += vector.x
        self.x2 += vector.x
        self.y1 += vector.y
        self.y2 += vector.y
        self.update()

    def overlaps(self, other):
        """
      returns true if bounding-boxes overlap
      """
        for p in self.getPoints():
            if other.inside(p):
                return True

        for p in other.getPoints():
            if self.inside(p):
                return True

        return False

    def trim(self, p1, p2=None):
        """
      returns the point of the first intersection
      """
        # x = i.x + alpha (o.x - i.x)
        # alpha = (x - i.x) / (o.x - i.x)
        if p2 is None:
            i, o = p1.p1, p1.p2  # p1 is a segment
        else:
            i, o = p1, p2

        d = 1e10
        alpha1 = safeDiv((self.x1 - i.x), (o.x - i.x), d)
        alpha2 = safeDiv((self.x2 - i.x), (o.x - i.x), d)
        alpha3 = safeDiv((self.y1 - i.y), (o.y - i.y), d)
        alpha4 = safeDiv((self.y2 - i.y), (o.y - i.y), d)

        alpha = [alpha1, alpha2, alpha3, alpha4, 1.0]
        alpha = [x for x in alpha if 0.0 <= x <= 1.0]
        alpha = min(alpha)

        return Point2(i.x + alpha * (o.x - i.x), i.y + alpha * (o.y - i.y))

    def paint(self, canvas):
        """
        @TODO
        """
        canvas.line(self.x1, self.y1, self.x2, self.y1)
        canvas.line(self.x2, self.y1, self.x2, self.y2)
        canvas.line(self.x2, self.y2, self.x1, self.y2)
        canvas.line(self.x1, self.y2, self.x1, self.y1)

    def getSize(self):
        """
        @TODO
        """
        if self.empty: raise ValueError("empty BBox")

        return self.x2 - self.x1, self.y2 - self.y1

    def getPoints(self):
        """
        @TODO
        """
        if self.empty: return []  # raise ValueError("Empty BBOX")
        return (Point(self.x1, self.y1), Point(self.x2, self.y1),
                Point(self.x2, self.y2), Point(self.x1, self.y2))

    def __str__(self):
        """
        @TODO
        """
        if self.empty: return "empty BBox"
        return "from %f %f to %f, %f " % (self.x1, self.y1, self.x2, self.y2)

    def getCenter(self):
        """
        @TODO
        """
        return Point((self.x2 + self.x1) / 2, (self.y2 + self.y1) / 2)

    def place(self, handle, point):
        """
        @TODO
        """
        self.update()
        if self.empty: return
        self.shift(self.handles[handle] - point)

    def topo(self, toponimic):
        """
        @TODO
        """
        if not self.empty:
            return self.handles[toponimic]
        else:
            return Point2(0, 0)


BBox2 = BBox
