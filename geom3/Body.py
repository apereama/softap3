from Transformations import TrivTrans


class Body:
    """
    @TODO
    """

    def __init__(self):
        """
        """
        self.trans = TrivTrans()

    def setTrans(self, trans):
        """
        sets a transformation
        """
        self.trans = trans

    def transform(self, trans):
        """
        @TODO
        """
        self.trans = trans * self.trans

    def getTrans(self):
        """
        @TODO
        """
        return self.trans

    def getTriangles(self):
        """
        @TODO
        """
        raise NotImplementedError("not implemented")
