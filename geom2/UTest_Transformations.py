#!/usr/bin/python
# -*- coding: utf-8 -*-
# ====================
#  HURA 0.9
# ====================
#
#
# ====================
#  License
# ====================
#
#    Hura v0.9
#    Copyright (C)2013 Angel Perea Martinez ( angelpeream @ yahoo.com )
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
#    MA  02110-1301  USA
#
# (see gpl.txt for the complete licence text).
#

import unittest
from softap3.geom2.Segment import Segment
from softap3.geom2.Point2D import Point2 as Point
from softap3.geom2.Transformations import *

INDEG = 360.0 / 6.28
#===========================
# THTestCase
#===========================
class TransformationTestCase(unittest.TestCase):
  """
  test of the pushkin facilitiess
  """

  #----------------------------------------
  def setUp(self):
    pass

  #-----------------------------------------
  def tearDown(self):
    pass

  #----------------------------------------
  def testJoiningTransformation(self):
    """
    """
    p1, p2 = Point(0,0), Point(1,0)
    q1, q2 = Point(4,4), Point(4,5)    
    t = getJoiningTransformation(p1, p2, q1, q2)
    
    self.assertEqual(t * p1, Point(4,4))
    self.assertEqual(t * p2, Point(4,5))
    
    

#------------------------------
#        getTestSuite
#------------------------------
def getTestSuite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TransformationTestCase))
    return suite



if __name__ == '__main__':
   unittest.TextTestRunner(verbosity=2).run(getTestSuite())