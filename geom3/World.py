from TriangleList import *
from softap3.Canvas2D import *
from softap3.geom2.Point2 import Point2
from Plane import Plane
from Point3 import Point3 as Point
#==================
#    World
#==================
class World:
  """
  """
  def __init__(self):
    self.objects = []
    self.planeXZ = Plane(Point(0,0,0), Point(1,0,0), Point(0,0,1))
    self.planeXY = Plane(Point(0,0,0), Point(1,0,0), Point(0,1,0))
    self.planeYZ = Plane(Point(0,0,0), Point(0,1,0), Point(0,0,1))
    
    
  #-----------------------------
  def addObject(self, obj, trans = None):
    self.objects.append(obj)
    if trans != None:
      obj.setTrans(trans)
    
  #--------------------------
  def addObjects(self, *objs):
    [self.addObject(o) for o in objs]
    
  #-----------------------------
  def project(self, camera, size, filename):
    """
    """
    triangles = []
    for o in self.objects:
      #print o.getTriangles()
      triangles += o.getTrans() * o.getTriangles()
      
    # 1) demaclate
    triangles = TriangleList(camera.transform(triangles))
    triangles = triangles.demaclate() # demaclate the triangles in 3D
    triangles = triangles.flatten()
    
    # camara transformation has clipped scene at +-1000, +-1000
    t = calcTransform(Point2(-1000,-1000), Point2(1000,1000), Point2(0,0), Point2(size[0], size[1]))
    canvas = Canvas2D(size, trans = t)
    triangles.draw(canvas)
    canvas.savePng(filename)
  
  
    
      
