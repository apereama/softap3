import sys
from ColorConsole import *


#--------------------------------
# fatalError
#--------------------------------
def fatalError(title, msg, description="", exit=0):
  """
  prints a fatal error and exits the application
  """
  print(("%s:%s" % (colorize(title, "red"), msg)))
  if description != "":
    print(description)
  if exit != 0:
    sys.exit(exit)


    
#-------------------------------------
# parseArgs
#-------------------------------------
def parseArgs(argv):
  """
  simple parsing of the args in the command line
  """
  args = []; params = {}
  
  for a in argv:
    if a.startswith("--"):
      name, value = a.split("=")
      params[name[2:]] = value
      
    elif a.startswith("-"):
      name, value = a.split("=")
      params[name[2:]] = value

    else:
      args.append(a)

  return args, params
      
