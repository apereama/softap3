# -*- coding: utf-8 -*-

import  http.server, sys, os
from softap.BashParse import * 

#--------------------------------
# limits the size of a string
#-------------------------------
def smartTrim(string, length, sufix = "..."):
  if len(string) > length:
    return string[:length - len(sufix)] + sufix
  else:
    return string


# ====================
#  Module explanation
# ====================
#
# =================
#  License terms
# =================
#
# (C) Angel Perea Martinez, 2008. (e-mail: angelpeream at yahoo.com )
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59
# Temple Place, Suite 330, Boston, MA 02111-1307 USA
#
# (see gpl.txt for the complete licence text

#----- std libs ------------

from softap.common import * 
import http.server, traceback, sys, traceback, os.path, re
from urllib.parse import unquote_plus, quote_plus, unquote, urlencode
from cgi import parse_qsl, FieldStorage

#------ logging -----------
import logging
logger = logging.getLogger("server.handler")

#----http request codes -----

REQ_OK = 200
REQ_NOT_FOUND = 404
REQ_INTERNAL_ERROR = 500
#------------------------------- 
  
#-----------------------------
def urldecode(paramline):
  """
  this is the inverse of the urillib.urlencode function
  """
  return dict(parse_qsl(paramline))

#-----------------------------------
def formatError(title, exception, tb):
  """
  formats an error in html
  @param title the title of the exceptin. will appear in red
  @param exception the description
  @param optional traceback
  """
  lines = []
  lines.append("<font color=red>%s:</font> %s" % (title, exception))
  lines.append("<p>")
  lines.append("<pre>" + tb + "</pre>")
  lines.append("</p>")
  return "\n".join(lines)


  
  
#===================
# HttpRequestHandler
#===================
class HttpRequestHandler(http.server.BaseHTTPRequestHandler):
  """
  This class is instantiated anew in each request. Handles the HTTP request,
  translating it to the browser
  """

  browser = None
  
  #-------------------------------------------
  def do_GET(self, requestMethod = "get"):
    """
    the get request that distinguishes between commands and files
     @REFACTOR before edn20: this function should call getBin of Browser and
      guess _here_ the mime type.
    """
    
    ### 1)  Executes the command
  
    
    try:
      mimeData = self._command(requestMethod)   
      code = REQ_OK

    except BaseException as e: # this returns last exception formatted      
      print(e)
      raise
      logger.error('uncatched exception in request handler "%s"' % e)
      tb = traceback.format_exc()      
      print(indent(box(formatTraceback(tb), 120, "red"), 3))
      code = REQ_INTERNAL_ERROR
        
  
    ## 2) write the header with MIME content

    self.send_response(code)   # transaction accepted

    #mimeLabel = mimeTable.get(mime, "binary/unknown") # defaults to unknown
    #if not mime in mimeTable: logger.warning("browser returned unknown extension 
    # type '%s' " % mime)

    self.send_header('Content-type', mimeData.getLabel())
    self.end_headers()
    
    print(mimeData)
    print(mimeData.getLabel())
    self.wfile.write(mimeData.data)
    self.wfile.close()


  #-------------------------------------------
  def do_POST(self):
    """
    treat POST as GET
    """
    return self.do_GET("post")

    
  #-------------------------------------------
  def log_request(self, *args, **params):
    """
    this function just overrides the BaseHTTPRequestHandler
    logrequest() function that prints annoying stuff.
    """
    pass


  #------------------------------------------------------------------------------
  #                          private functions
  #------------------------------------------------------------------------------
  
  #---------------------------------------
  def _errorPage(self, message):
    """
    """
    lines = []
    lines.append("<pre>ERROR: %s</pre>" % message)
    html = "\n".join(lines)
    return  html


  #--------------------------------------
  def _command(self, requestMethod, debugFieldsGET = None, debugFieldsPOST = None):
    """
    parses one request, returning a tuple mime, data, or an exception    
    """



    ### 1) security    
    
    self._checkSecurity()


    ### 1) keep out robots
    if self.path == "/robots.txt":
      return ".asc", ""
      
      
    ### 2) get extract command and params
    try:
      fields = self._mergeRequestFields(self.path, requestMethod)  
                                                  # merges post and get fields
      
    except Exception as e: # writes the error to the file
      msg = "Exception decoding request parameters '%s'" % str(e)
      logger.error(msg)
      raise e

    ## 3) process command
        
    if "formrequest" in fields: # command was issued from a form . Test this first,
                                # because previous cmd param stay if you press a button
      logger.debug("(form request...)")
      mime, data = self._formCommand(fields)

    elif "cmd" in fields:       # command comes from a link
      logger.debug("(url request...)")
      mimeData = self._urlCommand(fields)

    else: # empty commands are sent to the engine
      logger.debug("(empty request...)")
      fields["cmd"] = ""
      mimeData = self._urlCommand(fields)
      

    ## 4) activate html, if was returned
    if mimeData.getLabel() == "text/html": 
      mimeData.data = self._html2http(mimeData.data) # resolves links and controls
    
    return mimeData



  #-----------------------------------------
  def _html2http(self, data):
    """
    """
    return data
  
    
  #-------------------------------------------
  def http2command(self, formDict):
    """
    returns the command and the params (if any) for an http request
    it takes all the controls with the prefix equal to the button activated
    """

    # 2) looks in form for id of the pressed button, if any.

    data = None
    group = None

    ## 1) tries to guess the pressed button. A field ended with ____button
    NOCOMMAND = "!!NoCommand!!"
    
    for field in formDict:  
      if field.endswith(  "____button"):
        group = field[:-len("____button")]
        command = formDict.get(group + "____command", NOCOMMAND)
        if command == NOCOMMAND:
          logger.warning("wrong command structure in parsed http")
    
    if (group == None):      
      raise ValueError("unknown button pressed, code=%s, data=%s" % ( group, data))
          

    ### 3) gathers all the params that belong to the pressed button, 
    #      i.e, finishing with the same code
    
    params = {}
    
    for field in formDict:
      if field.endswith("____button"): continue # do not add the button
      if field == group + "____command": continue # do not add the path and the function name
      
      if field.endswith(group):
        paramName= field[:-len(group)]  # paramName is appended to the code. Example 0XA5B23param1
        params[paramName] = formDict[field]
                
    logger.debug("command:%s, params:%s" % (command, ",".join(list(params.keys()))))

    # 3) return the command and the params
    
    return command, params


  #----------------------------------------------
  def _mergeRequestFields(self, url, requestMethod):
    """
    gets all the params in the request, both from POST and GET fields
    """
    
    ## 1) process param cmd to extract the command
    
    paramLine = unquote_plus(url).split("/",1)[1]
    path, dontcare, paramline = paramLine.partition("?")

    # 2) merges POST fields and linefields
    
    lineFields = urldecode(paramline)
    fields = self._parsePostFields(requestMethod) # gets post fields, allowing debugging
    fields.update(lineFields)
    
    if path != "" and not "cmd" in fields:
      fields["cmd"] = 'get "/' + path + '"' # adds back the taken forward slash
    return fields


  #-----------------------------------------------
  def _checkSecurity(self):
    """
    checks that the request comes from authorized client
    """
    return None # should raise an exception if not authorized
  
  
  #-----------------------------------------------
  def _urlCommand(self, params):
    """
    processes commands issued in the URL line
    """
    command = params.pop("cmd") # get value and remove from dictionary
    command, args, params2 = cmdlineToTuple(command)
    params.update(params2)
    print("-", HttpRequestHandler.browser)
    r = HttpRequestHandler.browser.call(command, *args, **params) # params are named 
                                            # parameters (post and line fields)

    return self.getMime(r)


  #------------------------------------------------------
  def _formCommand(self, formParams):
    """
    processes commands coming in a form in the POST fields
    """
    #html = "form command"
    
    
    command, params = self.http2command(formParams)

    logger.debug("form call request received ---")
    logger.debug("   * command:%s" % command)
    logger.debug("   * params:%s" % (",".join(["%s=%s" % (a,smartTrim(b, 30)) for a,b in list(params.items())])))

    r = HttpRequestHandler.browser.command(command , *[], **params )

    return getMime(r)


  #------------------------------
  def getMime(self, value):
    """
    guard function, in case the functions do not return any mimeDAta
    """
    if isinstance(value, MimeData):
      return value
    
    elif isinstance(value, str):
      return MimeData("str.html", str(value))
    else:
      return MimeData("str.html", str(value))
    

  #------------------------------------------------------
  
  def _sendFile(self, branch):
    """
    a file requested
    """

    #---1) resolve filename
    fileName = HttpRequestHandler.browser._engineCommand("getOsPath", str(branch))

    #--- 2) guess mime type

    ext = os.path.splitext(fileName)[1]
    ext = ext.strip().lower()
    mime = mimeTypes.get(ext.strip().lower(), mimeTypes[".exe"])
    
    #--- 3) send and headers data

    try:
      data = file(fileName, "rb").read()
      self.send_response(REQ_OK)
      self.send_header('Content-type', mime ); self.end_headers()
      self.wfile.write(data); self.wfile.close()

    except IOError as e:  # the file could not be opened
      logger.warning("file '%s' does not exist" % fileName )
      self.send_response(REQ_NOT_FOUND)


  #----------------------------------------------------
  def _parsePostFields(self, requestMethod, debugFields = None):
    """
    parses fields included in the additional data sent in the http request
    """
    r = {}
    fs = FieldStorage(self.rfile, self.headers, environ = {'REQUEST_METHOD': requestMethod})    

    if fs.length != -1:
      for k in list(fs.keys()):
        r[k] = fs.getfirst(k)
        #print fs[k], fs[k].file, type(fs[k])
        
        if fs[k].filename :
          r["content_" + k] = fs[k].file.read() # adds a field

    #print r
    
    return r

  #------------------------------------
  def _infoException(self, e):
    """
    """
    lines = []
    lines.append("<PRE> <b> uncatched exception in Request Server </b>\n")
    lines.append("\n" + str(e) + "\n")
    tb = "\n".join(traceback.format_tb(sys.exc_info()[2])) # third value is the traceback
    lines.append(tb)
    lines.append("</PRE>")
    html = "\n".join(lines)
    return html
