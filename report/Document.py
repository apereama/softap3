
from .TextWriter import TextWriter
 
from softap3.fsTools import writeBinFile



#==================
#   Document
#==================
class Document:
    """
    An universal document. implementations (html, latex,...) inherit from these
    """
  
    def __init__(self, params = None):
        """
        """
        self.indentations = []
        self.resources = {}
        self.sourceCode = TextWriter()
        if params != None:
            self.params = params
        else:
            self.params = {}
        self.parts = {} # dictionary to store sections
        self.lastLine = ""
        self.text = []
        self.partList = []
        
    def getParam(self, name, default = None):
        """
        returns one parameter of the document
        """
        if default != None:
            return self.params.get(name, default)
        else:
            return self.params.get(name)
       
    def setParam(self, name, value):
        """
        sets the value of a document
        """
        self.params[name] = value
        
    ##------------------------------
    # parts
    #------------------------------
    def addPart(self, name, part):
        """
        adds a part to the document
        @param name name of the part
        """
        self.partList.append(name)
        self.parts[name] = part
        
    #--------------------------
    def getPart(self, name):
        """
        returns a part under the given name
        @param name name of the part
        """
        return self.parts[name]
        
    #-----------------------
    #    writting
    #-------------------------
    def addSection(self, title, level):
        """
        adds a section to th document
        @title the title for the section
        @level an integer, from 1 to 5. The meanings are:
            * 1: part
            * 2: chapter
            * 3: section
            * 4: subsection
            * 5: subsubsection
        """
        raise NotImplementedError("not implemented")
    

    #---------------------------
    def writeLine(self, line = "", raw = False):
        """
        writes a line of text
        @param raw. Don't process text. Pass control chars
        """
        if not raw: 
            line = self._protect(line)
            
        self.writer.writeLine(line)                

    #----------------------------
    def writeLn(self, text = "", raw = False):
        """
        Note: writeln should be in the final document, not in the source. The
        source can contain many lines for a line in the document
        """
        self.writeLine(text, raw)
  
    #-----------------------------
    def write(self, text, raw = False):
        """
        writes text into the document
        """
        if not raw: text = self._protect(text)
        self.writer.write(text)

    #--------------------------------
    def writeRaw(self, text):
        """
        writes text without protecting it, so any latex commands inside
        the text will be interpreted
        @param text     
        """
        self.write(text, raw = True)
                                
        
    #---------------------
    def indent(self, i = "  "):
        """        
        this is indent in the final document, NOT in the source
        """
        raise NotImplementedError("abstract method")

    #---------------------
    def dedent(self):
        """
        this in indent in the final document, NOT in the source
        """
        raise NotImplementedError("abstract method")            
    
            
    #----------------------
    def addFigure(self, caption = None, content = None):
        """
        Allows adding figures
        
        @param caption caption for the figure
        @param content what goes inside the figure borders
        """
        raise NotImplementedError("not implemented")
        

        
    #----------------------------------
    def addDoubleFigure(self, caption1, content1, caption2, content2):
        """
        allows adding double figures to the document        
        """
        raise NotImplementedError("not implemented")
    
    #----------------------------------
    def addFlowFigure(self, caption, content):
        """
        """
        raise NotImplementedError("not implemented")

            
    #----------------------------------
    def addTable(self, cells):
        """
        """
        raise NotImplementedError("not implemented")

    #----------------------------------
    def writeItem(self, text, name = "", raw=False):
        """
        """
        raise NotImplementedError("not implemented")


    #------------------------------------
    def writeError(self, text):
        """
        writes text in the form of an error
        @param text explanation of the error 
        
        """
        self.writeRaw(r"\textcolor{Red}{%s}" % self._protect(text))
   
    #-----------------------
    def getWriter(self):
        """
        returns the textWritter of this document
        """
        return self.sourceCode
            
    #---------------------------------
    def writeCommand(self, command, text = ""):
        """
        writes a command in the text language. Its used as a commodity function
        by the descendants of this class
        @param command name of the command
        @param text text inside {} 
        """
        raise NotImplementedError("Abstract method of Document")
        
        
    #---------------------------
    # files
    #---------------------------

    def getSource(self):
        """
        @return the code
        """
        raise NotImplementedError("abstract method of Document")
    
    #----------------------------
    def save(self, name):
        """
        saves the report under the given name
        @param name
        """
        writeBinFile(name, self._getFile().data)
 
    #------------------------------    
    def getFile(self, name = "document", tempDir = None):
        """
        returns the filename where this document will be saved
        """
        return self._getFile(name, tempDir)
    
    
  
    #---------------------
    def addResource(self, name, mimeData):
        """
        adds a resource (a MIME file, usually an image)
        @param mimeData a Mime object
        """
        self.resources[name] = mimeData
            
    #----------------------
    def saveResources(self, directory):
        """
        Saves all the resources in the directory
        @param directory directory where the data will be stored
        """
        for name, mime in list(self.resources.items()):            
            writeFile(directory + "/" + name, mime.data)
        
        
    #----------------------
    # private functions
    #--------------------
    def _getFile(self, name = "document", tempDir = None):
        pass
    
    #---------------------------------
    def _protect(self, text):
        """
        this is a pretty slow function, most likely candidate to be optimized!!!!
        
        @param string string being formatted to latex
        @returns the string so i doesn't flip error messages
        """
        return NotImplementedError("not implemented")
  
