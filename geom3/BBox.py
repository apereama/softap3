"""
@TODO
"""

from .Point3 import Point3


class BBox3:
    """
    a bounding box for objects
    """

    def __init__(self, corner1=None, corner2=None):
        """
        defaults None for corner1 and corner2 allows empty bbox definitions, that makes easier
         for calculations
        """
        if corner1 is None and corner2 is None:
            self.empty = True
        else:
            self.empty = False
            self.x1, self.x2 = min(corner1.x, corner2.x), max(corner1.x, corner2.x)
            self.y1, self.y2 = min(corner1.y, corner2.y), max(corner1.y, corner2.y)
            self.z1, self.z2 = min(corner1.z, corner2.z), max(corner1.z, corner2.z)

    def engulf(self, b):
        """@TODO"""

        if not isinstance(b, BBox3):
            b = b.getBBox()

        if self.empty:
            self.x1, self.x2 = b.x1, b.x2
            self.y1, self.y2 = b.y1, b.y2
            self.z1, self.z2 = b.z1, b.z2
            return

        if isinstance(b, list):
            for i in b:
                self.engulf(i)

        elif isinstance(b, BBox3):

            if b.empty:
                return

            else:
                self.x1, self.x2 = min(self.x1, self.x2, b.x1, b.x2), max(self.x1, self.x2, b.x1, b.x2)
                self.y1, self.y2 = min(self.y1, b.y1, self.y2, b.y2), max(self.y2, b.y2, self.y1, b.y1)
                self.z1, self.z2 = min(self.z1, self.z2, b.z1, b.z2), max(self.z1, self.z2, b.z1, b.z2)

        elif isinstance(b, Point3):
            self.x1, self.x2 = min(self.x1, self.x2, b.x), max(self.x1, self.x2, b.x)
            self.y1, self.y2 = min(self.y1, self.y2, b.y), max(self.y1, self.y2, b.y)
            self.z1, self.z2 = min(self.z1, self.z2, b.z), max(self.z1, self.z2, b.z)

        else:

            raise ValueError("unknown object type", b)

    def addPoints(self, points):
        """
        @TODO
        """
        for p in points:
            self.addPoint(p)

    def addPoint(self, p):
        """
        @TODO
        """
        if self.empty:
            self.x1, self.x2 = p.x, p.x
            self.y1, self.y2 = p.y, p.y
            self.z1, self.z2 = p.z, p.z

        self.x1, self.x2 = min(self.x1, self.x2, p.x), max(self.x1, self.x2, p.x)
        self.y1, self.y2 = min(self.y1, self.y2, p.y), max(self.y1, self.y2, p.y)
        self.z1, self.z2 = min(self.z1, self.z2, p.z), max(self.z1, self.z2, p.z)

    def contains(self, p):
        """@TODO"""
        if self.empty:
            return False
        else:
            return self.x1 <= p.x <= self.x2 and self.y1 <= p.y <= self.y2 and self.z1 <= p.z <= self.z2
