#------------------------
# toDegrees
#-----------------------
def toDegrees(theta):
    """
    """
    return theta * 360 / 6.2830


#---------------------------
# quantize
#---------------------------
def quantizeDown(x, step):
    """
    return the minimal k such that (step * k) > x
    produce stairs out of ranges!!!
    """
    return (int(x) / step) * step

#---------------------------
# quantizeUp
#---------------------------
def quantizeUp(x,step):
    """
    returns the maximun k such that (step * k) < x
    produce stairs out of ranges!!!
    """
  
    return ((int(x) / step) + 1) * (step )



#-----------------------------
# linInterpolate
#-----------------------------
def linInterpolate(pairs, x):
  """
  interpolates linearly between values
  """

  # 1) value falls left from the given points

  if x < pairs[0][0]: return pairs[0][1]

  # 2) value is among the given points

  for i in range(1, len(pairs)):
    xLeft, yLeft = pairs[i-1]
    xRight, yRight = pairs[i]    
    if (x>xLeft) and (x < xRight):
      alpha = (x - xLeft) / float(xRight - xLeft)  # alfa goes from 0 to 1
      return yLeft + alpha * (yRight - yLeft)

  # 3) value is far to the right of the given points

  return pairs[-1][1]



#--------------------------------
# lagrangeInterpolate
#--------------------------------
def lagrangeInterpolate (x,list):
    """
    devuelve un n�mero resultante de interpolar mediante
    el polinomio interpolador de Lagrange    
        
               mult(j=1, j!=i)(x-xj)
     sum(i)( ---------------------- * yi)
              mult(j=1, j!=i)(xi-xj)
    '"""
    
    result = 0.0
    for i in range(0,len(list)):
      xi = list[i][0]
      yi = list[i][1]
      term = 1.0

      rang2 = range(0,len(list))
      rang2.remove(i)    # elimina la ocurrencia de i = j
      for j in rang2:
        xj = list[j][0]
        term =  term * (x - xj) / (xi - xj)

      result = result + term * yi

    return result

#----------------------------------
#   gaussian
#----------------------------------
def gaussian(center, halfWidth, point):
  """
  returns the value of the gaussian (center, sdev),
  where attains value 0.5 at center+-halfWidth, at the given point
  """

  sigma2 = (halfWidth * halfWidth) / math.ln(0.5)
  return exp( ((point-center)*(point-center)) / (sigma * sigma))


#------------------------------
#   weightedChoice
#------------------------------
def weightedChoice(data):
  """
  selects one of the data, where data contains probability and items

  If data is a dictionary then should be in the form id:probability
  if data is a list then is a list of pairs (id, probability)
  
  @param data either a list of pairs (item, weight) or a dictionary (item:weight)

  """
  if isinstance(data, dict):
    items = data.keys()
    weights = data.values()
  elif isinstance(data, list):
    items = [x[0] for x in data]
    weights =[x[1] for x in data]

  else:
    raise ValueError("data in wrong format")

  s = sum([x for x in weights])
  n = random.randint(0, int(s))

  for i in range(0, len(items)):
    n -= weights[i]
    if n <= 0: return items[i]

  raise ValueError("unreachable statement reached")



#------------------
# percent
#------------------
def percent(value, reference):
    """
    """
    return "%2.1f%%" % ((value * 100.0) / reference)
#======================
# un average
#======================
class BufferAverage:
    """
    """
    def __init__(self, size, initValue = 0):
        """
        """
        self.size = size
        self.value = initValue
      
    #------------------------
    def __sum__(self, value):
        """
        """
        alpha = 1.0 / self.size
        self.value = value * alpha + self.value * (1.0 - alpha)
      
    #----------------------
    def getValue(self):
        return self.value
    
   
    
#=====================
#  RunningAverage
#=====================
class RunningAverage:
    """
    """
    def __init__(self, size, initValue = 0):
        self.size = size
        self.values = [initValue]
    
    #-----------------------------------
    def add(self, value):
        """
        """
        self.values.append(value)
        if len(self.values) > self.size:
            self.values.pop(0)
    
    #---------------------------------
    def getValue(self):
        """
        """
        return sum(self.values) / float(len(self.values))
    
