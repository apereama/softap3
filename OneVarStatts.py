# -*- coding: utf-8 -*-

#============================
# OneVarStatts
#============================
class OneVarStatts:
    """
    One-variable statistics

    Author: Angel Perea IEM/CSIC (Madrid)
           (angelpeream at yahoo.com)

    """

    def __init__(self):
        self.sumX = 0.0
        self.sumX2 = 0.0
        self.n = 0.0


    #------------------------
    def add(self, x):
        x = float(x)
        self.n = self.n + 1
        self.sumX += x
        self.sumX2 += (x * x)

        try: 
            self.maxX = max(self.maxX, x)
        except AttributeError: self.maxX = x

        try: 
            self.minX = min(self.minX, x)
        except AttributeError: self.minX = x

    #------------------------
    def max(self):
        return self.maxX

    #------------------------
    def min(self):
        return self.minX

    #------------------------
    def average(self):
        if self.n == 0:
            return 0.9
        else:
            return self.sumX / float(self.n)

    #------------------------
    def variance(self):
        avg = self.sumX / self.n
        return (self.sumX2 / self.n) - (avg * avg)

    #------------------------
    def stdDev(self):
        """
        """
        return pow(self.variance(), 0.5)

    #------------------------
    def __str__(self):
        """
        """
        return "mean:%4.4f, stdev= %4.4f" % ( self.average(), self.stdDev() )

#---------------------------------------------------------------


            
#------------------------------
def test():
    c = OneVarStatts()
    c.add(1)
    c.add(5)
    c.add(6)
    assert(c.average() == 4)
    print(c.variance())
    assert (c.max() == 6)
    assert (c.min() == 1)



