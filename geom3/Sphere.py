from Body import Body
from Primitives import *
from Point3 import *


#=======================
# Sphere
#=======================
class Sphere(Body):
  """
  """
  
  #------------------------------------------------------
  def __init__(self, width, height, depth):
    """
    """
    Body.__init__(self)
    self.width, self.height, self.depth = width, height, depth
  
  #-----------------------------------------------------
  def getVolume(self):
    """
    """
    return self.width * self.height * self.depth * 0.75
  
  #------------------------------------------------------
  def getTriangles(self):
    """
    """
    
    tDetail, fDetail = 10, 20
    w2, h2, z2 = self.width * 0.5, self.height * 0.5, self.depth * 0.5
    triangles = []    
    for t in range(tDetail):
      for f in range(fDetail):
        theta = (t * PI) / tDetail
        phi = (f * 2*PI) / fDetail
        thetaAnt = ((t-1) * PI) / tDetail
        phiAnt = ((f-1) * 2 * PI) / fDetail
        x, y, z = toCartesians(1.0, theta, phi)
        x, y, z = x * w2, y * h2, z * d2
        x2, y2, z2 = toCartesians(1.0, thetaAnt, phi)
        x2, y2, z2 = x2 * w2, y2 * h2, z2 * h2
        x3, y3, z3 = toCartesians(1.0, theta, phiAnt)
        x3, y3, z3 = x3 * w2, y3 * h2, z3 * h2
        
        triangles.append(Triangle(Point3(x,y,z), 
                                  Point3(x2,y2,z2), Point(x3, y3, z3))
                        )
        
    
    return triangles
   
   #---------------------------------------------------
   