"""
@TODO
"""

# -*- coding: utf-8 -*-
#
#
# ====================
#    License
# ====================
#
# Copyright (C)2013 Angel Perea Martinez ( angelpeream @ yahoo.com )
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
# MA    02110-1301    USA
#
# (see gpl.txt for the complete licence text).
#

import cairo
# from softap3.geom2.Transformations import *
from softap3.geom2.Point2D import Point2D as Point2
from softap3.geom2.Matrix3 import *
from softap3.listTools import *

degrees = float(6.28) / 360


class TCanvas:
    """
    an special canvas 
    Everything is done in logical coordinates.
    There is a function setTrans(point1, pixel1, point2, pixel2)


    Shapes should be put in with a transformation
    The method autoCenter(shape) returns the transformation needed to view the frame in the area

    * A canvas allows shapes to be put in.
    * There are no primitives. Primitives are functions in the canvas
    * A canvas do NOT have a particular area. User can set the visible area
    
    """

    # ------------------------------------------
    def __init__(self, size, surface=None, context=None):
        """
        @param size Bounding box with the area
        """

        self.transStrack = None
        self.size = size

        # turtle-related fieldso
        self.pos = (0, 0)  #
        self.angle = 0 * degrees  #
        self.stackPos = []  # a stack with the positions
        self.lastNode = None  # absolute position of last node painted
        self.scale = 1.0

        # prepare cairo things
        if surface is None: surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, size[0], size[1])
        self.surface = surface

        if context is None: context = cairo.Context(surface)
        self.canvas = context

        self.trans = DummyTrans()
        self.transStack = [self.trans]

        # background    
        self.canvas.set_operator(cairo.OPERATOR_SOURCE)
        self.canvas.set_source_rgb(1, 1, 1)  # background
        self.canvas.paint()

        # default color
        self.setColor(0, 0, 0)

        # test lines
        test = False
        if test:
            self.canvas.move_to(0, 0)
            self.canvas.line_to(1000, 1000)
            self.setColor(0, 0, 0)
            self.canvas.stroke()

        if test:
            alpha = 0.01
            self.rectangle((size[0] * alpha, size[1] * alpha), (size[0] * (1 - alpha), size[1] * (1 - alpha)))

    def checkPoint(self, p):
        """
        converts tuples to points
        """
        if isinstance(p, tuple):
            return Point2(p[0], p[1])
        else:
            return p

    def autoCenter(self, shape, _frame=(0, 0)):
        """
        auto-centers the shape in the canvas. Calculates the transformation needed to fit all the
        objects in the frame, with only a translation and an scaling . Note that the boundingBox
        or the frame are in general non-similar rectangles
        
        @param shape Bounding box in logical _space_ coordinates
        @frame empty area around the image, in _device_ coordinates
        @returns the transformation that maps the shape into the area
        @param _frame:
        @param shape:
        """

        # area.expandRel(frame[0], frame[1]) # to make a border around the objects

        # 2) get the transformation to fill the canvas
        aw, ah = float(shape.size()[0]), float(shape.size()[1])

        scaleX = self.size[0] / max(aw, 2)
        scaleY = self.size[1] / max(ah, 2)

        a = min([scaleX, scaleY])
        scale = Scale(a, -a)  # because in cairo, 0,0 is upper left

        # dx = ((self.size[0] / a) - lw) / 2.0
        # dy = ((self.size[1] / a) - lh) / 2.0
        offset = Offset(- area.x1,
                        - area.y2)  # because in cairo 0,0 is upper left. this transformation along with scaling (a, -a)

        # xm, ym = (area.x2 + area.x1) /2.0, (area.y1 + area.y2) / 2.0
        # offset = Offset( (xm - (self.size[0] / (2.0 * a))),
        #                                 (ym - (self.size[1] / (2.0 * a)))
        #                             )        
        t = scale * offset
        self.scale = a
        self.trans = t

    def transform(self, point):
        """
        transform from logical to physical coordinates
        """
        return self.trans * point

    def inverseTransform(self, point):
        """
        inverse transform a point (physical to logical)
        """
        return self.trans.inverse() * point

    def line(self, p1, p2, p3=None, p4=None, stroke=None, width=1):
        """
        draw a line, given 2 points or 4 coordinates
        """
        # self.canvas.set_line_widtdh(0.5)
        # self.canvas.set_source_rgb (0, 0, 0)
        self.canvas.set_operator(cairo.OPERATOR_SOURCE)
        # self.canvasctx.paint()

        if p3 is not None and p4 is not None:  # user passed coordinates
            p1, p2 = Point2(p1, p2), Point2(p3, p4)

        q1, q2 = self.transform(p1), self.transform(p2)
        # print ("%i,%i to %i,%i" % (q1.x, q1.y, q2.x, q2.y))
        self.canvas.move_to(q1.x, q1.y)
        self.canvas.line_to(q2.x, q2.y)
        self.canvas.stroke()

    def pline(self, *points):
        """@TODO"""
        for p1, p2 in getPairs(points):
            self.line(p1, p2)

    def calcTransform(self, p1, p2, q1, q2):
        """
        @param p1 logical point 1
        @param q1 canvas    point 2
        @param p2 logical point 1
        @param q2 canvas    point 2
        
        | sx 0    dx    | p1x | = q1x ;
        | 0 sy    dy    | p1y | = q1y ;
        | 0    0     1    |    1    | = 1     ;
        
        sx    = (q2x - q1x) / (p2x - p1x)
        sy    = (q2y - q1y) / (p2y - p1y)
        """

        sx = (q2.x - q1.x) / float(p2.x - p1.x)
        sy = (q2.y - q1.y) / float(p2.y - p1.y)

        dx = q1.x - sx * p1.x
        dy = q1.y - sy * p1.y
        return Matrix3(sx, 0, dx,
                       0, sy, dy,
                       0, 0, 1)

    def pushTrans(self, trans):
        """
        sets a transformation
        """
        t = self.transStack[-1] * trans
        self.transStack.append(t)
        self.trans = t

    def setTrans(self, trans):
        """@TODO"""
        self.transStrack = []
        self.pushTrans(trans)

    def popTrans(self):
        """
        @TODO
        """
        self.transStack.pop()
        self.trans = self.transStack[-1]

    def tAdvance(self, length):
        """
        advances the turtle
        """
        self.line(self.pos, self.pos + Vector(angle=self.angle, length=length))

    def setColor(self, r, g=None, b=None):
        """
        @TODO
        """
        if isinstance(r, tuple):
            r, g, b = r

        if g is None and b is None:
            r, g, b = r.getRgb()

        self.canvas.set_source_rgb(r, g, b)

    def tTurn(self, angle):
        """
        turns the turtle
        """
        self.angle -= angle  # turtle turns to the right

    def measureText(self, text, font=None, size=None):
        """
        returns the size of text
        """
        if font is not None: self.canvas.select_font_face(font)
        if size is not None: self.canvas.set_font_size(self.scale * size)

        return [s / self.scale for s in self.canvas.text_extents(text)[2:4]]

    def setFont(self, fontName, size):
        """
        @TODO
        """
        size = self.scale * size  # size is given in user coordinates, so must be transformed
        self.canvas.select_font_face(fontName)
        self.canvas.set_font_size(size)

    def putText(self, p, text):
        """
        @TODO
        """
        q = self.transform(p)
        # self.canvas.select_font_face("Sans")
        # self.canvas.set_font_size(40)
        extents = self.canvas.text_extents(text)
        self.canvas.move_to(q.x - extents[2] / 2.0, q.y + extents[3] / 2.0)
        self.canvas.show_text(text)

    def marker(self, p, size):
        """@TODO"""
        p = p - Point2(size / 2.0, size / 2.0)
        self.rectangle(p, size=(size, size))

    def rectangle(self, p1, p2=None, size=None):
        """
        @TODO
        """
        if p2 is None and size is None: raise ValueError("either second point or size must be given")
        if p2 is None:
            p2 = p1 + size

        # line performs the transformation
        p1, p2 = self.checkPoint(p1), self.checkPoint(p2)

        self.line(p1.x, p1.y, p2.x, p1.y)
        self.line(p2.x, p1.y, p2.x, p2.y)
        self.line(p2.x, p2.y, p1.x, p2.y)
        self.line(p1.x, p2.y, p1.x, p1.y)

    def normalize(self, point):
        """@TODO"""
        if isinstance(point, tuple) and length(point) == 2:
            point = Point2(point[0], point[1])
        return point

    def circle(self, point, radius):
        """
        @TODO
        """
        point = self.transform(self.normalize(point))
        self.canvas.arc(point.x, point.y, radius, 0, 6.28)

    def savePng(self, filename):
        """
        returns the objects in png, using cairo
        """
        self.canvas.stroke()
        if filename.lower().endswith(".png"):  # strip extension
            filename = filename[:-4]

        self.surface.write_to_png('%s.png' % filename)

    def pointToPixel(self, point):
        """
        @TODO
        """
        return self.transform(point)

    def pixelToPoint2(self, x, y):
        """
        @TODO
        @param x:
        @param y:
        @return:
        """
        return self.inverseTransform(Point2(x, y))


TurtleCanvas = TCanvas
