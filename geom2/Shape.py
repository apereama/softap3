"""
@TODO
"""

from softap3.geom2.TurtleCanvas import *
from Point2D import Point2D as Point


class Shape:
    """
    @TODO
    """

    def __init__(self, layer=0):
        """
        """
        self.pos = None
        children = []
        self.trans = OffsetTrans(0.0, 0.0)
        self.children = children
        self.layer = layer  # layer of the shape

    def _measureText(self, text, size, font):  # fontSize):
        """
        measures text
        returns two pairs: the size of the whole text
         and the size of the first character, that will be the one centered
        """
        textSize = Shape.auxCanvas.measureText(text, font=font, size=size)
        return textSize

    def getPos(self, space):
        """
        @TODO
        """
        return

    def getPosition(self):
        """
        @TODO
        """
        return self.pos

    def draw(self, canvas):
        """
        @TODO
        """
        for c in self.children:
            canvas.pushTrans(c.trans)  # sets the transformation for the child
            c.draw(canvas)
            canvas.popTrans()  # undoes the transformation

    def getBBox(self):
        """
        @TODO
        """
        "calc bbox gral"
        b = BBox2()
        for c in self.children:
            b.engulf(c)
        return b

    def shift(self, a, b=None):
        """
        @TODO
        """
        delta = a if b == None else Point(a, b)
        self.pos = self.pos + delta

    def add(self, child):
        """
        @TODO
        """
        child.parent = self
        self.children.append(child)

    def shiftTo(self, a, b=None):
        """@TODO
         """
        delta = a if b is None else Point(a, b)
        self.pos = delta
