from rongo.Common import guard, MimeData, readFile, writeFile, bashCall

from .Document import Document
import os
from rongo.Common import oldTempnam


TEMPLATE = r"""

%%packages%%
\documentclass[hidelinks,a4paper,10pt]{report}
%\documentclass[a4paper,12pt]{scrartcl}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{graphicx}  % to include figures
\usepackage{listings}
\usepackage{ulem}    % tachar text
\normalem
\usepackage[utf8x]{inputenc}
\usepackage{pdfpages}
\usepackage{multicol}
\usepackage{geometry}
\usepackage{amssymb}
\usepackage{subcaption}
\usepackage{mdframed}
\usepackage[version=4]{mhchem}
\usepackage{float}
\usepackage{hyperref}
\usepackage{wrapfig}
\usepackage{sectsty}
\geometry{a4paper, portrait, margin=20mm}
\pagestyle{empty}

\begin{document}
%%text%%
\end{document}
"""[1:]



#====================
# PdfDocument
#====================
class RtfDocument(Document):
    """
    a pdf document, that allows for putting
    """
    
    #-------------------
    def __init__(self):
        """
        """
        Document.__init__(self)
        self.language = "pdf"
       
    
    #-------------------
    def addSection(self, title, level):
        """
        """
      
        tags = {1: ("\part{}", ""),
              2: ("\chapter{}", ""),
              3: ("\section{}", ""),
              4: ("\subsection{}", ""),
              5: ("\subsubsection{}", ""),
              }
      
        tag = tags[guard(level, 1, 4)]
        self.writeLine(tag[0] + title + tag[1])
  
  
    #--------------------
    def getLatex(self):
        """
        """
        text = "\n".join(self.text)
        s = TEMPLATE.replace("%%text%%", text)
        s = s.replace("%%packages%%", "")
        return s
    
    #---------------------
    def getSource(self):
        """
        """
        return self.getLatex()
    
    #-----------------------------------
    def error(self, string):
        self.write("ERROR: %s " % string)
  
  
    #----------------------
    def addFigure(self, caption = None, content = None):
        """
        """
                  
        self.write(r"""
  \begin{figure}[H]
    \centering
  """)  
        
        try:
            if not isinstance(content, str):
                content.display_text_latex(2, self)
            else:
                self.write(content)
  
            if not isinstance(caption, str):
                caption = caption.display_text_latex(2, self)
          
            self.write(r"\caption{%s}" % caption)
        finally:
            self.write(r"""
  \end{figure}
  """)
     
      
      
    #--------------------
    def getFile(self, name = "document", tempDir = None):
        """
        """
        if tempDir == None:
            directory = oldTempnam(); 
        else:
            directory = tempDir
        
        if not os.path.isdir(directory): os.makedirs(directory)
        
        self.saveResources(directory)
        latexFilename = "file.latex"
        pdfFilename = "file.pdf"
        
        # 1) execute latex command
        current = os.getcwd();  os.chdir(directory)
        writeFile(latexFilename, self.getLatex())
        cmd = "pdflatex  -interaction=nonstopmode -halt-on-error -output-directory='%s' '%s'" % (directory, latexFilename)
        code, std, err  = bashCall(cmd)
        
        if code != 0:
            print(std)
            print("--------")
            print(err)
            raise ValueError("there was an error producing the ouput")
      
        # 2) return the generated file    
        
        data = readFile(pdfFilename)
        data = MimeData("%s.pdf" % name, data)
        os.chdir(current) # return to the previous directory
        return data

