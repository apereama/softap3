class PcPoint:
    """
    a point
    """
    def __init__(self, point):
        self.point = point # a reference to a point3d
        self.segments = []  # list with adjacent segments


MIN_DISTANCE = 1.0E-5

#=========================
# PointContainer
#========================= 
class PointContainer:
    """
    """
    #-------------------------------------------
    def __init__(self):
        self.points = []
    
    #---------------------------------------
    def addPoint(self, p):
        """
        """
        for q in self.points:
            if p.distance(q)<MIN_DISTANCE:
                return q
            
        # if it comes to here, there is no near point, so create one
        x = PcPoint(p)
        self.points.append(x)
        return x
        
    