#!/usr/bin/env python3
# -*- coding: utf-8 -*-
########################################
#
#
#
# Misc functions, used so often in projects
# that is worth to group them in a module
#
#
#
#
#########################################

import imp
from .devTools import getTraceback 
from .stringTools import colorize
mins, minutes, hours, days = 60, 60, 3600, 86400
from .safeCheck import guard
from .shellTools import bashCall
from .fsTools import readFile, writeFile, writeBinFile, readBinFile 
from .stringTools import indent

degrees = 6.28 / 360
import logging
logger = logging.getLogger("hura")
 
def display(msg, color = None):
    if color == None:
        print(msg)
    else:
        print(colorize(msg, color))
#---------------------------------------
def writeRows(filename, l):
    """
    """
    g = open(filename, "w")
    for a,b in l: 
        g.write("%f %f \n" % (a,b))
    g.close()
  
#----------------------------------------
def readRows(filename):
    """
    """
    f = open(filename, "r")
    d = []
    for l in f.read().split("\n"):
        if l.strip() == "": continue
        v = l.strip().split(" ")
        d.append(v)
    f.close()
    return d



############# IMPORTS ###############

#-------------------------------
# importModule
#------------------------------
def importModule(name, directory):
    """
    imports a module in run-time
    """
    filename = directory + "/" + name + ".py"
    #if path != "": fileName = path + "/" + filename
    f = open(filename, "r")
    r = imp.load_source(name, filename, f)
    f.close()
    return r


#--------------------------------------
#  importClass
#--------------------------------------------------
def importClass(name, moduleName, directory):
    """
    """
    module = importModule(moduleName, directory)
    cl = getattr(module, name)
    return cl

def debug(message):
    logger.debug(message)
 


 
 
 
