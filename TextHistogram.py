﻿import math

#------------------------------
#  guard
#-----------------------------
def guard(value, minValue, maxValue):
  """
  """
  return min(maxValue, max(value, minValue))


#========================
# Histogram
#========================
class Histogram:
  """
  """

  #---------------------------------------------
  def __init__(self, minV, maxV, resolution):
    self.min = minV
    self.max = maxV
    self.range = maxV - minV
    self.resolution = resolution
    self.nBins = self.range / resolution
    self.bins = []
    for i in range(1,nBuckets): self.bins.append(0)
    self.greater = 0
    self.lesser = 0

  #--------------------------------
  def add(self, x):
    """
    """
    if (x > self.max): self.greater = self.greater + 1
    elif (x < self.min): self.lesser = self.lesser + 1
    else:
      i = self.nBins * ( x - self.min ) / (range)
      self.bins[i] = self.bins[i] + 1
      n = n + 1


      
#=======================
# TextHistogram
#=======================
class TextHistogram:
  """
  """

  #---------------------------------
  def __init__(self, nBins, minValue, maxValue, values = None, title = None, logMode = False):
    """
    """
    if values == None: values = []
    self.values = values
    self.range = None
    self.title = title
    self.log = logMode
    self.nBins = nBins
    self.min = float(minValue)
    self.max = float(maxValue)
  #---------------------------------
  def addValues(self, values):
    """
    """
    self.values.extend(values)


  #---------------------------------
  def getStacks(self, range = None):
    """
    returns bin heights
    """
    if self.values == []: return []

    stacks = [0] * self.nBins
    minV, maxV = self.min, self.max
    span = max(1,float(maxV - minV))

    for v in self.values:
      v = guard(v, minV, maxV)
      b = int((v - minV) * self.nBins / span)
      b = guard(b, 0, self.nBins -1 )
      stacks[b] += 1

    return stacks

  #---------------------------------
  def chart(self, width, height):
    """
    returns a graphical representation of the bins, without edges nor text
    """
    lines = []
    stacks = self.getStacks(width)
    
    if self.log: stacks = [100 * math.log(max(1,b)) for b in stacks]
    if stacks == []:  return "  \n  " # empty
    
    maxValue = max(max(stacks), 1) # avoid 0
    
    for l in range(height):
      line = ""
      vScale =  float(height) / maxValue
      for j in range(width):
        s = "#" if stacks[j] * vScale > (height - l) else " "
        line += s
      lines.append(line)
    return "\n".join(lines)


  #---------------------------------
  def toString(self, height):
    """
    width is determined by numer of bins
    """
    s = self.chart(self.nBins, height).split("\n")
    s = [" |" + s for s in s]
    s.append("-+" + "-" * len(s[0]))
    if self.title != None: s.append(self.title.center(len(s[0])))
    #s.append(str(self.values))
    #s.append(str(self.getRange()))
    #s.append(str(self.getStacks(width)))
    return "\n".join(s)
