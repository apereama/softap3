#==============================
#  TcTriangle
#============================== 
class TcTriangle:
    """
    """
    def __init__(self, segRef1, segRef2, segRef3):
        self.seg1 = segRef1
        self.seg2 = segRef2
        self.seg3 = segRef3
        self.p1 = self.segRef1.pr1
        self.p2 = self.segRef2.pr1
        self.p3 = self.segRef3.pr1
        

#==============================
# TriangleContainer
#==============================
class TriangleContainer:
    """
    """
    def __init__(self, segmentContainer):
        self.triangles = []
        self.sc = segmentContainer
    
    #------------------------------------------------------
    def addTriangle(self, p1, p2, p3):
        """
        """
        sr1 = self.sc.addSegment(p1,p2)
        sr2 = self.sc.addSegment(p2, p3)
        sr3 = self.sc.addSegment(p3, p1)
        
        for t in sr1.getIncidentTriangles():
            # adjacent triangles contain p1 and p2
            # so the check is for p3
            q1, q2, q3 = t.getPoints()
            if (q1 == p3) or (q2 == p3) or (q3 == p3):
                return t # this is the triangle we are looking for
        
        # if we come here, is because no similar triangle was found, so add one
        newT = TcTriangle(sr1, sr2, sr3)
        self.triangles.append(newT)
        return newT
    