"""
@TODO <short description of the module>

@TODO <description of the module>

:copyright: 2016 by Angel Perea Martinez (angelpeream@yahoo.com), see AUTHORS
            for more details

:license: lGPLv3, see LICENSE for more details
"""
import random
import time
import re
import sys
import logging

myLogger = logging.getLogger("strutil")

ESC = "\033["

END = "\033[0m"

    
#-------------------
# returns a rotating wheel
#-------------------
def rotate():
    """ returns a generator, whose yield() function returns a rotating sign along invocations"""
    while 1:
        for i in ["/","-","\\", "|"]: yield str(i)

#-------------------------
# printDict
#------------------------
def printDict(d):
    """
    """
    for n,v in list(d.items()):
        print(("%s:%s" % (str(n), str(v))))
  
 
DISPLAY_LEVEL = 1

#--------------------------
# display
#--------------------------
def display(string, color = None, level=10):
    """
    """
    if level < DISPLAY_LEVEL: 
        return
    if color != None: string = colorize(string, color)
    print(string)
    
    
 
#-----------------------------
def displayParam(name, value, level=10):
    """
    parameter print
    """
    if level < DISPLAY_LEVEL: 
        return
    print(("%s: %s" % (colorize(name, "green"), value)))

#---------------------------
# pprint
#---------------------------
def pprint(name, value):
    """
    parameter print. Take out from here
    """
    print(("%s:'%s'" % (colorize(name, "green"), value)))
    
    
#---------------------------
def tprint(name):
    """
    title print
    """
    print()
    print(("#" * (len(name) + 4)))
    print(("  " + name + "  "))
    print(("#" * (len(name) + 4)))
    print() 

#------------------------
_indent = " "

#-----------------
def iprint(s):
    """
    print indented
    """
    print(( _indent + s ))
  
#-------------------
# preprint
#-------------------
def preprint(string):
    """ prints a string without forcing a line feed"""
    sys.stdout.write(string + "\r") 
    sys.stdout.flush()
    
#--------------------
def printFlush(string):
    """
    """
    sys.stdout.write(string); sys.stdout.flush()

#----------------------------
# getSlice
#---------------------------
def getSlice(s, start, end):
    """
    returns an slice of a string between two substrings
    """
    i, j = s.index(start), s.index(end)
    return s[i + len(start): j]


#---------------------------
#  randomId
#---------------------------
def randomId(length=10, prefix="", suffix = "", charset="abcdefghijklmnopqrstuvwxyz1234567890"):
    """
    returns a random id
    @param length length of the id, usualy 10
    @param prefix prefix
    @param charset chars with whom the id is made
    """
    return prefix + "".join([random.choice(charset) for _ in range(length)]) + suffix


#-------------------------------
# adjustText
#-------------------------------
def adjustText(text, ncols = 80):
    """
    """
    lines = []
    for l in text.split("\n"):
        if len(l) > ncols:
            while len(l) > ncols:
                lines.append(l[:ncols])
                l= l[ncols:]
        
        lines.append(l)
            
    return "\n".join(lines)


#--------------------------
# reMap
#--------------------------
def reMap(text, pattern, function, *extraArgs, **extraParams):
    """
    aplies funcion(match, arg) to every match of pattern in text
    """
    print("deprecate for edn20")
    chText = []
    i = 0
    for m in re.finditer(pattern, text):
        chText.append(text[i:m.start(0)])
        r = function(m, *extraArgs, **extraParams)
        if r is not None:
            chText.append(r)
        i = m.end(0)
    chText.append(text[i:])
    return "".join(chText)

reSubst = reMap


#------------------------------
#  indent
#-----------------------------
def indent(string, depth):
    """
    indents a text
    """
    if isinstance(string, list):
        lines = string
    else:
        lines = string.split("\n")

    lines = [" " * depth + l for l in lines]
    return "\n".join(lines)


#--------------------------
#  transpose
#--------------------------
def transposeText(text):
    """
    transposes a block of text
    """
    result = []
    lines = text.split("\n")
    l = max([len(l) for l in lines])
    lines = [i.ljust(l) for i in lines]
    lines.reverse()
    for row in range(l-1, 0, -1):
        result.append("".join([line[row] for line in lines]))
    return result


#--------------------
# htColorize
#--------------------
def htColorize(text, color):
    """
    returns color for html
    """
    return '<font color="%s">%s</font>' % (color, text)


#-------------------------
# stripList
#-------------------------
def stripList(ls):
    """
    strips members of a list
    """
    return [x.strip() for x in ls]


#---------------------------
# findInOrder
#---------------------------
def findInOrder(text, *strings):
    """
    returns True if the given string are found (not overlapped) in the 
    same order in the string
    """
    i = 0
    for s in strings:
        i = text.find(s, i)
        if i == -1: return False        
        i += len(s)       # to avoid overlapping
        
    return True   
        

#-------------------------
#  splitWidth
#-------------------------
def splitWidth(string, width = 80, splitChar = "\n"):
    """
    """
    lines = []
    while len(string)>0:
        a,string = string[:width], string[width:]
        lines.append(a)
    return splitChar.join(lines)
    
  
#-----------------------------
__IS_COLOR_CONSOLE = True   # flag to disable colorizer


#-----------------------------
# setColorConsole
#-----------------------------
def setColorConsole(value=True):
    """
    sets the value for color console. True means color enabled
    """
    __IS_COLOR_CONSOLE = value

colors = {
        "white": "39m",
        "brightwhite": "1m",
        "black": "30m",
        "grey": "1;30m",
        "red": "31m",
        "lightred": "1;31m",
        "green": "32m",
        "lightgreen": "1;32m",
        "orange": "33m",
        "yellow": "1;33m",
        "blue": "34m",
        "lightblue": "1;34m",
        "magenta": "35m",
        "pink": "1;35m",
        "lightmagenta": "1;35m",
        "cyan": "36m",
        "lightcyan": "1;36m",
        "lightgrey": "37m",
        "darkwhite": "1;37m", 
        "normal": "1;0m"
    }


#------------------------------
#      colorize
#------------------------------
def colorize(string, color=None, reverse = False, underline = False, returnToNormal = True):
    """
    returns a text formatted so it will be displayed in a bash console
    with the wanted color
    #red{asdfasfd}
    
    """
    
    string = str(string)
    if not __IS_COLOR_CONSOLE:
        return string  # if is disabled

    END = "\033[0m"
    
    # 1) replace back to normal to back to this color (because this
    # color is now what "normal" is
    
    string = string.replace(END, ESC + colors[color])
    
    # 2) apply color to the beginning of the string
    
    if color is not None:
        
        colorCode = colors[color]
        
        if reverse: 
            colorCode = colorCode.replace("0;3", "4")
            colorCode = colorCode.replace("1;3" , "4")

        if underline: 
            colorCode = colorCode.replace("0;3", "4;3")
            colorCode = colorCode.replace("1;3" , "4;3")
    
        if not returnToNormal:
            END = ""
            
        result =  ESC + colorCode + string + END
        return result
     
    else: # color is introduced partially using ${}
        for c in colors:
            pattern = "$" + c + "{([^}]*?)}"
            while 1:
                m = re.search(pattern, string)
                if m is None: break
                start = ESC + colors[c]                
                string = string[:m.pos(0)] + start + m.group(1) + END + string[m.pos(0) + len(m.group(0)):]
                
        return string

def setColor(color=None, reverse = False, underline = False):
    """
    changes color, without returning to normal
    """
    print(colorize("", color, reverse, underline, returnToNormal = False))
    
#----------------------------
# colorizeHtml
#----------------------------
def colorizeHtml(text, color):
    """
    returns a text formatted in a way so it results in colored HTML text
    """
    return "<font color=%s>%s</font>" % (color, text)



def colorizeBySign(s, value, keepMinus=True):
    """
    colorizes a string according to the sign
    """
    if int(value) > 0:
        return colorize(s, "green")
    else:
        if keepMinus:
            return colorize(s, "red")
        else:
            return colorize(s[1:], "red")


def colorizeByRanges(string, value, points, values):
    """
    returns the appropriate color for a value, given a swatch
    :param string:
    :param value:
    :param points:
    :param values:
    :return: the calculated color
    """
    """
    :param value: 
    :param points: 
    :param colors: 
    :return: 
    """
    if (len(values) - 1) != len(points):
        raise ValueError(f"wrong number of points/colors {len(points)}, {len(values)}")
    i = 0
    while (i < len(points)) and (value > points[i]):
        i += 1
    return colorize(string, values[i])


#-------------------
# normalize
#-------------------
def normalize(name):
    """
    strips accents. Returns an ascii string from an unicode one.
    s must be a normal string... it will be converted to unicode

    The problem is that the returned string must _not_ be unicode  (being
     unicode makes the rest of the application thrive with errors when comparing
     unicode to std strings). As the input string is being converted to unicode,
     is difficult to keep track of homologous characters both in unicode and original string.
     solution is converting the string to unicode, and when not substituting, coherce to ascii.
     If that doesn't succeeds, put a "*"
    """
    
    a = "áéíóúàèìòùäëïöüâêîôûÁÉÍÓÚÀÈÌÒÙÄËÏöüÂÊÎÔÛ"
    b = "aeiouaeiouaeiouaeiouaeiouaeiouaeiouaeiou"
        
    name = name.strip().lower()
    result = ""
        
    for i in range(len(name)):
        c = name[i]
        if a.find(c) != -1:
            result += b[a.find(c)]
        else:
            result += c
    
    #print(name + "->" + result)
    return result.strip()


#-------------------------------------------

dInputQueue = []   # a queue to provide values to the xInput function
#-------------------------------------------------------------

#-------------------------------
#
#-------------------------------
def incrementName(name):
    """
    from "name" returns "name01"
    from "nameNN", where NN is a number, returns "nameMM", where MM=NN+1
    """
    i = len(name) - 1
    while i > 0 and name[i] in "01234567890":
        i -= 1
    while i < len(name) and not name[i] in "0123456789":
        i += 1
    n = 1 if i == len(name) else int(name[i:]) + 1
    return name[:i] + str(n).rjust(2, "0")


#--------------------------------
#
#-------------------------------
def consumeTokenBash(text):
    """
    gets the first token, no matter if it's quoted (with, may be, escaped quotes inside)
    or is a word (with, may be, escaped space chars inside)
    """

    pattern = r"""('.*?(?<!\\)' *)|(".*?(?<!\\)" *)|(.*?(?<!\\)( +|$))"""
    #           \-------------/   \------------/   \--------------/
    #                 |                  |                +--------- (1)
    #                 |                  +-------------------------- (2)
    #                 +----------------------------------------------(3)
    #
    # (1) or a space separated param taking care of \<space>
    # (2) or a quotes delimited with care to \"
    # (3)

    m = re.match(pattern, text)

    param = m.group(0).strip()
    rest = text[len(m.group(0)):]

    if param.startswith('"') or param.startswith("'"):
        param = param[1:-1]
    param = param.replace(r'\"', '"')
    param = param.replace(r'\ ', ' ')

    return param, rest

#-------------------------
# colorizeNumber
#-------------------------
def colorizeNumber(number, format_="%i"):
    """
    prints a number with different colors according with some expression
    """
    s = format_ % number
    if number < 0:
        s = colorize(s, "red")
    else:
        s = colorize(s, "green")
    return s


#--------------------------------
def commaList(l):
    return ", ".join(l)
 #---------------------------
#        mergeCols
#---------------------------
def mergeCols(cols, colWidth = 40, colGap = 5):
    """
    merges text from two sources into a two-column string
    """
    for i in range(len(cols)):
        if isinstance(cols[i], str):
            cols[i] = cols[i].split("\n")

    # wrap too-long lines
    #for column in cols:
    height = max([len(c) for c in cols])

    for col in cols:
        while len(col) < height: col.append("")

    result = []

    for i in range(height):
        line = ""
        for c in cols:
            #print len(c)
            line += " " * colGap + c[i].ljust(colWidth)
        result.append(line)

    return "\n".join(result)



#------------------------------------------------
def twoColumns(a, b, width, separator="|"):
    """
    """
    w2 = width / 2
    if isinstance(a, str):
        a = a.split("\n")
    if isinstance(b, str):
        b = b.split("\n")
    while len(a) < len(b):
        a.append("")
    while len(b) < len(a):
        b.append("")
    return "\n".join([a[i].ljust(w2) + separator + b[i].ljust(w2) for i in range(len(a))])

#---------------
# printTitle
#--------------
def printTitle(s):
    print("=" * (len(s) + 4))
    print("  " + s + "  ")
    print("=" * (len(s) + 4))


#------------------
# ProgressMonitor
#------------------
class ProgressMonitor:

    def __init__(self):
        self.reset(100)

    def advance(self, points=1):
        self.pos += points

    def reset(self, size):
        self.size = size
        self.pos = 0

    def getProgress(self):
        return self.pos / float(self.size)


#----------------
# ProgressBar
#----------------
class ProgressBar2:
    """
    allows progress bar displayng in no-erase consoles. Each call
    to advance decides whether to print another mark or not
    """

    def __init__(self, steps, width, char="="):
        self.steps = float(steps)    # width in chars
        self.width = float(width)    # total steps
        self.char = char
        self.pos = 0
        self.charsPerStep = float(self.width) / self.steps if self.steps != 0 else 0

    def getScale(self):
        return "|" + " " * (int(self.width) - 2) + "|"

    def advance(self, steps=1):
        """
        returns a string with the piece of the advanced bar to add
        """
        s = ""
        for _ in range(0, steps):
            self.pos = self.pos + 1
            if int(self.pos * self.charsPerStep) > \
               int((self.pos - 1) * self.charsPerStep):
                s += self.char
            else:
                pass
        return s

    def advanceTo(self, position):
        return self.advance(position - self.pos)

    def advanceToPercent(self, percent):
        return self.advanceTo(int(percent * self.width / 100.0))


#======================
# TextProgressMonitor
#======================
class TextProgressMonitor(ProgressMonitor):
    """
    """

    #---------------------------------
    def __init__(self, width):
        """
        """
        self.width = width

    #----------------------------------
    def advance(self, points=1):
        """
        """
        x = (self.width * self.pos) / self.size
        self.pos += points
        y = (self.width * self.pos) / self.size

        if y - x > 0:
            print("*" * (y - x))

#====================
#   PrintTree
#====================
def printTree(root, depths="", idField="name", childrenField="children"):
    """
    """
    children = getattr(root, childrenField)

    print(depths[:-1] + "+ " + root.name)

    if children != []:
        print(depths + "  |")

        for child in children:
            printTree(child, depths + "  |")

        print(depths)


#------------------
# reSubst
#------------------
def reSubst(text, rexp, funcion):
    """
    busca todas las ocurrencias de la expresion regular 're' en 'texto'
     y las substituye por el texto que devuelva la funcion 'funcion' al
     pasarle un matchObject
    """
    p = ""
    if isinstance(rexp, str):
        rexp = re.compile(rexp)
        
    while True:
        m = rexp.search(text)
        if m is None:
            break
        p = p + text[:m.start(0)] + funcion(m.group(0))
        text = text[m.end(0):]

    return p + text


#-----------------
# plural
#--------------------
def plural(i, name, suffix="s"):  # @TODO get better name for this function
    """
    returns the quantity

    Examples:
      plural(5, "house") = "5 houses"
      plural(1, "house") = "1 house"
      plural(0, "house") = "1 houses"
      plural(6, "torch", "es") = "6 torches"

    """
    # This function is a copy of other in Pycommons.StrUtil. It was copied
    # here to avoid dependence

    if i != 1:
        name = name + suffix

    return str(i) + " " + name


#-------------------------
# canonize
#-------------------------
def canonize(name):
    name = name.lower()
    name = [x for x in name if x in "abcdefghjijklmnopqrstuvwxyz"]
    return name


#---------------
# splitString
#---------------
def splitString(s, marker):
    """
    splits a string in two at the marker; if not find any, returns a pair
    with an empty string as second component. Note that is intended to
    substitute string.split() who does'nt return always a tuple
    """

    i = s.find(marker)
    if i == -1:
        i = len(s)
    return s[:i], s[i + len(marker):]

#-------------
# safeSplit
#-------------
def safeSplit(s, marker):
    """
    alias for splitString
    """
    return splitString(s, marker)


#-----------------------
# smartTrim
# trims a string, putting trailing dots
#-----------------------
def smartTrim(s, length, suffix = "..."):
    """
    """
    suffix = str(suffix)
    s = str(s)
    return s[:(length - len(suffix))] + suffix  if len(s) > length - len(suffix) else s


#---------------------------
# box
#--------------------------

def box2(s, width=80, color=None):
    """
    """
    lines = ["+" + s.ljust(width - 2) + "+"]
    end = "+" + "-" * width - 2 + "+"
    lines.append(end)
    lines.insert(0, end)
    if color is not None:
        lines = [colorize(l, color) for l in lines]
    return "\n".join(lines)


#-----------------------------
#  box
#----------------------------
def box(string, width=70, border="white", indent=0):
    """
    """
    if isinstance(string, str):
        lines = string.split("\n")
    elif isinstance(string, tuple) or isinstance(string, list):
        lines = string
    else:
        lines = str(string).split("\n")
        
    lines = [" " * indent + colorize("| ",border) + 
             l[:width].ljust(width) + colorize(" |",border) 
             for l in lines]
    
    s = colorize(" " * indent + "+" + "-" * (width + 2) + "+", border)
    lines.insert(0, s)
    lines.append(s)
    return "\n".join(lines)

#---------------
# trimFloat
#---------------
def trimFloat(f, digits):
    """
    """
    shift = pow(10, digits)
    f = int(f * shift) * 1.0
    return f / shift


#------------------------------------
#  xmlTag
#------------------------------------
def xmlTag(tagname, parameters=None, content=None):
    """
    produces xml tags. If content given, returns also the closing tag
    """
    s = "<" + tagname
    if parameters is not None:
        s += " " + parameters

    s = s + ">"

    if content is not None:
        s = s + content + "</" + tagname + ">"

    return s




#----------------------
#
#----------------------
def fileLog(fileName, colWidth, data):
    '''
    añade una fila con datos a un fichero log
    '''
    f = open(fileName, "a")
    d = [time.strftime('%d/%m/%y')]
    d.extend(data)
    data = d

    s = "\n"

    for x in data:
        s = s + str(x).rjust(colWidth) + ", "
    f.write(s)
    f.close()
    
#---------------------------------------
#
#---------------------------------------
def glob2re(pat):
    """Translate a shell PATTERN to a regular expression.
    There is no way to quote meta-characters.
    """

    i, n = 0, len(pat)
    res = ''
    while i < n:
        c = pat[i]
        i = i+1
        if c == '*':
            #res = res + '.*'
            res = res + '[^/]*'
        elif c == '?':
            #res = res + '.'
            res = res + '[^/]'
        elif c == '[':
            j = i
            if j < n and pat[j] == '!':
                j = j+1
            if j < n and pat[j] == ']':
                j = j+1
            while j < n and pat[j] != ']':
                j = j+1
            if j >= n:
                res = res + '\\['
            else:
                stuff = pat[i:j].replace('\\','\\\\')
                i = j+1
                if stuff[0] == '!':
                    stuff = '^' + stuff[1:]
                elif stuff[0] == '^':
                    stuff = '\\' + stuff
                res = '%s[%s]' % (res, stuff)
        else:
            res = res + re.escape(c)
    return res + '\Z(?ms)'



#-------------------------------------
#       xmlProtect
#-------------------------------------
def xmlProtect(string):
    """
    used to not to produce false tags
    @string string string to be protected against xml abuse
    @returns string string we want to take all the conflictive XML markup from
    """
    #@COMPLETE
    string = string.replace("&", "&amp;")
    string = string.replace("<", "&lt;")
    string = string.replace(">", "&gt;")
    string = string.replace('"', "&quot;")
    string = string.replace("'", "&apos;")

    return string

#---------------------------
#     htmlProtects
#---------------------------
def htmlProtect(string):
    """
    """
    return xmlProtect(string)

#-------------------------
#       latexProtect
#-------------------------
def latexProtect(string):
    """
    this is a pretty slow function, most likely candidate to be optimized!!!!
    @param string string being formatted to latex
    @returns the string so i doesn't flip error messages
    """
    monster = "##@1374826499342001@##"  # an unlikely string
    string = string.replace(r"$", monster)  # change to avoid problems later...
    string = string.replace("\\", r"$\backslash$")
                            # this will generate $, that's why they were removed
                            # and changed
    string = string.replace(monster, r"\$") 
    string = string.replace( r"{", r"\{")  
    string = string.replace(r"}", r"\}") 
    string = string.replace(r"&", r"\&") 
    string = string.replace(r"#", r"\#") 
    string = string.replace(r"%", r"\%") 
    string = string.replace(r"^", r"\^") 
    string = string.replace(r"_",r"-")  
    string = string.replace(r"~",r"\~") 
    return string

        
