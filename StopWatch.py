import time
 

from rongo.extlibs.softap.timeTools import secs2human

#====================
# StopWatch
#====================
class StopWatch:
    """
    """
    
    def __init__(self):
        """
        """
        self.reset()
        self.start()
        
    #-----------------------
    
    def read(self):
        """
        returns the elapsed time without stopping
        """
        return self.time + self.getLap()

    #---------------------
    def getTime(self):
        return self.read()
    
    #-------------------------
    def getLap(self):
        """
        """
        if self._start != None:
            return time.time() - self._start
        else:
            return 0
 
    #------------------------- 
    def start(self):
        """
        starts again the clock
        """
        self._start = time.time()

        
    #-------------------------
    def stop(self, human = False):
        """
        stops the clock, accumulating the time
        """
     
        if self._start != None:
            ret = time.time() - self._start
            self.time += ret
        else:
            ret = 0            
        
        self._start = None
        if human:
            return secs2human(ret)
        else:
            return ret
            
    #---------------------
    def reset(self):
        """
        resets the clock, stopping
        """
        self.time = 0
        self._start = None
        
