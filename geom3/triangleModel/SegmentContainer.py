from PointContainer import  PcPoint

#=================================
# ScSegment
#=================================
class ScSegment:
    """
    """
    def __init__(self, pcPoint1, pcPoint2):
        """
        @param point1
        @param point2
        """
        if not isinstance(pcPoint1, PcPoint): raise ValueError("not pcpoints")
        if not isinstance(pcPoint2, PcPoint): raise ValueError("not pcpoints")
        self.pr1, self.pr2 = pcPoint1, pcPoint2
        self.incidentTriangles = [] # list with incident triangles



#==============================
# SegmentContainer
#==============================
class SegmentContainer:
    
    def __init__(self):
        """
        """
        self.segments = []
        
        
    #-------------------------------------
    def addSegment(self, p1, p2):
        """
        """
        # 1) look for one end in the point container
        p = self.pc.addPoint(p1)
        for t in p.incidentSegments:
            a,_ = t.points  # are point3D
            other = p2 if a.equal(p2) else p1
            if other.point.equals(p2):
                return t
        
        # if it comes here, there was no similar segment
        q = self.pc.addPoint(p2)
        newSegment = ScSegment(p,q)
        self.segments.append(newSegment)
        return newSegment
    
    